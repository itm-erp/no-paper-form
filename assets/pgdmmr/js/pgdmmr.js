$(document ).ready(function() {
			
	var base_url = $('#baseurl').val();

	$("#login_btn").click(function(){
		user_login();
	});
	
	$("#btn_filter").click(function(){
		display_filter_data();
	}); 
	
	$(".print_doc").click(function(){
				print_doc(this);
			});

	$(".edit_record").click(function(){
		edit_record(this);
	
	});	
	
	$("#exact_search_select").change(function(){
			var placeholder=$("#exact_search_select").val();
			 $("#exact_search_txt").show();
			 $("#exact_search_txt").attr("placeholder", "Enter "+placeholder);
		}); 
			
		
		$("#relative_search_select").change(function(){
			var placeholder=$("#relative_search_select").val();
			 $("#relative_search_txt").show();
			 $("#relative_search_txt").attr("placeholder", "Enter "+placeholder);
		});
		
		
	//download_xls_data();
   $("#btn_xls").click(function(e) {
		e.preventDefault();

		//getting data from our table
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('tbl_filter_data');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');

		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = 'exported_Search_result_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
		a.click();
	});
	
	//download Coupons in xls ;
   $("#btn_coupons_xls").click(function(e) {
		e.preventDefault();

		//getting data from our table
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('tbl_coupons_data');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');

		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = 'exported_Coupons_result_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
		a.click();
	});
	
	//download Coupons To Be Approved in xls ;
   $("#btn_approved_coupons_xls").click(function(e) {
		e.preventDefault();

		//getting data from our table
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('tbl_approved_coupons_data');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');

		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = 'exported_Coupons_result_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
		a.click();
	});
	
	$("#coupon_user_id").change(function(){
	  $("#search_coupon_form").submit();
		});
		
	$("#coupon_app_user_id").change(function(){
		$("#search_apprv_coupon_form").submit();
		}); 
		
	$("#approve_all").click(function(){
			if(this.checked){
				$(".chk_approved").prop("checked", true);
			}
			else
			{
				$(".chk_approved").prop("checked", false);
			
			}
		});
	
	$("#btn_reset").click(function(){
		$("#search_form")[0].reset();
	});

	$("#btn_reset").click(function(){
		reset_filter();
	});
	
	get_new_registrations();
	get_form_submitted();
	get_payment_done();
	get_pre_payment();
	daywise_form_submitted();
	recent_form_submitted();
	daywise_form_submitted_tabularform();
	marketing_office_submission_graph();
	
	
	$('#tbl_filter_data').DataTable();
	$('#tbl_coupons_data').DataTable();
	$('#tbl_approved_coupons_data').DataTable();
	$("#agent_table").DataTable();
	display_filter_data();

});

//  /******* edit form validation *********** //


		jQuery.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z ]+$/i.test(value);
			}, "Letters only please"); 
		
		$("#step1_form").validate({
			
				
		rules:{
			required:{
				required:true
			},
			payment_student_name:{
				required:true,
				 lettersonly: true
			},
			payment_student_emailid:{
				required:true,
				email:true,
			},
			payment_student_mobileno:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
		
 	$("#step2_form").validate({
		rules:{
			required:{
				required:true
			},
			stud_first_name:{
				required:true,
				 lettersonly: true
			
			},
			stud_middle_name:{
				required:true,
				 lettersonly: true
			},
			stud_last_name:{
				required:true,
				lettersonly: true
			},
			stud_dob:{
				required:true,
			},
			stud_gender:{
				required:true,
			},
			stud_email:{
				required:true,
				email:true,
			},
			stud_mobileno:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			father_first_name:{
				required:true,
				lettersonly: true
			},
			father_middle_name:{
				required:true,
				 lettersonly: true
			},
			father_last_name:{
				required:true,
				 lettersonly: true
			},
			father_mobile:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			mother_first_name:{
				required:true,
				lettersonly: true	
				
			},
			mother_middle_name:{
				required:true,	
				 lettersonly: true
			},
			mother_last_name:{
				required:true,	
				 lettersonly: true
			},
			mother_mobile:{
				required:true,	
				maxlength:10,
				minlength:10,
				digits:true,
			},
			
			
		},
		
		
		/* $('#step2_continue_btn').on('click', function () {
			$('#step2_form').valid();
			}); */
		
/* 		$('#step2_form').valid();
		//$('#stud_first_name').valid();
		var form = $( "#step2_form" );
		form.validate();
		
		  alert( "Valid: " + form.valid() ); */
		
		
		messages:{
			stud_mobileno:'Please enter at least 10 Digits.',
			father_mobile:'Please enter at least 10 Digits.',
			mother_mobile:'Please enter at least 10 Digits.',
			stud_email:'Please Enter Valid Email ID',
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	

	
	 $("#step3_form").validate({
		rules:{
			required:{
				required:true
			},
			corres_address:{
				required:true,	
			},
			permanent_address:{
				required:true,	
			},
			corres_city:{
				required:true,	
			},
			permanent_city:{
				required:true,	
			},
			corres_state:{
				required:true,	
			},
			permanent_state:{
				required:true,	
			},
			corres_state:{
				required:true,	
			},
			corres_pin:{
				required:true,	
				digits:true,
			
				minlength:6,
			},
			permanent_pin:{
				required:true,	
				digits:true,
				
				minlength:6,
			},
			
		},
		messages:{
			corres_pin:'Please enter 6 Digits Pincode.',
			permanent_pin:'Please enter 6 Digits Pincode.'
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	$("#step4_form").validate({
		rules:{
			required:{
				required:true
			},
			campus_pref_1:{
				required:true
			},
			campus_specialization_1:{
				required:true,
			},
			campus_pref_2:{
				notEqualTo: '#campus_pref_1',
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	 $("#step5_form").validate({
		rules:{
			required:{
				required:true
			},
			ssc_institution:{
				required:true
			},
			ssc_city:{
				required:true
			},
			ssc_board_university:{
				required:true
			},
			ssc_degree:{
				required:true
			},
			ssc_passing_date:{
				required:true,
				//date:true,
			},
			ssc_percentage:{
				required:true,
				//digits:true,
			},
			hsc_institution:{
				required:true,
			},
			hsc_city:{
				required:true,
			},
			hsc_board_university:{
				required:true,
			},
			hsc_stream_specialization:{
				required:true,
			},
			hsc_degree:{
				required:true,
			},
			hsc_passing_date:{
				required:true,
			},
			hsc_percentage:{
				required:true,
			},
			graduation_institution:{
				required:true,
			},
			graduation_city:{
				required:true,
			},
			graduation_university:{
				required:true,
			},
			graduate_stream_specialization:{
				required:true,
			},
			graduation_degree:{
				required:true,
			},
			graduation_passing_date:{
				required:true,
			},
			graduation_percentage:{
				required:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	 $("#step8_form").validate({
		rules:{
			decl_applicatnt_name:{
				required:true
			},
			decl_parent_name:{
				required:true
			},
			appl_submit_date:{
				required:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
//  /******* edit form validation / ***********

function fnEmailVerify(strEmailId)
{	
	strEmail = strEmailId.toLowerCase();

	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(strEmail))
	{
	  return true;
	}
	return false;
}

function user_login()
{
	$(".help-block").hide();
	
	var user_email = $("#user_email").val();
	var user_pass = $("#user_pass").val();
	var validate = false;
	
	if(user_email == ""){
		$(".class_your_email").show();		
		validate = true;
		return;
	}else if(!fnEmailVerify(user_email)){
		$(".class_your_valid_email").show();
		validate = true;
		return;
	}
	
	if(user_pass == ""){
		$(".class_your_password").show();		
		validate = true;
		return;
	}
	
	var dataString = 'user_email='+escape(user_email);
	dataString += "&user_pass="+ escape(user_pass);	
	
	var base_url = $('#baseurl').val();
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/login/user_login",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');
			if(data == "FAIL"){				
				$(".class_wrong_uid_pass").show();
				return;
			}else if(data == "SUCCESSFUL"){			
				window.location.href = base_url + "index.php/pgdmmr/dashboard/";
			}			
		}
	});
}

function get_new_registrations()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/get_new_registrations",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			$("#count_new_registrations").html(data);
		}
	});
}

function get_form_submitted()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/get_form_submitted",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			$("#count_form_submitted").html(data);
		}
	});
}

function get_payment_done()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/get_payment_done",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			$("#count_payment_done").html(data);
		}
	});
}

function get_pre_payment()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/get_pre_payment",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			$("#pre_payment").html(data);
		}
	});
}

function daywise_form_submitted()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/daywise_form_submitted",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			//alert(data);
			if(data == ""){
				return;
			}			
			var json_arr = eval(data);			
			
			if($('#daywise_form_submitted_chart').length){	
				
				Morris.Area({
				  element: 'daywise_form_submitted_chart',
				  data: json_arr,
				  xkey: 'application_submittion_dt',
				  ykeys: ['rcount'],
				  labels: ['Form Count', 'Date']
				});	
				
			}
		}
	});		
}

function recent_form_submitted()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/recent_form_submitted",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			//alert(data);
			if(data == ""){
				return;
			}			
			
			var html_data = "";
			var json_arr = eval(data);						
			for(var i =0;i < json_arr.length-1;i++){
			  var item = json_arr[i];			  
			 
			 html_data +='<li><span class="handle"><i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i></span><span class="text">'+ item.application_number +'</span><small class="label label-danger"><i class="fa fa-clock-o"></i>&nbsp;'+item.application_submittion_formatted_dt+'</small><div class="tools"><i class="fa fa-edit"></i></div></li>';
			}
			
			$("#recent_form_submitted").empty();
			$("#recent_form_submitted").append(html_data);			
		}
	});
}

function daywise_form_submitted_tabularform()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/daywise_form_submitted_tabularform",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			//alert(data);
			if(data == ""){
				return;
			}			
			
			var html_data = "<tr><th>Date</th><th>Form Count</th></tr>";
			var json_arr = eval(data);						
			for(var i =0;i < json_arr.length-1;i++){
			  var item = json_arr[i];
			  html_data += '<tr><th>'+item.application_submittion_dt+'</th><th>'+item.rcount+'</th></tr>';
			}
			
			$("#datewise_form_submitted").empty();
			$("#datewise_form_submitted").append(html_data);			
		}
	});
}

function marketing_office_submission_graph()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/marketing_office_submission_graph",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			//alert(data);
			if(data == ""){
				return;
			} 
			
			if($("#marketing_office_submission_graph").length){
				/*var json_arr = eval(data);			
				//alert(json_arr);
				Morris.Area({
				  element: 'marketing_office_submission_graph',
				  data: json_arr,
				  xkey: 'name',
				  ykeys: ['reg_count'],
				  labels: ['City', 'Count']
				});*/
			}				
		}
	});
}

function marketing_office_submission_tabularform()
{
	var base_url = $('#baseurl').val();	
	var dataString = "";
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/marketing_office_submission_tabularform",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			alert(data);
			if(data == ""){
				return;
			} 
			
			var json_arr = eval(data);			
			//alert(json_arr);
			if($("#marketing_office_submission_graph").length){
				Morris.Area({
				  element: 'marketing_office_submission_graph',
				  data: json_arr,
				  xkey: 'name',
				  ykeys: ['reg_count'],
				  labels: ['City', 'Count']
				});
			}
				
		}
	});
}

function reset_filter()
{
	$("#txtApplicationNumber").val('');
	$("#filter_by").val('');
	$("#form_status").val('');
	$("#txtLocation").val('');
}
function display_filter_data()
{	
    //$('#loadingmessage').show();

	var base_url = $('#baseurl').val();	
	$("#tbl_filter_data").html("<center><img src='"+base_url+"/assets/img/ajaxloading.gif' / ></center>");
	var txtApplicationNumber = $("#txtApplicationNumber").val();
	var filter_by = $("#filter_by").val();
	var form_status = $("#form_status").val();
	var txtLocation = $("#txtLocation").val();
	var exact_search_select = $("#exact_search_select").val();
	var exact_search_txt = $("#exact_search_txt").val();
	var relative_search_select = $("#relative_search_select").val();
	var relative_search_txt = $("#relative_search_txt").val();
	
	var dataString = 'txtApplicationNumber='+escape(txtApplicationNumber);
	dataString += "&filter_by="+ escape(filter_by);
	dataString += "&form_status="+ escape(form_status);
	dataString += "&txtLocation="+ escape(txtLocation);
	dataString += "&exact_search_select="+ escape(exact_search_select);
	dataString += "&exact_search_txt="+ escape(exact_search_txt);
	dataString += "&relative_search_select="+ escape(relative_search_select);
	dataString += "&relative_search_txt="+ escape(relative_search_txt);
	
	$.ajax({
		url:base_url+"index.php/pgdmmr/dashboard/display_filter_data",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');				
			$("#tbl_filter_data").html(data);
			$('#tbl_filter_data').DataTable();
			
			$(".print_doc").click(function(){
				print_doc(this);
			});

			$(".edit_record").click(function(){
				edit_record(this);
			
			});

			$(".view_document").click(function(){
				view_document(this);
			});		
		  	
		}
	});
}
		function print_doc(obj)
		{	
			var reg_id = $(obj).attr("reg_id");	
			var base_url = $('#baseurl').val();	
			
			var dataString = 'reg_id='+escape(reg_id);
			
			$.ajax({
				url:base_url+"index.php/pgdmmr/dashboard/print_doc",
				type: "POST",
				data: dataString,
				datatype: "html",
				async: true,
				cache: false,
				success : function(data) 
				{
					var data = data.replace(/^\s+|\s+$/g, '');					
					$('.printModal').html(data); 
					$('#printModal').modal('show'); 
				}
			});
		}
		$("#mobile_number,#payment_student_mobileno, #stud_mobileno, #father_mobile, #mother_mobile, #total_work_experience").keypress(function(event) {
			return validateNumber(event);
		});		
	  	
				
		$("#step2_continue_btn").click(function(){	
		var valid = $('#step2_form').valid();
					if(valid == false){
						return false;
					}		
			save_continue_step2();			
		});
		
		$("#step3_back_btn").click(function(){						
			back_step3();			
		});
		
			
		$("#step3_continue_btn").click(function(){	
			var valid = $('#step3_form').valid();
				if(valid == false){
					return false;
				}		
			save_continue_step3();			
		});
		
		$("#permanent_corres_same").click(function(){
			if(this.checked){
				corres_permanent_add_same();
			}
			else
			{
				$("#step3_form").trigger("reset");
			
			}
		});
		
		
		$("#campus_pref_1").change(function(){
			campus_specialization_1(this);
			
		});
		
		$("#campus_pref_2").change(function(){
			campus_specialization_2(this);
		});
		
		$("#step4_back_btn").click(function(){						
			back_step4();			
		});
		
		$("#step4_continue_btn").click(function(){	
			 var valid = $('#step4_form').valid();
				if(valid == false){
					alert('hii');
					return false;
				}		
			save_continue_step4();			
		});

		$("#gdpi_locations").change(function(){						
			get_gdpi_location_available_dates();			
		});

		$("#btn_add_more_exams").click(function(){						
			add_exam_row();			
		});
			
		$(".delete_exam_row").click(function(event){						
			event.stopPropagation();
			delete_exam_row(this);			
		}); 
		
		$("#step5_back_btn").click(function(){						
			back_step5();			
		});
		
		$("#step5_continue_btn").click(function(){	
			var valid =  $('#step5_form').valid();
				if(valid == false){
					return false;
				}		
			save_continue_step5();			
		});
		
		$("#referrer_source").change(function(){						
			display_referrer_source();			
		});
		
		$("#step6_back_btn").click(function(){						
			back_step6();			
		});
		
		$("#step6_continue_btn").click(function(){	
			var valid = $('#step6_form').valid();
				if(valid == false){
					return false;
				}		
			save_continue_step6();			
		});
		
		$("#step7_back_btn").click(function(){						
			back_step7();			
		});
		
		$("#step7_continue_btn").click(function(){	
			var valid = $('#step7_form').valid();
				if(valid == false){
					return false;
				}		
			save_continue_step7();			
		});
		
		$("#step6_form").on('submit',(function(e) {			
			e.preventDefault();
			$.ajax({
				url: base_url+"index.php/admission/save_continue_step7",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					//alert(data);
				},
				error: function(data){
					//alert(data);
				} 	        
		   });
		}));
		
		$('#stud_dob').datepicker({disableTouchKeyboard: false, format: 'dd/mm/yyyy',autoclose: true, todayHighlight: false, enableOnReadonly: true,});
		
		$('#ssc_passing_date, #hsc_passing_date, #graduation_passing_date, #post_graduation_passing_date, .work_experience_from_year,.work_experience_to_year').datepicker({disableTouchKeyboard: false, format: 'mm/yyyy',autoclose: true, todayHighlight: false, enableOnReadonly: true,});
		
		$('.test_date').datepicker({disableTouchKeyboard: false, format: 'dd/mm/yyyy',autoclose: true, todayHighlight: false,});
		
		function fnEmailVerify(strEmailId)
		{	
			strEmail = strEmailId.toLowerCase();

			if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(strEmail))
			{
			  return true;
			}
			return false;
		}

		function validateNumber(event)
		{
			var key = window.event ? event.keyCode : event.which;
			if (event.keyCode == 8 || event.keyCode == 46
			 || event.keyCode == 37 || event.keyCode == 39) {
				return true;
			}
			else if ( key < 48 || key > 57 ) {
				return false;
			}
			else return true;
		}
		
	function edit_record(obj)
	{
		var reg_id = $(obj).attr("reg_id");	
		var base_url = $('#baseurl').val();	
		var dataString = 'reg_id='+escape(reg_id);
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/edit_record",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success : function(data) 
			{
				var data = data.replace(/^\s+|\s+$/g, '');	
     			$('.editModal').html(data); 				
				$('#editModal').modal('show'); 
				$('#campus_specialization_1').selectpicker('refresh');	
				$('#campus_specialization_2').selectpicker('refresh');	
			}
		});
		
	//	var reg_id = $(obj).attr("reg_id");
		//alert(reg_id);
	}
	
	function save_continue_step2()
	{
		var reg_id = $("#reg_id").val();	
		var stud_title = $("#stud_title").val();
		var stud_first_name = $("#stud_first_name").val();	
		var stud_middle_name = $("#stud_middle_name").val();	
		var stud_last_name = $("#stud_last_name").val();	
		var stud_dob = $("#stud_dob").val();	
		var stud_gender = $("#stud_gender").val();	
		var stud_email = $("#stud_email").val();	
		var stud_mobileno = $("#stud_mobileno").val();	
		var father_first_name = $("#father_first_name").val();	
		var father_middle_name = $("#father_middle_name").val();	
		var father_last_name = $("#father_last_name").val();	
		var father_mobile = $("#father_mobile").val();	
		var mother_first_name = $("#mother_first_name").val();	
		var mother_middle_name = $("#mother_middle_name").val();	
		var mother_last_name = $("#mother_last_name").val();	
		var mother_mobile = $("#mother_mobile").val();	
			
		
		var dataString = 'reg_id='+escape(reg_id);
		dataString += "&stud_title="+ escape(stud_title);
		dataString += "&stud_first_name="+ escape(stud_first_name);
		dataString += "&stud_middle_name="+ escape(stud_middle_name);
		dataString += "&stud_last_name="+ escape(stud_last_name);
		dataString += "&stud_dob="+ escape(stud_dob);
		dataString += "&stud_gender="+ escape(stud_gender);
		dataString += "&stud_email="+ escape(stud_email);
		dataString += "&stud_mobileno="+ escape(stud_mobileno);
		dataString += "&father_first_name="+ escape(father_first_name);
		dataString += "&father_middle_name="+ escape(father_middle_name);
		dataString += "&father_last_name="+ escape(father_last_name);
		dataString += "&father_mobile="+ escape(father_mobile);
		dataString += "&mother_first_name="+ escape(mother_first_name);
		dataString += "&mother_middle_name="+ escape(mother_middle_name);
		dataString += "&mother_last_name="+ escape(mother_last_name);
		dataString += "&mother_mobile="+ escape(mother_mobile);
			
		var base_url = $('#baseurl').val();
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/save_step2",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data) 
			{
				var data = data.replace(/^\s+|\s+$/g, '');						
				$('.nav-tabs > .active').next('li').find('a').trigger('click');			
			}
		});	
	}

	function back_step3()
	{
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	}

	function save_continue_step3()
	{	
		var reg_id = $("#reg_id").val();
		var corres_address = $("#corres_address").val();
		var permanent_corres_same = $("#permanent_corres_same").val();
		var find_your_location = $("#find_your_location").val();
		var permanent_address = $("#permanent_address").val();
		var unable_find_corres_location = $("#unable_find_corres_location").val();
		var unable_find_perm_location = $("#unable_find_perm_location").val();
		var corres_city = $("#corres_city").val();
		var perm_location_manually = $("#perm_location_manually").val();
		var corres_state = $("#corres_state").val();
		var permanent_city = $("#permanent_city").val();	
		var permanent_state = $("#permanent_state").val();
		var corres_pin = $("#corres_pin").val();
		var permanent_pin = $("#permanent_pin").val();
		
		var permanent_corres_same = ($("#permanent_corres_same").is(":checked") ? "Y" : "N");
		var unable_find_corres_location = ($("#unable_find_corres_location").is(":checked") ? "Y" : "N");
		var unable_find_perm_location = ($("#unable_find_perm_location").is(":checked") ? "Y" : "N");
		
		if(unable_find_corres_location == "Y"){
			corres_address = corres_city;
		}
		
		if(unable_find_perm_location == "Y"){
			permanent_address = permanent_city; 
		}
		
		var dataString = 'reg_id='+escape(reg_id);
		dataString += "&corres_address="+ escape(corres_address);
		dataString += "&permanent_corres_same="+ escape(permanent_corres_same);
		dataString += "&find_your_location="+ escape(find_your_location);
		dataString += "&permanent_address="+ escape(permanent_address);
		dataString += "&unable_find_corres_location="+ escape(unable_find_corres_location);
		dataString += "&unable_find_perm_location="+ escape(unable_find_perm_location);
		dataString += "&corres_city="+ escape(corres_city);
		dataString += "&perm_location_manually="+ escape(perm_location_manually);
		dataString += "&corres_state="+ escape(corres_state);
		dataString += "&permanent_city="+ escape(permanent_city);	
		dataString += "&permanent_state="+ escape(permanent_state);
		dataString += "&corres_pin="+ escape(corres_pin);
		dataString += "&permanent_pin="+ escape(permanent_pin);
		dataString += "&permanent_corres_same="+ escape(permanent_corres_same);
		dataString += "&unable_find_corres_location="+ escape(unable_find_corres_location);
		dataString += "&unable_find_perm_location="+ escape(unable_find_perm_location);
			
		var base_url = $('#baseurl').val();
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/save_continue_step3",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "SUCCESSFUL"){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				}else{
					alert(data);
				}		
			}
		});
	}

	function corres_permanent_add_same(){
		
		$("#permanent_address").val($("#corres_address").val());
		$("#find_your_perm_location").val($("#find_your_location").val());
		$("#permanent_city").val($("#corres_city").val());
		$("#permanent_state").val($("#corres_state").val());
		$("#permanent_pin").val($("#corres_pin").val());
		$("#autocomplete1").val($("#autocomplete").val());
	}

	function campus_specialization_1(obj)
	{
		var campus_id = $(obj).val();
		var reg_id = $("#reg_id").val();
		
		var base_url = $('#baseurl').val();
		var dataString = 'reg_id=' + escape(reg_id);
		dataString += "&campus_id="+ escape(campus_id);
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/campus_specialization_1",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "" || data == null || data == undefined || data == "[]" ){				
					$("#campus_specialization_1").html('<option value=""></option>');
					$('#campus_specialization_1').selectpicker('refresh');		
					return;
				}
				
				var json_arr = eval(data);			
				var html_data = "";
				for(var i =0;i < json_arr.length-1;i++){
				  var item = json_arr[i];			  
				  html_data += '<option vertical_campus_specialization_id="'+ item.vertical_campus_specialization_id +'" vertical_id="'+ item.vertical_id +'" campus_id="'+ item.campus_id +'" specialization_id="'+ item.specialization_id +'" campus_specialization_id="'+ item.campus_specialization_id +'" value="'+ item.campus_specialization_id +'">'+ item.specialization_name +'</option>';
				}			
				$("#campus_specialization_1").html(html_data);
				$('#campus_specialization_1').selectpicker('refresh');			
			}
		});
	}

	function campus_specialization_2(obj)
	{
		var campus_id = $(obj).val();
		var reg_id = $("#reg_id").val();
		
		
		var base_url = $('#baseurl').val();
		var dataString = 'reg_id=' + escape(reg_id);
		dataString += "&campus_id="+ escape(campus_id);
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/campus_specialization_2",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "" || data == null || data == undefined || data == "[]" ){				
					$("#campus_specialization_1").html('<option value=""></option>');
					$('#campus_specialization_1').selectpicker('refresh');		
					return;
				}
				var json_arr = eval(data);	
				var html_data = "";
				for(var i =0;i < json_arr.length-1;i++){
				  var item = json_arr[i];			  
				  html_data += '<option vertical_campus_specialization_id="'+ item.vertical_campus_specialization_id +'" vertical_id="'+ item.vertical_id +'" campus_id="'+ item.campus_id +'" specialization_id="'+ item.specialization_id +'" campus_specialization_id="'+ item.campus_specialization_id +'" value="'+ item.campus_specialization_id +'">'+ item.specialization_name +'</option>';
				}			
				$("#campus_specialization_2").html(html_data);
				$('#campus_specialization_2').selectpicker('refresh');
			}
		});
	}

	function back_step4()
	{
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	}

	function save_continue_step4()
	{
		var base_url = $('#baseurl').val();
		var reg_id = $("#reg_id").val();
		var campus_pref_1 = $("#campus_pref_1").val();
		var campus_pref_2 = $("#campus_pref_2").val();	
		var campus_specialization_1 = $('#campus_specialization_1').val(); 
		var campus_specialization_2 = $('#campus_specialization_2').val(); 
		var gdpi_locations = $("#gdpi_locations").val();
		var gdpi_available_dates = $('#gdpi_available_dates option:selected').attr('gdpi_location_available_dates_id');
			
		var dataString = 'reg_id=' + escape(reg_id);	
		dataString += "&campus_pref_1="+ escape(campus_pref_1);
		dataString += "&campus_pref_2="+ escape(campus_pref_2);	
		dataString += "&campus_specialization_1="+ escape(campus_specialization_1);	
		dataString += "&campus_specialization_2="+ escape(campus_specialization_2);	
		dataString += "&gdpi_locations="+ escape(gdpi_locations);
		dataString += "&gdpi_available_dates="+ escape(gdpi_available_dates);	
		
		
		if(campus_pref_1 == ""){
			return;
		}
		
		if(campus_specialization_1 == null || campus_specialization_1 == undefined || campus_specialization_1 == ""){
			return;
		}
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/save_continue_step4",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');
				if(data == "SUCCESSFUL"){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				}else{
					alert(data);
				}
			}
		});
		
	}

	function get_gdpi_location_available_dates()
	{
		
		var reg_id = $("#reg_id").val();
		var gdpi_locations = $("#gdpi_locations").val();
		
		
		var base_url = $('#baseurl').val();
		var dataString = 'reg_id=' + escape(reg_id);
		dataString += "&gdpi_locations="+ escape(gdpi_locations);
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/get_gdpi_location_available_dates",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "" || data == null || data == undefined || data == "[]" ){				
					$("#gdpi_available_dates").html('<option value=""></option>');
					$('#gdpi_available_dates').selectpicker('refresh');		
					
					return;
				}
				var json_arr = eval(data);	
				var html_data = "";
				for(var i =0;i < json_arr.length-1;i++){
				  var item = json_arr[i];			  
				  html_data += '<option gdpi_location_available_dates_id="'+ item.gdpi_location_available_dates_id +'" gdpi_location_id="'+ item.gdpi_location_id +'" available_date="'+ item.available_date +'" value="'+ item.available_date +'" formated_available_date="'+ item.formated_available_date +'">'+ item.formated_available_date +'</option>';
				}			
				$("#gdpi_available_dates").html(html_data);
				$('#gdpi_available_dates').selectpicker('refresh');
			}
		});
	}

	function add_exam_row()
	{	
		$(".exam_row").clone().removeClass("exam_row").appendTo("#span_exam_rows").find("input[type='text']").val("");	
		$(".delete_exam_row").click(function(){						
			delete_exam_row(this);			
		});
	}

	function delete_exam_row(obj)
	{	
		$(obj).closest('.row').remove();
	}

	function back_step5()
	{
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	}

	function save_continue_step5()
	{
		var reg_id = $("#reg_id").val();
		var base_url = $('#baseurl').val();
		var vertical_id = $("#vertical_id").val();

		var ssc_institution = $("#ssc_institution").val();
		var ssc_city = $("#ssc_city").val();
		var ssc_board_university = $("#ssc_board_university").val();
		var ssc_degree = $("#ssc_degree").val();
		var ssc_passing_date = $("#ssc_passing_date").val();
		var ssc_percentage = $("#ssc_percentage").val();
		
		var hsc_institution = $("#hsc_institution").val();
		var hsc_city = $("#hsc_city").val();
		var hsc_board_university = $("#hsc_board_university").val();
		var hsc_stream_specialization = $("#hsc_stream_specialization").val();
		var hsc_degree = $("#hsc_degree").val();
		var hsc_passing_date = $("#hsc_passing_date").val();
		var hsc_percentage = $("#hsc_percentage").val();
		
		var graduation_institution = $("#graduation_institution").val();
		var graduation_city = $("#graduation_city").val();
		var graduation_university = $("#graduation_university").val();
		var graduate_stream_specialization = $("#graduate_stream_specialization").val();
		var graduation_degree = $("#graduation_degree").val();
		var graduation_passing_date = $("#graduation_passing_date").val();
		var graduation_percentage = $("#graduation_percentage").val();
		
		var post_graduation_institution = $("#post_graduation_institution").val();
		var post_graduation_city = $("#post_graduation_city").val();
		var post_graduation_university = $("#post_graduation_university").val();
		var post_graduate_stream_specialization = $("#post_graduate_stream_specialization").val();
		var post_graduation_degree = $("#post_graduation_degree").val();
		var post_graduation_passing_date = $("#post_graduation_passing_date").val();
		var post_graduation_percentage = $("#post_graduation_percentage").val();
		
		var select_test_exams = "";
		$(".select_test_exams").each(function(){
			select_test_exams += $(this).val() + "~|~";	
		});
		
		var test_registration_number = "";
		$(".test_registration_number").each(function(){
			test_registration_number += $(this).val() + "~|~";	
		});
		
		var test_rank = "";
		$(".test_rank").each(function(){
			test_rank += $(this).val() + "~|~";	
		});
		
		var test_score_obtained = "";
		$(".test_score_obtained").each(function(){
			test_score_obtained += $(this).val() + "~|~";	
		});
		
		var test_percentile_obtained = "";
		$(".test_percentile_obtained").each(function(){
			test_percentile_obtained += $(this).val() + "~|~";	
		});
		
		var test_date = "";
		$(".test_date").each(function(){
			test_date += $(this).val() + "~|~";	
		});
		
		var test_result_status = "";
		$(".test_result_status").each(function(){
			test_result_status += $(this).val() + "~|~";	
		});
		
		
		var dataString = 'reg_id=' + escape(reg_id);
		dataString += "&vertical_id="+ escape(vertical_id);
		dataString += "&ssc_institution="+ escape(ssc_institution);
		dataString += "&ssc_city="+ escape(ssc_city);
		dataString += "&ssc_board_university="+ escape(ssc_board_university);
		dataString += "&ssc_degree="+ escape(ssc_degree);
		dataString += "&ssc_passing_date="+ escape(ssc_passing_date);
		dataString += "&ssc_percentage="+ escape(ssc_percentage);
		
		
		dataString += "&hsc_institution="+ escape(hsc_institution);
		dataString += "&hsc_city="+ escape(hsc_city);
		dataString += "&hsc_board_university="+ escape(hsc_board_university);
		dataString += "&hsc_stream_specialization="+ escape(hsc_stream_specialization);
		dataString += "&hsc_degree="+ escape(hsc_degree);
		dataString += "&hsc_passing_date="+ escape(hsc_passing_date);
		dataString += "&hsc_percentage="+ escape(hsc_percentage);
		
			 
		dataString += "&graduation_institution="+ escape(graduation_institution);
		dataString += "&graduation_city="+ escape(graduation_city);
		dataString += "&graduation_university="+ escape(graduation_university);
		dataString += "&graduate_stream_specialization="+ escape(graduate_stream_specialization);
		dataString += "&graduation_degree="+ escape(graduation_degree);
		dataString += "&graduation_passing_date="+ escape(graduation_passing_date);
		dataString += "&graduation_percentage="+ escape(graduation_percentage);
		
		
		dataString += "&post_graduation_institution="+ escape(post_graduation_institution);
		dataString += "&post_graduation_city="+ escape(post_graduation_city);
		dataString += "&post_graduation_university="+ escape(post_graduation_university);
		dataString += "&post_graduate_stream_specialization="+ escape(post_graduate_stream_specialization);
		dataString += "&post_graduation_degree="+ escape(post_graduation_degree);
		dataString += "&post_graduation_passing_date="+ escape(post_graduation_passing_date);
		dataString += "&post_graduation_percentage="+ escape(post_graduation_percentage);
		
		dataString += "&select_test_exams="+ escape(select_test_exams);
		dataString += "&test_registration_number="+ escape(test_registration_number);
		dataString += "&test_rank="+ escape(test_rank);
		dataString += "&test_score_obtained="+ escape(test_score_obtained);
		dataString += "&test_percentile_obtained="+ escape(test_percentile_obtained);
		dataString += "&test_date="+ escape(test_date);
		dataString += "&test_result_status="+ escape(test_result_status);
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/save_continue_step5",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "SUCCESSFUL"){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				}else{
					alert(data);
				}
			}
		});	
	}

	function display_referrer_source()
	{
		var referrer_source = $("#referrer_source").val();
		
		if(referrer_source == ""){
			$(".referrer_source_coaching_class").hide();
			$(".referrer_code_cls").hide();
			
			$("#referrer_coaching_class").val('');
			$("#referrer_city_name").val('');
			$("#referrer_code").val('');
		}else if(referrer_source == "COACHING_CLASSES"){
			$(".referrer_code_cls").hide();
			$(".referrer_source_coaching_class").show();
				
			$("#referrer_coaching_class").val('');
			$("#referrer_city_name").val('');	
			$("#referrer_code").val('');	
		}else {
			$(".referrer_source_coaching_class").hide();
			$(".referrer_code_cls").show();

			$("#referrer_coaching_class").val('');
			$("#referrer_city_name").val('');		
			$("#referrer_code").val('');		
		}
	}

	function back_step6()
	{
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	}

	function save_continue_step6()
	{
		var reg_id = $("#reg_id").val();
		var base_url = $('#baseurl').val();
		var vertical_id = $("#vertical_id").val();
		
		var work_experience_company = "";
		$(".work_experience_company").each(function(){
			work_experience_company += $(this).val() + "~|~";	
		});
		
		var work_experience_designation = "";
		$(".work_experience_designation").each(function(){
			work_experience_designation += $(this).val() + "~|~";	
		});
		
		var work_experience_from_year = "";
		$(".work_experience_from_year").each(function(){
			work_experience_from_year += $(this).val() + "~|~";	
		});
		
		var work_experience_to_year = "";
		$(".work_experience_to_year").each(function(){
			work_experience_to_year += $(this).val() + "~|~";	
		});
		
		var work_experience_monthly_remuneration = "";
		$(".work_experience_monthly_remuneration").each(function(){
			work_experience_monthly_remuneration += $(this).val() + "~|~";	
		});
		
		var work_experience_reason_leaving = "";
		$(".work_experience_reason_leaving").each(function(){
			work_experience_reason_leaving += $(this).val() + "~|~";	
		});
		
		var total_work_experience = $("#total_work_experience").val();
		
		var referrer_name = $("#referrer_name").val();
		var referrer_mobile_no = $("#referrer_mobile_no").val();
		var referrer_email_id = $("#referrer_email_id").val();
		var referrer_is = $("#referrer_is").val();
		var referrer_campus = $("#referrer_campus").val();
		var referrer_source = $("#referrer_source").val();
		var referrer_coaching_class = $("#referrer_coaching_class").val();
		var referrer_city_name = $("#referrer_city_name").val();
		var referrer_code = $("#referrer_code").val();	
		
		var dataString = 'reg_id=' + escape(reg_id);
		dataString += "&vertical_id="+ escape(vertical_id);
		dataString += "&work_experience_company="+ escape(work_experience_company);
		dataString += "&work_experience_designation="+ escape(work_experience_designation);
		dataString += "&work_experience_from_year="+ escape(work_experience_from_year);
		dataString += "&work_experience_to_year="+ escape(work_experience_to_year);
		dataString += "&work_experience_monthly_remuneration="+ escape(work_experience_monthly_remuneration);
		dataString += "&work_experience_reason_leaving="+ escape(work_experience_reason_leaving);
		dataString += "&total_work_experience="+ escape(total_work_experience);
		dataString += "&referrer_name="+ escape(referrer_name);
		dataString += "&referrer_mobile_no="+ escape(referrer_mobile_no);
		dataString += "&referrer_email_id="+ escape(referrer_email_id);
		dataString += "&referrer_is="+ escape(referrer_is);
		dataString += "&referrer_campus="+ escape(referrer_campus);
		dataString += "&referrer_source="+ escape(referrer_source);
		dataString += "&referrer_coaching_class="+ escape(referrer_coaching_class);
		dataString += "&referrer_city_name="+ escape(referrer_city_name);
		dataString += "&referrer_code="+ escape(referrer_code);
		
		$.ajax({
			url:base_url+"index.php/pgdmmr/dashboard/save_continue_step6",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "SUCCESSFUL"){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				}else{
					alert(data);
				}
				
			}
		});	
	}

	function back_step7()
	{
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	}

	function save_continue_step7()
	{	
		var reg_id = $("#reg_id").val();
		var base_url = $('#baseurl').val();
		var vertical_id = $("#vertical_id").val();
		
		var formData = new FormData($("#step7_form")[0]);
		formData.append("reg_id", reg_id);
		
		$.ajax({
			url: base_url+"index.php/pgdmmr/dashboard/save_continue_step7",
			type: "POST",
			data:  formData,
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){var data = data.replace(/^\s+|\s+$/g, '');			
				if(data == "SUCCESSFUL"){
					$('.nav-tabs > .active').next('li').find('a').trigger('click');
				}else{
					alert(data);
				}
			},
			error: function(data){
				alert(data);
			} 	        
		});
	}

	function view_document(obj)
	{
		var reg_id = $(obj).attr("reg_id");
		alert(reg_id);
	}

	