/**
 * File : add.js
 * 
 * This file contain the validation of all (add) forms 
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Priyanka Mhatre
 */

$(document).ready(function(){
	
	
	//for Agent
	var addAgentForm = $("#addagent");
	
	var validator = addAgentForm.validate({
		
		rules:{
			f_name :{ required : true },
			m_name :{ required : true },
			l_name :{ required : true },
			agent_type :{ required : true ,selected : true},
			consultancy_name :{ required : true },
			contact_number : { required : true, digits : true },
			email : { required : true, email : true, remote : { url : baseURL + "index.php/pgdmmr/dashboard/checkEmailExists", type :"post"} },
			campus :{ required : true ,selected : true},
			address :{ required : true },
			city :{ required : true ,selected : true}
		},
		messages:{
			f_name :{ required : "This field is required" },
			m_name :{ required : "This field is required" },
			l_name :{ required : "This field is required" },
			agent_type :{ required : "This field is required", selected : "Please select atleast one option" },
			consultancy_name :{ required : "This field is required" },			
			contact_number : { required : "This field is required", digits : "Please enter numbers only" },
			email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
			campus :{ required : "This field is required" , selected : "Please select atleast one option"},
			address :{ required : "This field is required" },
			city :{ required : "This field is required" ,selected : "Please select atleast one option"}
	
		}
	});
	
	// for agent type
	var addAgentTypeForm = $("#addagentType");
	
	var validator = addAgentTypeForm.validate({
		
		rules:{
			name : { required : true, remote : { url : baseURL + "index.php/pgdmmr/dashboard/checkAgentTypeExists", type :"post"} }
		},
		messages:{
			
			name : { required : "This field is required", remote : "Agent Type already exists" },
		}
	});
	
	//for Campus
	var addCampusForm = $("#addcampus");
	
	var validator = addCampusForm.validate({
		
		rules:{
			campus_name :{ required : true , remote : { url : baseURL + "index.php/pgdmmr/dashboard/checkCampusNameExists", type :"post"}},
			campus_address :{ required : true },
			campus_city :{ required : true ,selected : true},
			campus_state :{ required : true ,selected : true},
			campus_country :{ required : true ,selected : true},
			campus_pin :{ required : true , digits : true },
			campus_contact_number : { required : true, digits : true },
			campus_email_id : { required : true, email : true }
			
		},
		messages:{
			campus_name :{ required : "This field is required" , remote : "Name already exists"  },
			campus_address :{ required : "This field is required" },
			campus_city :{ required : "This field is required", selected : "Please select atleast one option" },
			campus_state :{ required : "This field is required", selected : "Please select atleast one option" },
			campus_country :{ required : "This field is required", selected : "Please select atleast one option" },
			campus_pin :{ required : "This field is required" ,digits : "Please enter numbers only"},			
			campus_contact_number : { required : "This field is required", digits : "Please enter numbers only" },
			campus_email_id : { required : "This field is required", email : "Please enter valid email address" }

		}
	});
	
	//for Campus Specialization
	var addCampusSpecializationForm = $("#addcampus_specialization");
	
	var validator = addCampusSpecializationForm.validate({
		
		rules:{
			specialization_name :{ required : true , remote : { url : baseURL + "index.php/pgdmmr/dashboard/checkCampusSpecializationExists", type :"post"}},
			campus_id :{ required : true ,selected : true},
			specialization_title :{ required : true },
			specialization_description :{ required : true },
			intake :{ required : true , digits : true },
			duration :{ required : true  },
			eligibility :{ required : true  }
						
		},
		messages:{
			specialization_name :{ required : "This field is required" , remote : "Name already exists"  },
			campus_id :{ required : "This field is required", selected : "Please select atleast one option" },
			specialization_title :{ required : "This field is required" },
			specialization_description :{ required : "This field is required" },
			intake :{ required : "This field is required" ,digits : "Please enter numbers only"},	
			duration :{ required : "This field is required" },	
			eligibility :{ required : "This field is required" }
		}
	});
	
	// for Coaching Class
	var addCoachingClassForm = $("#addcoachingclass");
	
	var validator = addCoachingClassForm.validate({
		
		rules:{
			name : { required : true, remote : { url : baseURL + "index.php/pgdmmr/dashboard/checkClassNameExists", type :"post"} }
		},
		messages:{
			
			name : { required : "This field is required", remote : "Class Name already exists" },
		}
	});
	
	
	

});
