$(document ).ready(function() {
		var base_url = $('#baseurl').val();
		
		
		var payment_done = $("#payment_done").val();
		
		if(payment_done == ''){
			//alert('payment_done - null');	
			 $("a[data-toggle='tab'").prop('disabled', true);
			 $("a[data-toggle='tab'").each(function () {
					$(this).prop('data-href', $(this).attr('href')); // hold you original href
					$(this).attr('href', '#'); // clear href
			 });                
			$("a[data-toggle='tab'").addClass('disabled-link');
		}
		
		else if(!payment_done == ''){
			//alert('payment_done - done');
			$('.nav-tabs > .active').next('li').find('a').trigger('click');
			$("a[data-toggle='tab'").prop('disabled', false);
			$("a[data-toggle='tab'").each(function () {
			$(this).attr('href', $(this).prop('data-href')); // restore original href
			});
			$("a[data-toggle='tab'").removeClass('disabled-link');
			
		}
		
		
		$(".show_login_model").click(function(){
			show_login_model();
		});
		
		$(".show_register_model").click(function(){
			show_register_model();
		});

		$(".show_forgot_password").click(function(){
			show_forgot_password();
		});

		$("#btn_reg").click(function(){			
			register_user();
		});

		$("#btn_login").click(function(){
			login_user();
		});

		$("#btn_forgot_password").click(function(){
			forgot_password();
		});
		
		$("#btn_apply_coupon").click(function(){
			apply_coupon();
		});		
		
		$("#mobile_number,#payment_student_mobileno, #stud_mobileno, #father_mobile, #mother_mobile, #total_work_experience").keypress(function(event) {
			return validateNumber(event);
		});
		
		$("#get_captcha").blur(function(){						
		});
		
		$("#btn_refresh_captcha").click(function(){						
			refresh_captcha();
		});	

		$("#step1_continue_btn").click(function(){	

			var valid = 	$('#step1_form').valid();
				if(valid == false){
					return false;
				}
			save_continue_step1();
		});
		
		
		$("#step2_save_exit_btn").click(function(){						
		
				var valid = 	$('#step2_form').valid();
					if(valid == false){
						return false;
					}
				save_continue_step2();			
		});
		
		$("#step2_continue_btn").click(function(){						
		
				var valid = 	$('#step2_form').valid();
					if(valid == false){
						return false;
					}
				save_continue_step2();			
		});
		
		
		
		$("#step3_back_btn").click(function(){						
			back_step3();			
		});
		
		
		
		
		$("#step3_save_exit_btn").click(function(){						
		
				var valid = 	$('#step3_form').valid();
				if(valid == false){
					return false;
				}
			save_continue_step3();			
		});
		
		
		$("#step3_continue_btn").click(function(){		
		
			var valid = 	$('#step3_form').valid();
				if(valid == false){
					//alert('hii');
					return false;
				}
			
			save_continue_step3();			
		});
		
		$("#btn-query-send").click(function(event){	
		
			var valid = 	$('#feedback_form').valid();
				if(valid == false){
					return false;
				}	
				
			ask_me(event);
		});
		
		
		$("#permanent_corres_same").change(function(){
			if(this.checked){
				corres_permanent_add_same();
			}
		});
		
		$("#campus_pref_1").change(function(){
			campus_specialization_1(this);
		});
		
		$("#campus_pref_2").change(function(){
			campus_specialization_2(this);
		});
		
		$("#step4_back_btn").click(function(){						
			back_step4();			
		});
		
		$("#step4_save_exit_btn").click(function(){						
		
				var valid = 	$('#step4_form').valid();
				if(valid == false){
					return false;
				}
			save_continue_step4();			
		});
		
		
		
		$("#step4_continue_btn").click(function(){	

			var valid = 	$('#step4_form').valid();
				if(valid == false){
					alert('hii');
					return false;
				}
			
			save_continue_step4();			
		});

		$("#gdpi_locations").change(function(){						
			get_gdpi_location_available_dates();			
		});

		$("#btn_add_more_exams").click(function(){						
			add_exam_row();			
		});
			
		$(".delete_exam_row").click(function(event){						
			event.stopPropagation();
			delete_exam_row(this);			
		}); 
		
		$("#step5_back_btn").click(function(){						
			back_step5();			
		});
		
		$("#step5_save_exit_btn").click(function(){						
		
				var valid = 	$('#step5_form').valid();
				if(valid == false){
					return false;
				}
			save_continue_step5();			
		});
		
		
		
		
		$("#step5_continue_btn").click(function(){	

			var valid = 	$('#step5_form').valid();
				if(valid == false){
					alert('hii');
					return false;
				}
			save_continue_step5();			
		});
		
		$("#referrer_source").change(function(){						
			display_referrer_source();			
		});
		
		$("#step6_back_btn").click(function(){						
			back_step6();			
		});
		
		
		$("#step6_save_exit_btn").click(function(){						
		
				var valid = 	$('#step6_form').valid();
				if(valid == false){
					return false;
				}
			save_continue_step6();			
		});
		
		
		$("#step6_continue_btn").click(function(){						
			save_continue_step6();			
		});
		
		$("#step7_back_btn").click(function(){						
			back_step7();			
		});
		
		$("#step7_save_exit_btn").click(function(){						
		
				var valid = 	$('#step7_form').valid();
				if(valid == false){
					return false;
				}
			save_continue_step7();			
		});
		
		
		
		
		$("#step7_continue_btn").click(function(){						
			save_continue_step7();			
		});
		
		$("#step8_continue_btn").click(function(){						
			save_continue_step8();			
		});
		
		$("#btn-query-send").click(function(){	
				var valid = 	$('#btn-query-send').valid();
				if(valid == false){
					return false;
				}	
			save_continue_step8();			
		});
		
		
		$("#step6_form").on('submit',(function(e) {			
			e.preventDefault();
			$.ajax({
				url: base_url+"index.php/admission/save_continue_step7",
				type: "POST",
				data:  new FormData(this),
				contentType: false,
				cache: false,
				processData:false,
				success: function(data){
					//alert(data);
				},
				error: function(data){
					//alert(data);
				} 	        
		   });
		}));
		
		$("#password_change").click(function(){
			password_change();			
		});
		
		$('#stud_dob').datepicker({disableTouchKeyboard: false, format: 'dd/mm/yyyy',autoclose: true, todayHighlight: false, enableOnReadonly: true,});
		
		$('#ssc_passing_date, #hsc_passing_date, #graduation_passing_date, #post_graduation_passing_date, .work_experience_from_year,.work_experience_to_year').datepicker({disableTouchKeyboard: false, format: 'mm/yyyy',autoclose: true, todayHighlight: false, enableOnReadonly: true,});
		
		$('.test_date').datepicker({disableTouchKeyboard: false, format: 'dd/mm/yyyy',autoclose: true, todayHighlight: false,});
		
		/* var dataString = "";
		$.ajax({
			url:base_url+"index.php/admission/check_accept_terms_conditions",
			type: "POST",
			data: dataString,
			datatype: "html",
			async: true,
			cache: false,
			success: function(data)
			{
				var data = data.replace(/^\s+|\s+$/g, '');							
				if(data == 'N'){
					$.fancybox({
						href: '#div_accept_terms_conditions', 
						modal: true
					});
					
					$("#btn_accept_terms_conditions").click(function(){
						accept_terms_conditions();
					});	
				}
			}
		}); */
		
		//form validation code
		
		/*  jQuery.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-z ]+$/i.test(value);
			}, "Letters only please");  */ 
		
		$("#step1_form").validate({
			
				
		rules:{
			required:{
				required:true
			},
			payment_student_name:{
				required:true,
				 lettersonly: true
			},
			payment_student_emailid:{
				required:true,
				email:true,
			},
			payment_student_mobileno:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
		
 	$("#step2_form").validate({
		rules:{
			required:{
				required:true
			},
			stud_first_name:{
				required:true,
				 lettersonly: true
			
			},
			stud_middle_name:{
				required:true,
				 lettersonly: true
			},
			stud_last_name:{
				required:true,
				lettersonly: true
			},
			stud_dob:{
				required:true,
			},
			stud_gender:{
				required:true,
			},
			stud_email:{
				required:true,
				email:true,
			},
			stud_mobileno:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			father_first_name:{
				required:true,
				lettersonly: true
			},
			father_middle_name:{
				required:true,
				 lettersonly: true
			},
			father_last_name:{
				required:true,
				 lettersonly: true
			},
			father_mobile:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			mother_first_name:{
				required:true,
				lettersonly: true	
				
			},
			mother_middle_name:{
				required:true,	
				 lettersonly: true
			},
			mother_last_name:{
				required:true,	
				 lettersonly: true
			},
			mother_mobile:{
				required:true,	
				maxlength:10,
				minlength:10,
				digits:true,
			},
			
			
		},
		
		
		/* $('#step2_continue_btn').on('click', function () {
			$('#step2_form').valid();
			}); */
		
/* 		$('#step2_form').valid();
		//$('#stud_first_name').valid();
		var form = $( "#step2_form" );
		form.validate();
		
		  alert( "Valid: " + form.valid() ); */
		
		
		messages:{
			stud_mobileno:'Please enter at least 10 Digits.',
			father_mobile:'Please enter at least 10 Digits.',
			mother_mobile:'Please enter at least 10 Digits.',
			stud_email:'Please Enter Valid Email ID',
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	

	
	 $("#step3_form").validate({
		rules:{
			required:{
				required:true
			},
			corres_address:{
				required:true,	
			},
			permanent_address:{
				required:true,	
			},
			corres_city:{
				required:true,	
			},
			permanent_city:{
				required:true,	
			},
			corres_state:{
				required:true,	
			},
			permanent_state:{
				required:true,	
			},
			corres_state:{
				required:true,	
			},
			corres_pin:{
				required:true,	
				digits:true,
			
				minlength:6,
			},
			permanent_pin:{
				required:true,	
				digits:true,
				
				minlength:6,
			},
			
		},
		messages:{
			corres_pin:'Please enter 6 Digits Pincode.',
			permanent_pin:'Please enter 6 Digits Pincode.'
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	$("#step4_form").validate({
		rules:{
			required:{
				required:true
			},
			campus_pref_1:{
				required:true
			},
			campus_specialization_1:{
				required:true,
			},
			campus_pref_2:{
				notEqualTo: '#campus_pref_1',
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	 $("#step5_form").validate({
		rules:{
			required:{
				required:true
			},
			ssc_institution:{
				required:true
			},
			ssc_city:{
				required:true
			},
			ssc_board_university:{
				required:true
			},
			ssc_degree:{
				required:true
			},
			ssc_passing_date:{
				required:true,
				//date:true,
			},
			ssc_percentage:{
				required:true,
				//digits:true,
			},
			hsc_institution:{
				required:true,
			},
			hsc_city:{
				required:true,
			},
			hsc_board_university:{
				required:true,
			},
			hsc_stream_specialization:{
				required:true,
			},
			hsc_degree:{
				required:true,
			},
			hsc_passing_date:{
				required:true,
			},
			hsc_percentage:{
				required:true,
			},
			graduation_institution:{
				required:true,
			},
			graduation_city:{
				required:true,
			},
			graduation_university:{
				required:true,
			},
			graduate_stream_specialization:{
				required:true,
			},
			graduation_degree:{
				required:true,
			},
			graduation_passing_date:{
				required:true,
			},
			graduation_percentage:{
				required:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	 $("#step8_form").validate({
		rules:{
			decl_applicatnt_name:{
				required:true
			},
			decl_parent_name:{
				required:true
			},
			appl_submit_date:{
				required:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
		$(".print_doc").click(function(){
				print_doc(this);
			});	
			
	
		$("#change_password").validate({
		rules:{
			required:{
				required:true
			},
			current_pwd:{
				required:true
			},
			new_pwd:{
				required:true,
			},
			login_pwd:{
				equalTo: '#new_pwd',
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
	
	
	$("#feedback_form").validate({
		rules:{
			required:{
				required:true
			},
			query_type:{
				required:true
			},
			qry_applicant_student_name:{
				required:true,
			},
			query_category:{
				required:true,
			},
			query_desc:{
				required:true,
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); 
		
	//form valida code end	
	
	$("#feedback-tab").click(function() {
			$("#feedback-form").toggle("slide");
		});
		
		$("#feedback-tab-header").click(function() {
			$("#feedback-form").toggle("slide");
		});
		
		$("#feedback_form").validate({
			rules:{
				required:{
					required:true
				},
				query_type:{
					required:true,
					lettersonly: true
				},
				qry_applicant_student_name:{
					required:true,
					email:true,
				},
				query_category:{
					required:true,									
				},
				query_desc:{
					required:true,										
				},				
			},
			errorClass: "help-inline text-danger",
			errorElement: "span",
			highlight:function(element, errorClass, validClass) {
				$(element).parents('.form-group').addClass('has-error');
			},
			unhighlight: function(element, errorClass, validClass) {
				$(element).parents('.form-group').removeClass('has-error');
				$(element).parents('.form-group').addClass('has-success');
			}
		}); 

		$("#btn-query-send").click(function(event){	
			var valid = 	$('#feedback_form').valid();
				if(valid == false){
					return false;
				}	
			
			ask_me(event);
		});
});
			
function fnEmailVerify(strEmailId)
{	
	strEmail = strEmailId.toLowerCase();

	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(strEmail))
	{
	  return true;
	}
	return false;
}

function validateNumber(event)
{
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode == 8 || event.keyCode == 46
     || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
}

function show_login_model()
{	
	$("#registration_modal").modal("hide")
	$("#forget_password_modal").modal("hide")
    $("#login_modal").modal("show")
	
	$("#registerForm").trigger("reset");
	$("#loginForm").trigger("reset");
	$("#forgetPasswordForm").trigger("reset");
}

function show_register_model()
{	
	$("#login_modal").modal("hide")
	$("#forget_password_modal").modal("hide")
    $("#registration_modal").modal("show")
	
	$("#registerForm").trigger("reset");
	$("#loginForm").trigger("reset");
	$("#forgetPasswordForm").trigger("reset");
}

function show_forgot_password()
{	
	$("#login_modal").modal("hide")
	$("#registration_modal").modal("hide")
	$("#forget_password_modal").modal("show")
	
	$("#registerForm").trigger("reset");
	$("#loginForm").trigger("reset");
	$("#forgetPasswordForm").trigger("reset");
}

function register_user()
{
	var student_full_name = $("#student_full_name").val();
	var vertical_id = $("#vertical_id").val();
	var your_email = $("#your_email").val();
	var mobile_number = $("#mobile_number").val();
	var your_password = $("#your_password").val();
	var choose_state = $("#choose_state").val();
	var choose_city = $("#choose_city").val();
	var get_captcha = $("#get_captcha").val();
	var user_type = $("#user_type").val();
	var agrement_checkbox = false;
	var validate = false;
	
	$(".class_your_name").hide();
	$(".class_your_email").hide();		
	$(".class_your_valid_email").hide();
	$(".class_mobile_number").hide();
	$(".class_your_password").hide();
	$(".class_your_state").hide();
	$(".class_your_state").hide();
	$(".class_your_captcha").hide();
	
	$(".register_error_row").hide();
	$("#register_error").html('');
	
	$(".register_successful_row").hide('');
	
	if(student_full_name == ""){
		$(".class_your_name").show();
		validate = true;
	}
	
	if(your_email == ""){
		$(".class_your_email").show();		
		validate = true;
	}else if(!fnEmailVerify(your_email)){
		$(".class_your_valid_email").show();
		validate = true;
	}
	
	if(mobile_number == ""){
		$(".class_mobile_number").show();
		validate = true;
	}
	
	if(your_password == ""){
		$(".class_your_password").show();
		validate = true;
	}
	
	if(choose_state == ""){
		$(".class_your_state").show();
		validate = true;
	}
	
	if(choose_city == ""){
		
	}
	
	if(get_captcha == ""){
		$(".class_your_captcha").show();		
		validate = true;		
	}
	
	if(validate == true){		
		return;
	}
	
	if(!$("#agrement_checkbox").is(":checked")){		
		$(".register_error_row").show();	
		$("#register_error").html("Please accept terms and conditions!");
		return;
	}
	
	var dataString = 'student_full_name='+escape(student_full_name);
	dataString += "&your_email="+ escape(your_email);
	dataString += "&mobile_number="+ escape(mobile_number);
	dataString += "&your_password="+ escape(your_password);
	dataString += "&choose_state="+ escape(choose_state);
	dataString += "&choose_city="+ escape(choose_city);
	dataString += "&get_captcha="+ escape(get_captcha);
	dataString += "&vertical_id="+ escape(vertical_id);
	dataString += "&user_type="+ escape(user_type);
	
	var base_url = $('#baseurl').val();
	
	//echo base_url;
	$.ajax({
		url:base_url+"index.php/admission/register_user",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		
		success: function(data) 
		{
			alert(data);
			var data = data.replace(/^\s+|\s+$/g, '');				
			var returnData = data.split(',');
			if(returnData[0] == 'FAIL'){
				$(".register_error_row").show();	
				$("#register_error").html(returnData[1]);
			}
			else if(returnData[0] == 'SUCCESSFUL'){
				$(".register_successful_row").show();		
				$("#registerForm").trigger("reset");
			}			
		}
	});	
}

function refresh_captcha()
{				
		var base_url = $('#baseurl').val();
		var dataString = "";
		
		$.ajax({
		url:base_url+"index.php/admission/refresh_captcha",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			$("#captcha_image").html(data);		
			$("#btn_refresh_captcha").click(function(){						
				refresh_captcha();
			});
		}
	});
}


function forgot_password()
{
	$(".forgetpassword_successful_row").hide();
	$(".forgetpassword_emptyfield_row").hide();
	$(".forgetpassword_wrongemail_row").hide();
	$(".forgetpassword_emailnotregistered_row").hide();
	
	var your_email_forgot_password = $("#your_email_forgot_password").val();
		
	if(your_email_forgot_password == ""){
		$(".forgetpassword_emptyfield_row").show();
		return;
	}else if(!fnEmailVerify(your_email_forgot_password)){
		$(".forgetpassword_wrongemail_row").show();
		return;
	}
	
	var dataString = 'your_email_forgot_password='+escape(your_email_forgot_password);	
	var base_url = $('#baseurl').val();
	
	$.ajax({
		url:base_url+"index.php/admission/forgot_password",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				
				$(".forgetpassword_successful_row").show();
				
			}else if(data == "FAIL"){
				
				$(".forgetpassword_emailnotregistered_row").show();
				
			}
		}
	});
	
}

function login_user()
{
	var your_email_login = $("#your_email_login").val();
	var your_password_login = $("#your_password_login").val();
	
	$(".alreadyreg_email_req_row").hide();
	$(".alreadyreg_wrongemailformat_row").hide();
	$(".alreadyreg_password_req_row").hide();
	$(".alreadyreg_uid_pass_wrong_row").hide();
	
	if(your_email_login == ""){
		$(".alreadyreg_email_req_row").show();
		return;
	}else if(!fnEmailVerify(your_email_login)){
		$(".alreadyreg_wrongemailformat_row").show();
		return;
	}
	
	if(your_password_login == ""){
		$(".alreadyreg_password_req_row").show();
		return;
	}
	
	var remember_me = false;
	if($("#remember_login_checkbox").is(":checked")){		
		var remember_me = true;
	}
	
	var dataString = 'your_email_login='+escape(your_email_login);
	dataString += "&your_password_login="+ escape(your_password_login);
	dataString += "&remember_me="+ escape(remember_me);
		
	var base_url = $('#baseurl').val();
	
	$.ajax({
		url:base_url+"index.php/admission/login_user",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');
			
			if(data == "FAIL"){				
				$(".alreadyreg_uid_pass_wrong_row").show();
				return;
			}else if(data == "SUCCESSFUL"){			
				window.location.href = base_url + "index.php/admission/process";
			}
			
		}
	});	
}

function save_continue_step1(){
	
	
		/*$("#step1_form").validate({
		rules:{
			required:{
				required:true
			},
			payment_student_name:{
				required:true,
			},
			payment_student_emailid:{
				required:true,
				email:true,
			},
			payment_student_mobileno:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			
		},
		messages:{
			payment_student_mobileno:'Please enter at least 10 Digits.',
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	});

	$('#step1_form').valid();
*/
	
	
}

function save_continue_step2()
{
	
	/*$("#step2_form").validate({
		rules:{
			required:{
				required:true
			},
			stud_first_name:{
				required:true,
			
			},
			stud_middle_name:{
				required:true,
			},
			stud_last_name:{
				required:true,
			},
			stud_dob:{
				required:true,
			},
			stud_gender:{
				required:true,
			},
			stud_email:{
				required:true,
				email:true,
			},
			stud_mobileno:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			father_first_name:{
				required:true,
			},
			father_middle_name:{
				required:true,
			},
			father_last_name:{
				required:true,
			},
			father_mobile:{
				required:true,
				maxlength:10,
				minlength:10,
				digits:true,
			},
			mother_first_name:{
				required:true,
				
			},
			mother_middle_name:{
				required:true,	
			},
			mother_last_name:{
				required:true,	
			},
			mother_mobile:{
				required:true,	
				maxlength:10,
				minlength:10,
				digits:true,
			},
			
			
		},
		
		/*  submitHandler: function (form) { // for demo
            alert('valid form');
            return false;
        } 
		 
		messages:{
			stud_mobileno:'Please enter at least 10 Digits.',
			father_mobile:'Please enter at least 10 Digits.',
			mother_mobile:'Please enter at least 10 Digits.',
			stud_email:'Please Enter Valid Email ID',
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	
	}); */
	
		//$('#stud_first_name').valid();
		//var form = $( "#step2_form" );
		//form.validate();
		
		  //alert( "Valid: " + form.valid() );
		
	 
		
	//alert ($('#step2_form').valid());
	
	
	
	var payment_student_name = $("#payment_student_name").val();
	var payment_student_emailid = $("#payment_student_emailid").val();
	var payment_student_mobileno = $("#payment_student_mobileno").val();
	var payment_student_finalreg_amount = $("#payment_student_finalreg_amount").val();
	
	var dataString = 'payment_student_name='+escape(payment_student_name);
	dataString += "&payment_student_emailid="+ escape(payment_student_emailid);
	dataString += "&payment_student_mobileno="+ escape(payment_student_mobileno);
	dataString += "&payment_student_finalreg_amount="+ escape(payment_student_finalreg_amount);
	
	var base_url = $('#baseurl').val();
	
	$.ajax({
		url:base_url+"index.php/admission/save_step1",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			$('.nav-tabs > .active').next('li').find('a').trigger('click');			
		}
	});	
	

	
}

function save_continue_step2()
{
	var reg_id = $("#reg_id").val();	
	var stud_title = $("#stud_title").val();
	var stud_first_name = $("#stud_first_name").val();	
	var stud_middle_name = $("#stud_middle_name").val();	
	var stud_last_name = $("#stud_last_name").val();	
	var stud_dob = $("#stud_dob").val();	
	var stud_gender = $("#stud_gender").val();	
	var stud_email = $("#stud_email").val();	
	var stud_mobileno = $("#stud_mobileno").val();	
	var father_first_name = $("#father_first_name").val();	
	var father_middle_name = $("#father_middle_name").val();	
	var father_last_name = $("#father_last_name").val();	
	var father_mobile = $("#father_mobile").val();	
	var mother_first_name = $("#mother_first_name").val();	
	var mother_middle_name = $("#mother_middle_name").val();	
	var mother_last_name = $("#mother_last_name").val();	
	var mother_mobile = $("#mother_mobile").val();	
		
	
	var dataString = 'reg_id='+escape(reg_id);
	dataString += "&stud_title="+ escape(stud_title);
	dataString += "&stud_first_name="+ escape(stud_first_name);
	dataString += "&stud_middle_name="+ escape(stud_middle_name);
	dataString += "&stud_last_name="+ escape(stud_last_name);
	dataString += "&stud_dob="+ escape(stud_dob);
	dataString += "&stud_gender="+ escape(stud_gender);
	dataString += "&stud_email="+ escape(stud_email);
	dataString += "&stud_mobileno="+ escape(stud_mobileno);
	dataString += "&father_first_name="+ escape(father_first_name);
	dataString += "&father_middle_name="+ escape(father_middle_name);
	dataString += "&father_last_name="+ escape(father_last_name);
	dataString += "&father_mobile="+ escape(father_mobile);
	dataString += "&mother_first_name="+ escape(mother_first_name);
	dataString += "&mother_middle_name="+ escape(mother_middle_name);
	dataString += "&mother_last_name="+ escape(mother_last_name);
	dataString += "&mother_mobile="+ escape(mother_mobile);
		
	var base_url = $('#baseurl').val();
	
	$.ajax({
		url:base_url+"index.php/admission/save_step2",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data) 
		{
			var data = data.replace(/^\s+|\s+$/g, '');						
			$('.nav-tabs > .active').next('li').find('a').trigger('click');			
		}
	});	
}

function back_step3()
{
	$('.nav-tabs > .active').prev('li').find('a').trigger('click');
}

function save_continue_step3()
{	


	var reg_id = $("#reg_id").val();
	var corres_address = $("#corres_address").val();
	var permanent_corres_same = $("#permanent_corres_same").val();
	var find_your_location = $("#find_your_location").val();
	var permanent_address = $("#permanent_address").val();
	var unable_find_corres_location = $("#unable_find_corres_location").val();
	var unable_find_perm_location = $("#unable_find_perm_location").val();
	var corres_city = $("#corres_city").val();
	var perm_location_manually = $("#perm_location_manually").val();
	var corres_state = $("#corres_state").val();
	var permanent_city = $("#permanent_city").val();	
	var permanent_state = $("#permanent_state").val();
	var corres_pin = $("#corres_pin").val();
	var permanent_pin = $("#permanent_pin").val();
	
	var permanent_corres_same = ($("#permanent_corres_same").is(":checked") ? "Y" : "N");
	var unable_find_corres_location = ($("#unable_find_corres_location").is(":checked") ? "Y" : "N");
	var unable_find_perm_location = ($("#unable_find_perm_location").is(":checked") ? "Y" : "N");
	
	if(unable_find_corres_location == "Y"){
		corres_address = corres_city;
	}
	
	if(unable_find_perm_location == "Y"){
		permanent_address = permanent_city; 
	}
	
	var dataString = 'reg_id='+escape(reg_id);
	dataString += "&corres_address="+ escape(corres_address);
	dataString += "&permanent_corres_same="+ escape(permanent_corres_same);
	dataString += "&find_your_location="+ escape(find_your_location);
	dataString += "&permanent_address="+ escape(permanent_address);
	dataString += "&unable_find_corres_location="+ escape(unable_find_corres_location);
	dataString += "&unable_find_perm_location="+ escape(unable_find_perm_location);
	dataString += "&corres_city="+ escape(corres_city);
	dataString += "&perm_location_manually="+ escape(perm_location_manually);
	dataString += "&corres_state="+ escape(corres_state);
	dataString += "&permanent_city="+ escape(permanent_city);	
	dataString += "&permanent_state="+ escape(permanent_state);
	dataString += "&corres_pin="+ escape(corres_pin);
	dataString += "&permanent_pin="+ escape(permanent_pin);
	dataString += "&permanent_corres_same="+ escape(permanent_corres_same);
	dataString += "&unable_find_corres_location="+ escape(unable_find_corres_location);
	dataString += "&unable_find_perm_location="+ escape(unable_find_perm_location);
		
	var base_url = $('#baseurl').val();
	
	$.ajax({
		url:base_url+"index.php/admission/save_continue_step3",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				$('.nav-tabs > .active').next('li').find('a').trigger('click');
			}else{
				alert(data);
			}		
		}
	});
}

function corres_permanent_add_same(){
	
	$("#permanent_address").val($("#corres_address").val());
	$("#find_your_perm_location").val($("#find_your_location").val());
	$("#permanent_city").val($("#corres_city").val());
	$("#permanent_state").val($("#corres_state").val());
	$("#permanent_pin").val($("#corres_pin").val());
}

function campus_specialization_1(obj)
{
	var campus_id = $(obj).val();
	var reg_id = $("#reg_id").val();
	
	var base_url = $('#baseurl').val();
	var dataString = 'reg_id=' + escape(reg_id);
	dataString += "&campus_id="+ escape(campus_id);
	
	$.ajax({
		url:base_url+"index.php/admission/campus_specialization_1",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "" || data == null || data == undefined || data == "[]" ){				
				$("#campus_specialization_1").html('<option value=""></option>');
			    $('#campus_specialization_1').selectpicker('refresh');		
				return;
			}
			
			var json_arr = eval(data);			
			var html_data = "";
			for(var i =0;i < json_arr.length-1;i++){
			  var item = json_arr[i];			  
			  html_data += '<option vertical_campus_specialization_id="'+ item.vertical_campus_specialization_id +'" vertical_id="'+ item.vertical_id +'" campus_id="'+ item.campus_id +'" specialization_id="'+ item.specialization_id +'" campus_specialization_id="'+ item.campus_specialization_id +'" value="'+ item.campus_specialization_id +'">'+ item.specialization_name +'</option>';
			}			
			$("#campus_specialization_1").html(html_data);
			$('#campus_specialization_1').selectpicker('refresh');			
		}
	});
}

function campus_specialization_2(obj)
{
	var campus_id = $(obj).val();
	var reg_id = $("#reg_id").val();
	
	
	var base_url = $('#baseurl').val();
	var dataString = 'reg_id=' + escape(reg_id);
	dataString += "&campus_id="+ escape(campus_id);
	
	$.ajax({
		url:base_url+"index.php/admission/campus_specialization_2",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "" || data == null || data == undefined || data == "[]" ){				
				$("#campus_specialization_1").html('<option value=""></option>');
			    $('#campus_specialization_1').selectpicker('refresh');		
				return;
			}
			var json_arr = eval(data);	
			var html_data = "";
			for(var i =0;i < json_arr.length-1;i++){
			  var item = json_arr[i];			  
			  html_data += '<option vertical_campus_specialization_id="'+ item.vertical_campus_specialization_id +'" vertical_id="'+ item.vertical_id +'" campus_id="'+ item.campus_id +'" specialization_id="'+ item.specialization_id +'" campus_specialization_id="'+ item.campus_specialization_id +'" value="'+ item.campus_specialization_id +'">'+ item.specialization_name +'</option>';
			}			
			$("#campus_specialization_2").html(html_data);
			$('#campus_specialization_2').selectpicker('refresh');
		}
	});
}

function back_step4()
{
	$('.nav-tabs > .active').prev('li').find('a').trigger('click');
}

function save_continue_step4()
{

	
		/* $("#step4_form").validate({
		rules:{
			required:{
				required:true
			},
			campus_pref_1:{
				required:true
			},
			campus_specialization_1:{
				required:true,
			},
			campus_pref_2:{
				notEqual: '#campus_pref_1'
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	}); */
	
	//$('#step4_form').valid();
	
	
	var base_url = $('#baseurl').val();
	var reg_id = $("#reg_id").val();
	var campus_pref_1 = $("#campus_pref_1").val();
	var campus_pref_2 = $("#campus_pref_2").val();	
	var campus_specialization_1 = $('#campus_specialization_1').val(); 
	var campus_specialization_2 = $('#campus_specialization_2').val(); 
	var gdpi_locations = $("#gdpi_locations").val();
	var gdpi_available_dates = $('#gdpi_available_dates option:selected').attr('gdpi_location_available_dates_id');
		
	var dataString = 'reg_id=' + escape(reg_id);	
	dataString += "&campus_pref_1="+ escape(campus_pref_1);
	dataString += "&campus_pref_2="+ escape(campus_pref_2);	
	dataString += "&campus_specialization_1="+ escape(campus_specialization_1);	
	dataString += "&campus_specialization_2="+ escape(campus_specialization_2);	
	dataString += "&gdpi_locations="+ escape(gdpi_locations);
	dataString += "&gdpi_available_dates="+ escape(gdpi_available_dates);	
	
	
	if(campus_pref_1 == ""){
		return;
	}
	
	if(campus_specialization_1 == null || campus_specialization_1 == undefined || campus_specialization_1 == ""){
		return;
	}
	/* if(campus_pref_1 == campus_pref_2){
		return;
	} */
	
	$.ajax({
		url:base_url+"index.php/admission/save_continue_step4",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');
			if(data == "SUCCESSFUL"){
				$('.nav-tabs > .active').next('li').find('a').trigger('click');
			}else{
				alert(data);
			}
		}
	});
	
}

function get_gdpi_location_available_dates()
{
	
	var reg_id = $("#reg_id").val();
	var gdpi_locations = $("#gdpi_locations").val();
	
	
	var base_url = $('#baseurl').val();
	var dataString = 'reg_id=' + escape(reg_id);
	dataString += "&gdpi_locations="+ escape(gdpi_locations);
	
	$.ajax({
		url:base_url+"index.php/admission/get_gdpi_location_available_dates",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "" || data == null || data == undefined || data == "[]" ){				
				$("#gdpi_available_dates").html('<option value=""></option>');
			    $('#gdpi_available_dates').selectpicker('refresh');		
				return;
			}
			var json_arr = eval(data);	
			var html_data = "";
			for(var i =0;i < json_arr.length-1;i++){
			  var item = json_arr[i];			  
			  html_data += '<option gdpi_location_available_dates_id="'+ item.gdpi_location_available_dates_id +'" gdpi_location_id="'+ item.gdpi_location_id +'" available_date="'+ item.available_date +'" value="'+ item.available_date +'" formated_available_date="'+ item.formated_available_date +'">'+ item.formated_available_date +'</option>';
			}			
			$("#gdpi_available_dates").html(html_data);
			$('#gdpi_available_dates').selectpicker('refresh');
		}
	});
}

function add_exam_row()
{	
	$(".exam_row").clone().removeClass("exam_row").appendTo("#span_exam_rows").find("input[type='text']").val("");	
	$(".delete_exam_row").click(function(){						
		delete_exam_row(this);			
	});
}

function delete_exam_row(obj)
{	
	$(obj).closest('.row').remove();
}

function back_step5()
{
	$('.nav-tabs > .active').prev('li').find('a').trigger('click');
}

function save_continue_step5()
{
	
	$("#step5_form").validate({
		rules:{
			required:{
				required:true
			},
			ssc_institution:{
				required:true
			},
			ssc_city:{
				required:true
			},
			ssc_board_university:{
				required:true
			},
			ssc_degree:{
				required:true
			},
			ssc_passing_date:{
				required:true,
				date:true,
			},
			ssc_percentage:{
				required:true,
				digits:true,
			},
			hsc_institution:{
				required:true,
			},
			hsc_city:{
				required:true,
			},
			hsc_board_university:{
				required:true,
			},
			hsc_stream_specialization:{
				required:true,
			},
			hsc_degree:{
				required:true,
			},
			hsc_passing_date:{
				required:true,
				date:true,
			},
			hsc_percentage:{
				required:true,
				digits:true,
			},
			graduation_institution:{
				required:true,
			},
			graduation_city:{
				required:true,
			},
			graduation_university:{
				required:true,
			},
			graduate_stream_specialization:{
				required:true,
			},
			graduation_degree:{
				required:true,
			},
			graduation_passing_date:{
				required:true,
				date:true
			},
			graduation_percentage:{
				required:true,
				digits:true
			},
			post_graduation_institution:{
				required:true,
			},
			post_graduation_city:{
				required:true,
			},
			post_graduation_university:{
				required:true,
			},post_graduate_stream_specialization:{
				required:true,
			},
			post_graduation_degree:{
				required:true,
			},
			post_graduation_passing_date:{
				required:true,
				date:true
			},
			post_graduation_percentage:{
				required:true,
				digits:true,
			},
			select_test_exams:{
				required:true,
			},
			test_registration_number:{
				required:true,
			},
			test_rank:{
				required:true,
			},
			test_score_obtained:{
				required:true,
				digits:true
			},
			test_percentile_obtained:{
				required:true,
				digits:true
			},
			test_score_obtained:{
				required:true,
				digits:true
			},
			test_date:{
				required:true,
				date:true
			},
			
		},
		errorClass: "help-inline text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error');
			$(element).parents('.form-group').addClass('has-success');
		}
	});
	
	// $('#step5_form').valid();
	
	var reg_id = $("#reg_id").val();
	var base_url = $('#baseurl').val();
	var vertical_id = $("#vertical_id").val();

	var ssc_institution = $("#ssc_institution").val();
	var ssc_city = $("#ssc_city").val();
	var ssc_board_university = $("#ssc_board_university").val();
	var ssc_degree = $("#ssc_degree").val();
	var ssc_passing_date = $("#ssc_passing_date").val();
	var ssc_percentage = $("#ssc_percentage").val();
	
	var hsc_institution = $("#hsc_institution").val();
	var hsc_city = $("#hsc_city").val();
	var hsc_board_university = $("#hsc_board_university").val();
	var hsc_stream_specialization = $("#hsc_stream_specialization").val();
	var hsc_degree = $("#hsc_degree").val();
	var hsc_passing_date = $("#hsc_passing_date").val();
	var hsc_percentage = $("#hsc_percentage").val();
	
	var graduation_institution = $("#graduation_institution").val();
	var graduation_city = $("#graduation_city").val();
	var graduation_university = $("#graduation_university").val();
	var graduate_stream_specialization = $("#graduate_stream_specialization").val();
	var graduation_degree = $("#graduation_degree").val();
	var graduation_passing_date = $("#graduation_passing_date").val();
	var graduation_percentage = $("#graduation_percentage").val();
	
	var post_graduation_institution = $("#post_graduation_institution").val();
	var post_graduation_city = $("#post_graduation_city").val();
	var post_graduation_university = $("#post_graduation_university").val();
	var post_graduate_stream_specialization = $("#post_graduate_stream_specialization").val();
	var post_graduation_degree = $("#post_graduation_degree").val();
	var post_graduation_passing_date = $("#post_graduation_passing_date").val();
	var post_graduation_percentage = $("#post_graduation_percentage").val();
	
	var select_test_exams = "";
	$(".select_test_exams").each(function(){
		select_test_exams += $(this).val() + "~|~";	
	});
	
	var test_registration_number = "";
	$(".test_registration_number").each(function(){
		test_registration_number += $(this).val() + "~|~";	
	});
	
	var test_rank = "";
	$(".test_rank").each(function(){
		test_rank += $(this).val() + "~|~";	
	});
	
	var test_score_obtained = "";
	$(".test_score_obtained").each(function(){
		test_score_obtained += $(this).val() + "~|~";	
	});
	
	var test_percentile_obtained = "";
	$(".test_percentile_obtained").each(function(){
		test_percentile_obtained += $(this).val() + "~|~";	
	});
	
	var test_date = "";
	$(".test_date").each(function(){
		test_date += $(this).val() + "~|~";	
	});
	
	var test_result_status = "";
	$(".test_result_status").each(function(){
		test_result_status += $(this).val() + "~|~";	
	});
	
	
	var dataString = 'reg_id=' + escape(reg_id);
	dataString += "&vertical_id="+ escape(vertical_id);
	dataString += "&ssc_institution="+ escape(ssc_institution);
	dataString += "&ssc_city="+ escape(ssc_city);
	dataString += "&ssc_board_university="+ escape(ssc_board_university);
	dataString += "&ssc_degree="+ escape(ssc_degree);
	dataString += "&ssc_passing_date="+ escape(ssc_passing_date);
	dataString += "&ssc_percentage="+ escape(ssc_percentage);
	
	
	dataString += "&hsc_institution="+ escape(hsc_institution);
	dataString += "&hsc_city="+ escape(hsc_city);
	dataString += "&hsc_board_university="+ escape(hsc_board_university);
	dataString += "&hsc_stream_specialization="+ escape(hsc_stream_specialization);
	dataString += "&hsc_degree="+ escape(hsc_degree);
	dataString += "&hsc_passing_date="+ escape(hsc_passing_date);
	dataString += "&hsc_percentage="+ escape(hsc_percentage);
	
	     
	dataString += "&graduation_institution="+ escape(graduation_institution);
	dataString += "&graduation_city="+ escape(graduation_city);
	dataString += "&graduation_university="+ escape(graduation_university);
	dataString += "&graduate_stream_specialization="+ escape(graduate_stream_specialization);
	dataString += "&graduation_degree="+ escape(graduation_degree);
	dataString += "&graduation_passing_date="+ escape(graduation_passing_date);
	dataString += "&graduation_percentage="+ escape(graduation_percentage);
	
	
	dataString += "&post_graduation_institution="+ escape(post_graduation_institution);
	dataString += "&post_graduation_city="+ escape(post_graduation_city);
	dataString += "&post_graduation_university="+ escape(post_graduation_university);
	dataString += "&post_graduate_stream_specialization="+ escape(post_graduate_stream_specialization);
	dataString += "&post_graduation_degree="+ escape(post_graduation_degree);
	dataString += "&post_graduation_passing_date="+ escape(post_graduation_passing_date);
	dataString += "&post_graduation_percentage="+ escape(post_graduation_percentage);
	
	dataString += "&select_test_exams="+ escape(select_test_exams);
	dataString += "&test_registration_number="+ escape(test_registration_number);
	dataString += "&test_rank="+ escape(test_rank);
	dataString += "&test_score_obtained="+ escape(test_score_obtained);
	dataString += "&test_percentile_obtained="+ escape(test_percentile_obtained);
	dataString += "&test_date="+ escape(test_date);
	dataString += "&test_result_status="+ escape(test_result_status);
	
	$.ajax({
		url:base_url+"index.php/admission/save_continue_step5",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				$('.nav-tabs > .active').next('li').find('a').trigger('click');
			}else{
				alert(data);
			}
		}
	});	
}

function display_referrer_source()
{
	var referrer_source = $("#referrer_source").val();
	
	if(referrer_source == ""){
		$(".referrer_source_coaching_class").hide();
		$(".referrer_code_cls").hide();
		
		$("#referrer_coaching_class").val('');
		$("#referrer_city_name").val('');
		$("#referrer_code").val('');
	}else if(referrer_source == "COACHING_CLASSES"){
		$(".referrer_code_cls").hide();
		$(".referrer_source_coaching_class").show();
			
		$("#referrer_coaching_class").val('');
		$("#referrer_city_name").val('');	
		$("#referrer_code").val('');	
	}else {
		$(".referrer_source_coaching_class").hide();
		$(".referrer_code_cls").show();

		$("#referrer_coaching_class").val('');
		$("#referrer_city_name").val('');		
		$("#referrer_code").val('');		
	}
}

function back_step6()
{
	$('.nav-tabs > .active').prev('li').find('a').trigger('click');
}

function save_continue_step6()
{
	var reg_id = $("#reg_id").val();
	var base_url = $('#baseurl').val();
	var vertical_id = $("#vertical_id").val();
	
	var work_experience_company = "";
	$(".work_experience_company").each(function(){
		work_experience_company += $(this).val() + "~|~";	
	});
	
	var work_experience_designation = "";
	$(".work_experience_designation").each(function(){
		work_experience_designation += $(this).val() + "~|~";	
	});
	
	var work_experience_from_year = "";
	$(".work_experience_from_year").each(function(){
		work_experience_from_year += $(this).val() + "~|~";	
	});
	
	var work_experience_to_year = "";
	$(".work_experience_to_year").each(function(){
		work_experience_to_year += $(this).val() + "~|~";	
	});
	
	var work_experience_monthly_remuneration = "";
	$(".work_experience_monthly_remuneration").each(function(){
		work_experience_monthly_remuneration += $(this).val() + "~|~";	
	});
	
	var work_experience_reason_leaving = "";
	$(".work_experience_reason_leaving").each(function(){
		work_experience_reason_leaving += $(this).val() + "~|~";	
	});
	
	var total_work_experience = $("#total_work_experience").val();
	
	var referrer_name = $("#referrer_name").val();
	var referrer_mobile_no = $("#referrer_mobile_no").val();
	var referrer_email_id = $("#referrer_email_id").val();
	var referrer_is = $("#referrer_is").val();
	var referrer_campus = $("#referrer_campus").val();
	var referrer_source = $("#referrer_source").val();
	var referrer_coaching_class = $("#referrer_coaching_class").val();
	var referrer_city_name = $("#referrer_city_name").val();
	var referrer_code = $("#referrer_code").val();	
	
	var dataString = 'reg_id=' + escape(reg_id);
	dataString += "&vertical_id="+ escape(vertical_id);
	dataString += "&work_experience_company="+ escape(work_experience_company);
	dataString += "&work_experience_designation="+ escape(work_experience_designation);
	dataString += "&work_experience_from_year="+ escape(work_experience_from_year);
	dataString += "&work_experience_to_year="+ escape(work_experience_to_year);
	dataString += "&work_experience_monthly_remuneration="+ escape(work_experience_monthly_remuneration);
	dataString += "&work_experience_reason_leaving="+ escape(work_experience_reason_leaving);
	dataString += "&total_work_experience="+ escape(total_work_experience);
	dataString += "&referrer_name="+ escape(referrer_name);
	dataString += "&referrer_mobile_no="+ escape(referrer_mobile_no);
	dataString += "&referrer_email_id="+ escape(referrer_email_id);
	dataString += "&referrer_is="+ escape(referrer_is);
	dataString += "&referrer_campus="+ escape(referrer_campus);
	dataString += "&referrer_source="+ escape(referrer_source);
	dataString += "&referrer_coaching_class="+ escape(referrer_coaching_class);
	dataString += "&referrer_city_name="+ escape(referrer_city_name);
	dataString += "&referrer_code="+ escape(referrer_code);
	
	$.ajax({
		url:base_url+"index.php/admission/save_continue_step6",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				$('.nav-tabs > .active').next('li').find('a').trigger('click');
			}else{
				alert(data);
			}
			
		}
	});	
}

function back_step7()
{
	$('.nav-tabs > .active').prev('li').find('a').trigger('click');
}

function save_continue_step7()
{	
	var reg_id = $("#reg_id").val();
	var base_url = $('#baseurl').val();
	var vertical_id = $("#vertical_id").val();
	
	var formData = new FormData($("#step7_form")[0]);
	formData.append("reg_id", reg_id);
	
	$.ajax({
		url: base_url+"index.php/admission/save_continue_step7",
		type: "POST",
		data:  formData,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				$('.nav-tabs > .active').next('li').find('a').trigger('click');
			}else{
				alert(data);
			}
		},
		error: function(data){
			alert(data);
		} 	        
	});
}

function save_continue_step8()
{
		var valid = 	$('#step1_form').valid();

}

function apply_coupon()
{	
	var reg_id = $("#reg_id").val();
	var base_url = $('#baseurl').val();
	var vertical_id = $("#vertical_id").val();	
	var payment_student_reg_amount = $("#payment_student_reg_amount").val();	
	var payment_student_coupon = $("#payment_student_coupon").val();	
	
	$("#coupon_msg").html('').hide();
	
	if(payment_student_coupon == "" || payment_student_coupon == null || payment_student_coupon == undefined){
		return;
	}
	
	var dataString = 'reg_id=' + escape(reg_id);
	dataString += "&vertical_id="+ escape(vertical_id);
	dataString += "&payment_student_reg_amount="+ escape(payment_student_reg_amount);
	dataString += "&payment_student_coupon="+ escape(payment_student_coupon);
	
	$.ajax({
		url: base_url+"index.php/admission/apply_coupon",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data){			
			var data = data.replace(/^\s+|\s+$/g, '');
			
			if(data == "INVALID"){
				$("#coupon_msg").html('<span>You have Invalid Coupon</span>').show();				
			}else if(data >= 0){
				$("#payment_student_finalreg_amount").val(data);
				$("#coupon_msg").html('<span>Discounted amount of form fee is Rs.'+ data +'</span>').show();				
			}			
		},
		error: function(data){
			alert(data);
		} 	        
	});	
}

function accept_terms_conditions()
{
	var reg_id = $("#reg_id").val();
	var base_url = $('#baseurl').val();
	var vertical_id = $("#vertical_id").val();
	var dataString = "";
	
	if(!$("#accept_terms_conditions").is(":checked")){
		alert("Please accept terms and conditions!");	
		return;
	}
	
	$.ajax({
		url:base_url+"index.php/admission/accept_terms_conditions",
		type: "POST",
		data: dataString,
		datatype: "html",
		async: true,
		cache: false,
		success: function(data)
		{
			var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				$("#hdn_accept_terms_conditions").val('Y');				
				parent.$.fancybox.close();
			}
		}
	});
}

function print_doc(obj)
		{	
			var reg_id = $("#reg_id").val();
			var base_url = $('#baseurl').val();	
			
			var dataString = 'reg_id='+escape(reg_id);
		
			//$('.printModal').html("<img src='"+base_url +"/assets/img/ajaxloading.gif' />");
			
			$.ajax({
				url:base_url+"index.php/admission/print_doc",
				type: "POST",
				data: dataString,
				datatype: "html",
				async: true,
				cache: false,
				success : function(data) 
				{
					var data = data.replace(/^\s+|\s+$/g, '');					
					$('.printModal').html(data); 
					$('#printModal').modal('show'); 
				}
			});
		}

function password_change()
{
	//alert('password_change');
	//var reg_id 		= $("#reg_id").val();
	var base_url	 = $('#baseurl').val();
	var current_pwd = $("#current_pwd").val();
	var new_pwd 	= $("#new_pwd").val();
	var login_pwd 	= $("#login_pwd").val();
	
	var dataString = 'current_pwd=' + escape(current_pwd);
	dataString += "&new_pwd="+ escape(new_pwd);
	dataString += "&login_pwd="+ escape(login_pwd);
	alert('dd'+dataString);
	
	$.ajax({
		url: base_url+"index.php/admission/password_change",
		type: "POST",
		data: dataString,
		//datatype: "php",
		async: true,
		cache: false,
		success: function(data)
		{
			alert('sds');
			alert(data);
			//var data = data.replace(/^\s+|\s+$/g, '');			
			if(data == "SUCCESSFUL"){
				$('.nav-tabs > .active').next('li').find('a').trigger('click');
			}
		}
	});
}

function ask_me(event)
{		
	//$("#feedback-form").valid();
	var base_url = $('#baseurl').val();
	var reg_id = $("#reg_id").val();
	var query_type = $("#query_type").val();
	var qry_applicant_student_name = $("#qry_applicant_student_name").val();
	var query_category = $("#query_category").val();
	var query_desc = $("#query_desc").val();	
	
	var dataString = 'query_type='+escape(query_type);	
	dataString += "&reg_id="+ escape(reg_id);	
	dataString += "&qry_applicant_student_name="+ escape(qry_applicant_student_name);	
	dataString += "&query_category="+ escape(query_category);	
	dataString += "&query_desc="+ escape(query_desc);		
	
	alert(dataString)	
	
	var $form = $(this);
	$.ajax({
		type: "POST",
		url: base_url+"index.php/admission/ask_me",
		data: dataString,
		success: function(data) {

			var data = data.replace(/^\s+|\s+$/g, '');
			alert(data);
			if(data = "SUCCESSFUL"){
				$("#feedback-form").find("textarea").val('');
				$("#feedback-form").find("select").val('');
				$("#feedback-form").toggle("slide").find("input").val('');
			}else{
				alert(data);
			}			
		}
	});
	event.preventDefault();
}