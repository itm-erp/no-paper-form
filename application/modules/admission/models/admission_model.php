<?php 
(defined('BASEPATH')) OR exit('No direct script access allowed'); 
/**
 * Description of Model
 *
 * @author Harshal 
 */
 

 include APPPATH . 'third_party/class.phpmailer.php';
 include APPPATH . 'third_party/class.smtp.php';

 
class Admission_model extends CI_Model {
 
    function __construct() 
	{
        parent::__construct();
		$this->load->database();
		$this->load->library('email');	
    }
 
    public function index() 
	{		
		
    }
	
	public function uniqueID() {		
		
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		
		list($usec, $sec) = explode(' ', microtime());
		
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		
		list($usec1, $sec1) = explode(' ', microtime());
		$strUid = str_replace(".", "", $sec1.$usec1);
		return $strUid;
	}
	public function print_doc()
	{
		$reg_id = $_POST['reg_id'];
		//$data_array	= array();
		
		$query = "SELECT IFNULL(a.stud_title, '') as stud_title, IFNULL(a.first_name, '') AS first_name, IFNULL(a.middle_name, '') as middle_name, IFNULL(a.last_name, '') AS last_name, a.dob, IFNULL(a.gender, '') AS gender, IFNULL(a.father_first_name,'') AS father_first_name, IFNULL(a.father_middle_name, '') AS father_middle_name, IFNULL(a.father_last_name, '') AS father_last_name, IFNULL(a.father_occupation, '') AS father_occupation, IFNULL(a.father_mobile, '') AS father_mobile, IFNULL(a.father_email_id, '') AS father_email_id, IFNULL(a.mother_first_name, '') AS mother_first_name, IFNULL(a.mother_middle_name, '') AS mother_middle_name, IFNULL(a.mother_last_name, '') AS mother_last_name, IFNULL(a.mother_occupation, '') AS mother_occupation, IFNULL(a.mother_mobile, '') AS mother_mobile, IFNULL(a.mother_email_id, '') AS mother_email_id, IFNULL(b.permanent_address, '') AS permanent_address, IFNULL(b.permanent_city, '') AS permanent_city, IFNULL(b.permanent_state, '')AS permanent_state, IFNULL(b.permanent_country, '') AS permanent_country, IFNULL(b.permanent_pin, '') AS permanent_pin, IFNULL(b.corres_address, '') AS corres_address, IFNULL(b.corres_city, '') AS corres_city, IFNULL(b.corres_state, '') AS corres_state, 
		IFNULL(b.corres_country, '') AS corres_country, IFNULL(b.corres_pin, '') AS corres_pin, IFNULL(b.permanent_corres_same, '') AS permanent_corres_same, IFNULL(c.student_email_id, '') AS student_email_id, IFNULL(c.student_mobile_no, '') AS student_mobile_no
		FROM student_personal_details AS a
		LEFT JOIN student_address_details AS b ON(a.reg_id = b.reg_id)
		LEFT JOIN student_register AS c ON(a.reg_id = c.reg_id)
		WHERE a.reg_id = '$reg_id'";						
		//echo $query;
		$query_result = $this->db->query($query);
		$res = $query_result->row_array();
		
		
		$data_array['student_personal_address_info'] = $res;
		
		$query = "SELECT IFNULL(a.reg_id,'') AS reg_id, IFNULL(a.campus_id,'') AS campus_id, IFNULL(a.specialization_id, '') 	AS	specialization_id, IFNULL(a.gdpi_location_id, '') AS gdpi_location_id, IFNULL(a.gdpi_prefered_date, '') AS gdpi_prefered_date, 
		IFNULL(b.campus_name, '') AS campus_name, IFNULL(c.specialization_name, '') AS specialization_name, IFNULL(a.preference_level, '') AS preference_level ,IFNULL(d.available_date, '') AS available_date
		FROM student_campus_preferences AS a 
		LEFT JOIN campus_master AS b ON(a.campus_id = b.campus_id) 
		LEFT JOIN campus_specialization AS c ON(a.specialization_id = c.campus_specialization_id)
		LEFT JOIN gdpi_location_available_dates AS d ON(a.gdpi_location_id = d.gdpi_location_available_dates_id)
		WHERE a.reg_id = '$reg_id'";						
		//echo $query;
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();
		
		$data_array['student_campus_specilization_info'] = $res;
			
		$query = "SELECT a.reg_id, a.educational_level, a.institute_name, a.city_name, a.board_university, a.stream_specialization_id,
				a.stream_specialization_gen_txt, a.degree, a.passing_date, a.percentage_obtained,
				b.name as unversity,c.name
				FROM student_academic_details AS a
				LEFT JOIN board_university_master AS b ON(a.board_university = b.board_university_id)
				LEFT JOIN stream_specialization_master AS c ON(a.stream_specialization_id = c.stream_specialization_id)
				LEFT JOIN degree_master AS d ON (a.degree = d.degree_master_id)
				WHERE a.reg_id = '$reg_id'";
		//echo $query;
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();
		
		$data_array['student_academic_info'] = $res;
		
		$query = "SELECT a.stud_work_exp_id, a.company_name, a.designation, a.start_date, a.end_date, a.monthly_salary, a.reason_leaving 
				  FROM student_work_experience AS a
				  WHERE a.reg_id = '$reg_id'";
		//echo $query;
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();
		
		$data_array['student_workexp_info'] = $res;
		
		$query = "SELECT a.stud_refer_id, a.reg_id, a.name, a.mobile_no, a.email_id, a.referrer_is_id, a.referrer_source_id,
					a.coatching_class_id, a.campus_id, a.city, a.other_source,a.name AS std_name,
					b.name AS ref_type, 
					c.name AS ref_source,
					d.name AS class_name 
					FROM student_referrer_details AS a
					LEFT JOIN referrer_is_master AS b ON(a.referrer_is_id = b.referrer_is_id)
					LEFT JOIN referrer_source_master AS c ON(a.referrer_source_id = c.referrer_source_id)
					LEFT JOIN coaching_class_master AS d ON(a.coatching_class_id = d.coaching_class_id)
					LEFT JOIN campus_master AS e ON(a.campus_id = e.campus_id)
					LEFT JOIN city_master AS f ON(a.city = f.city_id)
				    WHERE a.reg_id = '$reg_id'";
		//echo $query;
		$query_result = $this->db->query($query);
		$res = $query_result->row_array();		
		$data_array['student_refer_info'] = $res;		
		
		$this->load->view('application_print_view', $data_array);
	}
	
	public function get_states()
	{
		$query = "SELECT a.state_id as state_id, a.name as state_name, a.status as state_status 
				  FROM state_master as a";
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();	
		return $res;		
	}
	
	public function get_cities()
	{
		$query = "SELECT a.city_id as city_id, a.name as city_name, a.status as city_status 
				  FROM city_master as a";
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();	
		return $res;		
	}
	
	public function get_campuses()
	{
		$query = "SELECT a.campus_id as campus_id, a.vertical_id as vertical_id, a.campus_name as campus_name, a.campus_address as campus_address, 		  a.campus_city as campus_city
				  FROM campus_master as a
				  WHERE a.vertical_id='PGDM'";
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();	
		return $res;		
	}
	
	public function validate_existing_email()
	{		
		$user_email = $this->db->escape($_POST['your_email']);		
		$query = "SELECT * FROM student_register as a
				  WHERE a.student_email_id = $user_email";
		$query_result = $this->db->query($query);
		return $query_result->result();
    }
	
	public function validate_existing_mobile_no()
	{		
		$student_mobile_no = $this->db->escape($_POST['mobile_number']);		
		$query = "SELECT * FROM student_register as a 
				  WHERE a.student_mobile_no = $student_mobile_no";
		$query_result = $this->db->query($query);
		return $query_result->result();
    }
	
	public function student_register()
	{
		$unique_id		= $this->uniqueID();
		
		$vertical_id	= $this->db->escape($_POST['vertical_id']);
		$user_type	= $this->db->escape($_POST['user_type']);
		$student_full_name = $this->db->escape($_POST['student_full_name']);
		$your_email 	= $this->db->escape($_POST['your_email']);
		$mobile_number 	= $this->db->escape($_POST['mobile_number']);
		$your_password 	= $this->db->escape($_POST['your_password']);
		$choose_state 	= $this->db->escape($_POST['choose_state']);
		$choose_city 	= $this->db->escape($_POST['choose_city']);
		$get_captcha 	= $this->db->escape($_POST['get_captcha']);
		$activation_key 	= $this->db->escape(md5(rand(1,1000)));
		
		$query = "INSERT INTO student_register(reg_id, vertical_id, student_full_name, student_email_id, student_mobile_no, student_state, student_city, created_dt) VALUES($unique_id, $vertical_id, $student_full_name, $your_email, $mobile_number, $choose_state, $choose_city, now())";		
		$this->db->query($query);
		
		$query = "INSERT INTO user_master(user_id, user_pass, user_type, user_email, user_mob, activation_key, created_dt) 
								   VALUES($your_email, $your_password, $user_type, $your_email, $mobile_number, $activation_key, now())";		
		$this->db->query($query);		
		
		 $v_url =  base_url() .  'index.php/admission/verify?user_id=' .  urlencode($your_email) . '&a_key=' . urlencode($activation_key);
		
		$email_message = '<html>

							<head></head>

							<body>
								<p>
									<table cellpadding="0" cellpadding="0" style="width:100%" border="0">
										<tr>
											<td style="width:100%">
												Dear '. $student_full_name .'
											</td>
										</tr>

										<tr>
											<td style="width:100%">
												<br/> Thank You for showing interest in applying for ITM Business School Application Form with ITM Group of Institutions.
											</td>
										</tr>

										<tr>
											<td style="width:100%">
												<br/> You are just a click away from your application form. Validate your email address by clicking the link below
											</td>
										</tr>

										<tr>
											<td style="width:100%">
												<br/> '.$v_url.'
											</td>
										</tr>

										<tr>
											<td style="width:100%">
												<br/> If clicking the link doesn\'t work, you can copy and paste the link into your browser\'s address window, or retype it there.
											</td>
										</tr>

										<tr>
											<td style="width:100%">
												<br/> For any admission related queries, please email us at pgdm.admissions@itm.edu or call us on 1800 209 9727.
											</td>
										</tr>

										<tr>
											<td style="width:100%">
												<br/>
												<br/>
												<b>Yours Sincerely,</b>
												<br/> ITM Admissions Team,
												<br/> Plot No. 25/26, Institutional Area, Sector-4, Kharghar, Navi Mumbai- 410210.
												<br/> Call on: 1800-209-9727 E-mail: pgdm.admissions@itm.edu
												<br/>
											</td>
										</tr>
									</table>
								</p>
							</body>

							</html>';
							
			
			
			$mail = new PHPMailer(); 
			$mail->IsSMTP(); 
			$mail->SMTPDebug = 3; 
			$mail->SMTPAuth = true; 
			$mail->SMTPSecure = 'tls'; 
			$mail->Host = "smtp.gmail.com";
			$mail->Port = 587; // or 587
			$mail->IsHTML(true);
			$mail->Username = "darshanr@itmuonline.org";
			$mail->Password = "india@1234@";
			$mail->SetFrom("darshanr@itmuonline.org");
			$mail->AddReplyTo("darshanr@itmuonline.org");
			$mail->Subject = 'ITM Group of Institutions : Verify your Email ID.';
			$mail->Body = $email_message;
			$mail->AddAddress('darshanr@itmuonline.org');

			 if(!$mail->Send()) {
					echo 'send';
			 } else {
				echo 'fail';
				
			 } 			
							

		$email_message_login = '<html>
								<head></head>
								<body>
									Dear Student
										Your Id and Password is as follows
										<br/>
										UserId:&nbsp;&nbsp;'. $your_email .' <br/>
										Password:&nbsp;&nbsp;'. $your_password .'

										<br/>
												<br/>
												<b>Yours Sincerely,</b>
												<br/> ITM Admissions Team,
												<br/> Plot No. 25/26, Institutional Area, Sector-4, Kharghar, Navi Mumbai- 410210.
												<br/> Call on: 1800-209-9727 E-mail: pgdm.admissions@itm.edu
												<br/>
								</body>
							  </html>';
							  
					//$mail = new PHPMailer(); 
					$mail->IsSMTP(); 
					$mail->SMTPDebug = 3; 
					$mail->SMTPAuth = true; 
					$mail->SMTPSecure = 'tls'; 
					$mail->Host = "smtp.gmail.com";
					$mail->Port = 587; 
					$mail->IsHTML(true);
					$mail->Username = "darshanr@itmuonline.org";
					$mail->Password = "india@1234@";
					$mail->SetFrom("darshanr@itmuonline.org");
					$mail->Subject = 'ITM Group of Institutions : Your Login Details.';
					$mail->Body = $email_message_login;
					$mail->AddAddress($your_email);

					 if(!$mail->Send()) {
							echo 'send';
					 } 
					 else {
						echo 'fail';
						
					 } 
		

		return;
	}
	
	public function forgot_password()
	{		
		$your_email_forgot_password	= $this->db->escape($_POST['your_email_forgot_password']);
		$query = "SELECT * FROM user_master as a 
				  WHERE a.user_id = $your_email_forgot_password";
		$query_result = $this->db->query($query);	
		$res = $query_result->result();	
		if(is_array($res) && !empty($res)){
			
			$email_message = '<html>
								<head></head>
								<body>
									Dear Student
										Your Id and Password is as follows
										UserId:
										Password:									
								</body>
							  </html>';
			
			$this->email
				->from('info@example.com', 'ITM Group of Institutions')
				->to($your_email_forgot_password)
				->subject('ITM Group of Institutions : Verify your Email ID.')
				->message($email_message)
				->set_mailtype('html');

			// send email
			//$this->email->send();
			echo "SUCCESSFUL";
		}else{
			echo "FAIL";
		}			
	}
	
	public function login_user()
	{
		$your_email_login	= $this->db->escape($_POST['your_email_login']);
		$your_password_login= $this->db->escape($_POST['your_password_login']);
		$remember_me		= $this->db->escape($_POST['remember_me']);
		$query = "SELECT * FROM user_master as a 
				  WHERE a.user_id = $your_email_login AND a.user_pass = $your_password_login AND a.activation_key = 'Y' AND a.active = 'Y' ";
		$query_result = $this->db->query($query);
		$res = $query_result->row_array();
		
		
		if(is_array($res) && !empty($res)){
			
			$_SESSION['user_id'] = $res['user_id'];
			$_SESSION['user_type'] = $res['user_type'];
			
			$unique_id		= $this->db->escape($this->uniqueID());
			$user_id		= $this->db->escape($_SESSION['user_id']);
			
			$login_ip = $this->db->escape($this->get_client_ip());
		
			$query = "INSERT INTO user_log(log_id, user_id, login_ip, created_by, created_dt) 
			   VALUES($unique_id, $user_id, $login_ip, $user_id, NOW())";
			$this->db->query($query);
			
            return "SUCCESSFUL";
		}else{			
			return "FAIL";
			
		}
	}

	public function get_steps_data()
	{
		$user_id = $_SESSION['user_id'];
		
		$data_arr = array();
		
		
		$query = "SELECT a.vertical_id as vertical_id, a.vertical_name as vertical_name, a.registration_fee as registration_fee 
				  FROM vertical_master AS a
				  WHERE a.vertical_id = 'PGDM'";
		$query_result = $this->db->query($query);
		$vertical_data = $query_result->row_array();
		
		$data_arr['vertical_id'] 		= $vertical_data['vertical_id'];
		$data_arr['vertical_name'] 		= $vertical_data['vertical_name'];
		$data_arr['registration_fee']   = $vertical_data['registration_fee'];
		
		
		$query = "SELECT a.id, a.reg_id as reg_id, a.vertical_id as vertical_id, a.student_full_name as student_full_name, a.student_email_id as student_email_id, a.student_mobile_no as student_mobile_no, a.student_state as student_state, a.student_city as student_city, a.coupon_code as coupon_code, IFNULL(a.accept_terms_conditions, '') as accept_terms_conditions, IFNULL(a.payment_done, '') as payment_done
		FROM student_register AS a
		WHERE a.student_email_id = ". $this->db->escape($user_id);		
		
		$query_result = $this->db->query($query);
		$step_data = $query_result->row_array();
		$data_arr['step_data'] = $step_data;
		
		$reg_id = $step_data['reg_id'];
		
		$query = "SELECT a.stud_per_det_id AS stud_per_det_id, a.reg_id AS reg_id, 
				a.stud_title AS stud_title, a.first_name AS stud_first_name, a.middle_name AS stud_middle_name, a.last_name AS stud_last_name, a.dob AS stud_dob, a.gender AS stud_gender, 
				a.father_first_name AS father_first_name, a.father_middle_name AS father_middle_name, a.father_last_name AS father_last_name, a.father_mobile AS father_mobile, 
				a.mother_first_name AS mother_first_name, a.mother_middle_name AS mother_middle_name, a.mother_last_name AS mother_last_name, a.mother_mobile AS mother_mobile 
				FROM student_personal_details AS a
				WHERE a.reg_id = ". $this->db->escape($reg_id);		
		
		$query_result = $this->db->query($query);
		$step1_data = $query_result->row_array();
		$data_arr['step1_data'] = $step1_data;
		
		$query = "SELECT a.stud_addr_det_id, a.reg_id, a.permanent_address, a.permanent_city, a.permanent_state, a.permanent_country, a.permanent_pin,  
				 a.corres_address, a.corres_city, a.corres_city, a.corres_state, a.corres_country, a.corres_pin, a.permanent_corres_same
                 FROM student_address_details AS a
				 WHERE reg_id=". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);
		$step2_data = $query_result->row_array();
		$data_arr['step2_data'] = $step2_data;
		
		
		$query = "SELECT a.stud_acad_det_id, a.reg_id, a.vertical_id, a.educational_level, a.institute_name, a.city_name, a.board_university,
				 a.stream_specialization_id, a.degree, a.passing_date, a.percentage_obtained 
                 FROM student_academic_details AS a
				 WHERE educational_level = 'SSC' AND reg_id=". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);
		$step5_ssc_data = $query_result->row_array();
		$data_arr['step5_ssc_data'] = $step5_ssc_data;

		$query = "SELECT a.stud_acad_det_id, a.reg_id, a.vertical_id, a.educational_level, a.institute_name, a.city_name, a.board_university,
				 a.stream_specialization_id, a.degree, a.stream_specialization_gen_txt, a.passing_date, a.percentage_obtained 
                 FROM student_academic_details AS a
				 WHERE educational_level = 'HSC' AND reg_id=". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);
		$step5_hsc_data = $query_result->row_array();
		$data_arr['step5_hsc_data'] = $step5_hsc_data;		
		
		$query = "SELECT a.stud_acad_det_id, a.reg_id, a.vertical_id, a.educational_level, a.institute_name, a.city_name, a.board_university,
				 a.stream_specialization_id, a.degree, a.stream_specialization_gen_txt, a.passing_date, a.percentage_obtained 
                 FROM student_academic_details AS a
				 WHERE educational_level = 'GRADUATION' AND reg_id=". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);
		$step5_graduation_data = $query_result->row_array();
		$data_arr['step5_graduation_data'] = $step5_graduation_data;

		$query = "SELECT a.stud_acad_det_id, a.reg_id, a.vertical_id, a.educational_level, a.institute_name, a.city_name, a.board_university,
				 a.stream_specialization_id, a.degree, a.stream_specialization_gen_txt, a.passing_date, a.percentage_obtained 
                 FROM student_academic_details AS a
				 WHERE educational_level = 'POST_GRADUATION' AND reg_id=". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);
		$step5_post_graduation_data = $query_result->row_array();
		$data_arr['step5_post_graduation_data'] = $step5_post_graduation_data;		
		
		
		$data_arr['campuses_data'] = $this->get_campuses();			
		$data_arr['gdpi_locations'] = $this->get_gdpi_locations();
		$data_arr['degree_master'] = $this->get_degree_master();
		$data_arr['stream_specialization_master'] = $this->get_stream_specialization_master();
		$data_arr['board_university_master'] = $this->get_board_university_master();		  	
		$data_arr['exam_test_master'] = $this->get_exam_test_master();		  	
		$data_arr['coaching_class_master'] = $this->get_coaching_class_master();	
		$data_arr['referrer_source_master'] = $this->get_referrer_source_master();
		$data_arr['referrer_is_master'] = $this->get_referrer_is_master();
		
		$query = "SELECT a.adm_doc_id, a.reg_id, a.doc_type, a.title, a.description, a.doc_physical_path
				  FROM admission_document_master AS a				  
				  WHERE a.reg_id = ". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();
	  	$data_arr['admission_document'] = $res;

		$query = "SELECT a.stud_work_exp_id, a.reg_id, a.company_name, a.designation, a.start_date, a.end_date, a.monthly_salary,
				  a.reason_leaving	
				  FROM student_work_experience AS a				  
				  WHERE a.reg_id = ". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();
	  	$data_arr['student_work_experience'] = $res;

		$query = "SELECT a.stud_refer_id, a.reg_id, a.name, a.mobile_no, a.email_id, a.referrer_is_id,
				  a.referrer_source_id, a.coatching_class_id, a.campus_id, a.city, a.other_source	
				  FROM student_referrer_details AS a				  
				  WHERE a.reg_id = ". $this->db->escape($reg_id);				
		$query_result = $this->db->query($query);	
		$res = $query_result->row_array();
	  	$data_arr['student_referrer_details'] = $res;
		
		$query = "SELECT a.stud_camp_pref_id, a.reg_id, a.campus_id, a.specialization_id, a.gdpi_location_id, a.gdpi_prefered_date,
				  a.gdpi_prefered_date
				  FROM student_campus_preferences AS a				  
				  WHERE a.reg_id = ". $this->db->escape($reg_id);
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();
	  	$data_arr['student_campus_preferences'] = $res;
		
		$campus_id_1 = "";
		$campus_id_2 = "";		
		$gdpi_location_id = "";
		
		if($res[0]['campus_id'] != "" && $res[0]['campus_id'] != null){
			$campus_id_1 = $res[0]['campus_id'];
		}
		
		if($res[1]['campus_id'] != "" && $res[1]['campus_id'] != null){
			$campus_id_2 = $res[1]['campus_id'];
		}
		
		if($res[0]['gdpi_location_id'] != "" && $res[0]['gdpi_location_id'] != null){
			$gdpi_location_id = $res[0]['gdpi_location_id'];
		}
		
		$data_arr['campus_specialization_11'] = $this->campus_specialization_11($campus_id_1);
		$data_arr['campus_specialization_22'] = $this->campus_specialization_22($campus_id_2);				
		$data_arr['get_gdpi_locations_date'] = $this->get_gdpi_locations_date($gdpi_location_id);

		$query = "SELECT a.stud_test_id, a.reg_id, a.exam_test_id, a.exam_test_reg_num, a.rank, a.score_obtained, a.percentage_obtained,
				  a.test_date, a.result_status, b.name
                  FROM student_test_details AS a
                  LEFT JOIN exam_test_master AS b ON (a.exam_test_id = b.exam_test_id)
                  WHERE a.reg_id = ". $this->db->escape($reg_id);
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();
	  	$data_arr['student_test_details'] = $res;
		
		//$data_arr['check_accept_terms_conditions'] = $this->check_accept_terms_conditions();
		
		return $data_arr;
	}

	public function save_step1()
	{
		$user_id = $_SESSION['user_id'];
		
		$payment_student_name = $this->db->escape($_POST['payment_student_name']);
		$payment_student_emailid = $this->db->escape($_POST['payment_student_emailid']);
		$payment_student_mobileno = $this->db->escape($_POST['payment_student_mobileno']);
		$payment_student_finalreg_amount = $this->db->escape($_POST['payment_student_finalreg_amount']);
		
		$query = "UPDATE student_register SET student_full_name = $payment_student_name, student_mobile_no=$payment_student_mobileno, registration_fee=$payment_student_finalreg_amount 		
		WHERE student_email_id = ". $this->db->escape($user_id);
		
		$this->db->query($query);
		return;		
	}
	
	public function save_step2()
	{	
		$unique_id		= $this->uniqueID();
		
		$user_id = $_SESSION['user_id'];
		
		$reg_id = $this->db->escape($_POST['reg_id']);
		$stud_title = $this->db->escape($_POST['stud_title']);
		$stud_first_name = $this->db->escape($_POST['stud_first_name']);
		$stud_middle_name = $this->db->escape($_POST['stud_middle_name']);
		$stud_last_name = $this->db->escape($_POST['stud_last_name']);
		$stud_dob = $_POST['stud_dob'];
		$stud_gender = $this->db->escape($_POST['stud_gender']);
		$stud_email = $this->db->escape($_POST['stud_email']);
		$stud_mobileno = $this->db->escape($_POST['stud_mobileno']);
		$father_first_name = $this->db->escape($_POST['father_first_name']);
		$father_middle_name = $this->db->escape($_POST['father_middle_name']);
		$father_last_name = $this->db->escape($_POST['father_last_name']);
		$father_mobile = $this->db->escape($_POST['father_mobile']);
		$mother_first_name = $this->db->escape($_POST['mother_first_name']);
		$mother_middle_name = $this->db->escape($_POST['mother_middle_name']);
		$mother_last_name = $this->db->escape($_POST['mother_last_name']);
		$mother_mobile = $this->db->escape($_POST['mother_mobile']);
		
		
		$stud_dob_arr = explode('/', $stud_dob);
		$stud_dob_arr_str = date($stud_dob_arr[2] ."/". $stud_dob_arr[1] ."/". $stud_dob_arr[0] . " 00:00:00");
		$stud_dob_arr_str = $this->db->escape($stud_dob_arr_str);
		
		$query = "SELECT a.reg_id AS reg_id 
				  FROM student_personal_details as a
				  WHERE a.reg_id = $reg_id" ;
				  
		$query_result = $this->db->query($query);
		$result_data = $query_result->row_array();
		if(is_array($result_data) && !empty($result_data)){
			
			$query = "UPDATE student_personal_details SET stud_title = $stud_title, first_name = $stud_first_name, middle_name = $stud_middle_name, last_name = $stud_last_name, dob = $stud_dob_arr_str, gender = $stud_gender, father_first_name=$father_first_name, father_middle_name=$father_middle_name, father_last_name=$father_last_name, father_mobile=$father_mobile, mother_first_name=$mother_first_name, mother_middle_name=$mother_middle_name, mother_last_name=$mother_last_name, mother_mobile=$mother_mobile, modified_by=". $this->db->escape($user_id) .", modified_dt=NOW() 
			WHERE reg_id = $reg_id";			
			$this->db->query($query);
			return;		
			
		}else{			
			$query = "INSERT INTO student_personal_details(stud_per_det_id, reg_id, stud_title, first_name, middle_name, last_name, dob, gender, father_first_name, father_middle_name, father_last_name, father_mobile, mother_first_name, mother_middle_name, mother_last_name, mother_mobile, created_by, created_dt) VALUES($unique_id, $reg_id, $stud_title, $stud_first_name, $stud_middle_name, $stud_last_name, $stud_dob, $stud_gender, $father_first_name, $father_middle_name, $father_last_name, $father_mobile, $mother_first_name, $mother_middle_name, $mother_last_name, $mother_mobile, ". $this->db->escape($reg_id) .", NOW())";
			$this->db->query($query);
			return;			
		}			
	}

	public function save_continue_step3()
	{
		
		$unique_id		= $this->uniqueID();
		
		$user_id = $_SESSION['user_id'];
		
		$reg_id = $this->db->escape($_POST['reg_id']);
		$corres_address = $this->db->escape($_POST['corres_address']);
		$permanent_corres_same = $this->db->escape($_POST['permanent_corres_same']);
		$find_your_location = $this->db->escape($_POST['find_your_location']);
		$permanent_address = $this->db->escape($_POST['permanent_address']);
		$unable_find_corres_location = $this->db->escape($_POST['unable_find_corres_location']);
		$unable_find_perm_location = $this->db->escape($_POST['unable_find_perm_location']);
		$corres_city = $this->db->escape($_POST['corres_city']);
		$perm_location_manually = $this->db->escape($_POST['perm_location_manually']);
		$corres_state = $this->db->escape($_POST['corres_state']);
		$permanent_city = $this->db->escape($_POST['permanent_city']);		
		$permanent_state = $this->db->escape($_POST['permanent_state']);
		$corres_pin = $this->db->escape($_POST['corres_pin']);
		$permanent_pin = $this->db->escape($_POST['permanent_pin']);
				
		$query = "SELECT a.reg_id AS reg_id 
				  FROM student_address_details as a
				  WHERE a.reg_id = $reg_id" ;
				  
		$query_result = $this->db->query($query);
		$result_data = $query_result->row_array();
		if(is_array($result_data) && !empty($result_data)){
			
			$query = "UPDATE student_address_details SET permanent_address=$permanent_address, permanent_city=$permanent_city, permanent_state=$permanent_state, permanent_pin=$permanent_pin, corres_address=$corres_address, corres_city=$corres_city, corres_state=$corres_state, corres_pin=$corres_pin, permanent_corres_same=$permanent_corres_same, modified_by=". $this->db->escape($user_id) .", modified_dt=NOW()
			WHERE reg_id = $reg_id";					
			$this->db->query($query);			
			
		}else{		
		
			$query = "INSERT INTO student_address_details(stud_addr_det_id, reg_id, permanent_address, permanent_city, permanent_state, permanent_pin, corres_address, corres_city, corres_state,  corres_pin, permanent_corres_same, created_by, created_dt) VALUES($unique_id, $reg_id, $permanent_address, $permanent_city, $permanent_state, $permanent_pin, $corres_address, $corres_city, $corres_state, $corres_pin, $permanent_corres_same, ". $this->db->escape($reg_id) .", NOW())";			
			$this->db->query($query);
		}
		echo "SUCCESSFUL";	
		return;
	}
	
	public function campus_specialization_1()
	{
		$campus_id = $_POST['campus_id'];
		
		$query = "SELECT a.vertical_campus_specialization_id AS vertical_campus_specialization_id, a.vertical_id AS vertical_id, a.campus_id AS campus_id, 
				  a.specialization_id AS specialization_id, 
				  b.campus_specialization_id AS campus_specialization_id, b.campus_id AS campus_id, 
				  b.specialization_name, b.specialization_title, b.specialization_description	
				  FROM  vertical_campus_specialization_master AS a
				  LEFT JOIN campus_specialization AS b ON(a.campus_id = b.campus_id)
				  WHERE a.campus_id = ". $this->db->escape($campus_id) ."
				  GROUP BY b.campus_specialization_id";					
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();		
		$res = json_encode($res);
		return $res;
	}
	
	public function campus_specialization_11($campus_id)
	{
		$query = "SELECT a.vertical_campus_specialization_id AS vertical_campus_specialization_id, a.vertical_id AS vertical_id, a.campus_id AS campus_id, 
				  a.specialization_id AS specialization_id, 
				  b.campus_specialization_id AS campus_specialization_id, b.campus_id AS campus_id, 
				  b.specialization_name, b.specialization_title, b.specialization_description	
				  FROM  vertical_campus_specialization_master AS a
				  LEFT JOIN campus_specialization AS b ON(a.campus_id = b.campus_id)
				  WHERE a.campus_id = ". $this->db->escape($campus_id) ."
				  GROUP BY b.campus_specialization_id";					
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();				
		return $res;
	}
	
	public function campus_specialization_2()
	{
		$campus_id = $_POST['campus_id'];
		
		$query = "SELECT a.vertical_campus_specialization_id AS vertical_campus_specialization_id, a.vertical_id AS vertical_id, a.campus_id AS campus_id, 
				  a.specialization_id AS specialization_id, 
				  b.campus_specialization_id AS campus_specialization_id, b.campus_id AS campus_id, 
				  b.specialization_name, b.specialization_title, b.specialization_description	
				  FROM  vertical_campus_specialization_master AS a
				  LEFT JOIN campus_specialization AS b ON(a.campus_id = b.campus_id)
				  WHERE a.campus_id = ". $this->db->escape($campus_id) ."
				  GROUP BY b.campus_specialization_id";				
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();
		$res = json_encode($res);	
		return $res;
	}
	
	public function campus_specialization_22($campus_id)
	{	
		$query = "SELECT a.vertical_campus_specialization_id AS vertical_campus_specialization_id, a.vertical_id AS vertical_id, a.campus_id AS campus_id, 
				  a.specialization_id AS specialization_id, 
				  b.campus_specialization_id AS campus_specialization_id, b.campus_id AS campus_id, 
				  b.specialization_name, b.specialization_title, b.specialization_description	
				  FROM  vertical_campus_specialization_master AS a
				  LEFT JOIN campus_specialization AS b ON(a.campus_id = b.campus_id)
				  WHERE a.campus_id = ". $this->db->escape($campus_id) ."
				  GROUP BY b.campus_specialization_id";				
		$query_result = $this->db->query($query);	
		$res = $query_result->result_array();				
		return $res;
	}
	
	public function get_gdpi_locations()
	{		
		$query = "SELECT a.gdpi_location_id, a.name, a.description
				  FROM gdpi_location_master a";				  
		$query_result = $this->db->query($query);
		$result_data = $query_result->result_array();
		return $result_data;
	}
	
	public function get_gdpi_locations_date($gdpi_location_id)
	{		
		$gdpi_location_id = $this->db->escape($gdpi_location_id);
		
		$query = "SELECT a.gdpi_location_available_dates_id, a.gdpi_location_id, a.available_date,
				  a.status
				  FROM gdpi_location_available_dates a
				  WHERE a.status= 'Y' AND a.gdpi_location_id = " . $gdpi_location_id;
		$query_result = $this->db->query($query);
		$result_data = $query_result->result_array();
		return $result_data;
	}
	
	public function save_continue_step4()
	{
		$user_id = $_SESSION['user_id'];
		
		$reg_id = $this->db->escape($_POST['reg_id']);
		$campus_pref_1 = $this->db->escape($_POST['campus_pref_1']);
		$campus_pref_2 = $this->db->escape($_POST['campus_pref_2']);
		$campus_specialization_1 = str_replace("'", "", $_POST['campus_specialization_1']);
		$campus_specialization_2 = str_replace("'", "", $_POST['campus_specialization_2']);
		$gdpi_locations = $this->db->escape($_POST['gdpi_locations']);
		$gdpi_available_dates = $this->db->escape($_POST['gdpi_available_dates']);
		
		$query = "DELETE FROM student_campus_preferences
				  WHERE reg_id = $reg_id";				  
		$this->db->query($query);
		
		$arr_campus_specialization_1 = explode(',', $campus_specialization_1);
		if(is_array($arr_campus_specialization_1) && !empty($arr_campus_specialization_1)){
			foreach($arr_campus_specialization_1 as $val){
				
				$unique_id			= $this->db->escape($this->uniqueID());
				$specialization_id	= $this->db->escape($val);
				
				if($campus_pref_1 != "" && $campus_pref_1 != null && $specialization_id != "" && $specialization_id != null){
					$query = "INSERT INTO student_campus_preferences(stud_camp_pref_id, reg_id, campus_id, specialization_id, gdpi_location_id, gdpi_prefered_date, created_by, created_dt) 
							  VALUES($unique_id, $reg_id, $campus_pref_1, $specialization_id, $gdpi_locations, $gdpi_available_dates, ". $this->db->escape($user_id) .", now())";				  									
					$this->db->query($query);		  
				}
			}
		}
		
		$arr_campus_specialization_2 = explode(',', $campus_specialization_2);		
		if(is_array($arr_campus_specialization_2) && !empty($arr_campus_specialization_2)){
			foreach($arr_campus_specialization_2 as $val){
				
				$unique_id		= $this->uniqueID();
				$specialization_id	= $this->db->escape($val);
				
				if($campus_pref_2 != "" && $campus_pref_2 != null && $specialization_id != "" && $specialization_id != null){
				$query = "INSERT INTO student_campus_preferences(stud_camp_pref_id, reg_id, campus_id, specialization_id, gdpi_location_id, gdpi_prefered_date, created_by, created_dt) 
						  VALUES(". $this->db->escape($unique_id) .", $reg_id, $campus_pref_2, $specialization_id, $gdpi_locations, $gdpi_available_dates, ". $this->db->escape($user_id) .", now())";						  
				$this->db->query($query);	
				}				
			}
		}
		
		echo "SUCCESSFUL";
		return;	
	}
	
	public function get_gdpi_location_available_dates()
	{
		$user_id = $_SESSION['user_id'];
		
		$reg_id = $this->db->escape($_POST['reg_id']);
		$gdpi_locations = $this->db->escape($_POST['gdpi_locations']);
		
		$query = "SELECT a.gdpi_location_available_dates_id, a.gdpi_location_id, a.available_date, a.status,
				  DATE_FORMAT(a.available_date, '%e/%m/%Y') AS formated_available_date
				  FROM gdpi_location_available_dates AS a
				  WHERE a.gdpi_location_id = $gdpi_locations";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();	
		$res = json_encode($res);
		return $res;
	}
	
	public function get_degree_master()
	{
		$query = "SELECT a.degree_master_id, a.name, a.status
				  FROM degree_master as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;
	}
	
	public function get_stream_specialization_master()
	{
		$query = "SELECT a.stream_specialization_id, a.name, a.status
				  FROM stream_specialization_master	as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;		
	}
	
	public function get_board_university_master()
	{
		$query = "SELECT a.board_university_id, a.name, a.status
				  FROM board_university_master	as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;
	}
	
	public function get_exam_test_master()
	{
		$query = "SELECT a.exam_test_id, a.name, a.status
				  FROM exam_test_master as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;
	}
	
	public function save_continue_step5()
	{		
		$user_id = $this->db->escape($_SESSION['user_id']);
		
		$unique_id		= $this->db->escape($this->uniqueID());	
		$vertical_id		= $this->db->escape($_POST['vertical_id']);	
		
		$reg_id 			= $this->db->escape($_POST['reg_id']);
		$ssc_institution = $this->db->escape($_POST['ssc_institution']);
		$ssc_city 		= $this->db->escape($_POST['ssc_city']);
		$ssc_board_university = $this->db->escape($_POST['ssc_board_university']);
		$ssc_degree 		= $this->db->escape($_POST['ssc_degree']);
		$ssc_passing_date = $_POST['ssc_passing_date'];
		$ssc_percentage 	= $this->db->escape($_POST['ssc_percentage']);
		
		$hsc_institution = $this->db->escape($_POST['hsc_institution']);
		$hsc_city 		= $this->db->escape($_POST['hsc_city']);
		$hsc_board_university = $this->db->escape($_POST['hsc_board_university']);
		$hsc_stream_specialization = $this->db->escape($_POST['hsc_stream_specialization']);
		$hsc_degree 		= $this->db->escape($_POST['hsc_degree']);
		$hsc_passing_date = $_POST['hsc_passing_date'];
		$hsc_percentage 	= $this->db->escape($_POST['hsc_percentage']);
		
		$graduation_institution = $this->db->escape($_POST['graduation_institution']);
		$graduation_city = $this->db->escape($_POST['graduation_city']);
		$graduation_university = $this->db->escape($_POST['graduation_university']);
		$graduate_stream_specialization = $this->db->escape($_POST['graduate_stream_specialization']);
		$graduation_degree = $this->db->escape($_POST['graduation_degree']);
		$graduation_passing_date = $_POST['graduation_passing_date'];
		$graduation_percentage = $this->db->escape($_POST['graduation_percentage']);
		
		$post_graduation_institution = $this->db->escape($_POST['post_graduation_institution']);
		$post_graduation_city = $this->db->escape($_POST['post_graduation_city']);
		$post_graduation_university = $this->db->escape($_POST['post_graduation_university']);
		$post_graduate_stream_specialization = $this->db->escape($_POST['post_graduate_stream_specialization']);
		$post_graduation_degree = $this->db->escape($_POST['post_graduation_degree']);
		$post_graduation_passing_date = $_POST['post_graduation_passing_date'];
		$post_graduation_percentage = $this->db->escape($_POST['post_graduation_percentage']);
		
		$select_test_exams = $_POST['select_test_exams'];
		$test_registration_number = $_POST['test_registration_number'];
		$test_rank 		= $_POST['test_rank'];
		$test_score_obtained = $_POST['test_score_obtained'];
		$test_percentile_obtained = $_POST['test_percentile_obtained'];
		$test_date 		= $_POST['test_date'];
		$test_result_status = $_POST['test_result_status'];
		
		$ssc_passing_date_arr = explode('/', $ssc_passing_date);
		$ssc_passing_date_str = date($ssc_passing_date_arr[1] ."-". $ssc_passing_date_arr[0] ."-01 00:00:00");
		$ssc_passing_date_str = $this->db->escape($ssc_passing_date_str);
		
		$hsc_passing_date_arr = explode('/', $hsc_passing_date);
		$hsc_passing_date_str = date($hsc_passing_date_arr[1] ."-". $hsc_passing_date_arr[0] ."-01 00:00:00");
		$hsc_passing_date_str = $this->db->escape($hsc_passing_date_str);
		
		$graduation_passing_date_arr = explode('/', $graduation_passing_date);
		$graduation_passing_date_str = date($graduation_passing_date_arr[1] ."-". $graduation_passing_date_arr[0] ."-01 00:00:00");
		$graduation_passing_date_str = $this->db->escape($graduation_passing_date_str);
		
		$post_graduation_passing_date_arr = explode('/', $post_graduation_passing_date);
		$post_graduation_passing_date_str = date($post_graduation_passing_date_arr[1] ."-". $post_graduation_passing_date_arr[0] ."-01 00:00:00");
		$post_graduation_passing_date_str = $this->db->escape($post_graduation_passing_date_str);		

		$query = "DELETE FROM student_academic_details
			      WHERE reg_id = $reg_id";				  
		$this->db->query($query);	
		
		$query = "INSERT INTO student_academic_details(stud_acad_det_id, reg_id, vertical_id, educational_level, institute_name, city_name, board_university, degree, passing_date, percentage_obtained, created_by, created_dt) VALUES($unique_id, $reg_id, $vertical_id, 'SSC', $ssc_institution, $ssc_city, $ssc_board_university, $ssc_degree, $ssc_passing_date_str, $ssc_percentage, $user_id, now())";
		$this->db->query($query);
		
		$unique_id		= $this->db->escape($this->uniqueID());
		$query = "INSERT INTO student_academic_details(stud_acad_det_id, reg_id, vertical_id, educational_level, institute_name, city_name, board_university, degree, stream_specialization_id, passing_date, percentage_obtained, created_by, created_dt) 
		VALUES($unique_id, $reg_id, $vertical_id, 'HSC', $hsc_institution, $hsc_city, $hsc_board_university, $hsc_degree, $hsc_stream_specialization, $hsc_passing_date_str, $hsc_percentage, $user_id, now())";
		$this->db->query($query);
				
		$unique_id		= $this->db->escape($this->uniqueID());
		$query = "INSERT INTO student_academic_details(stud_acad_det_id, reg_id, vertical_id, educational_level, institute_name, city_name, board_university, degree, stream_specialization_gen_txt, passing_date, percentage_obtained, created_by, created_dt) 
		VALUES($unique_id, $reg_id, $vertical_id, 'GRADUATION', $graduation_institution, $graduation_city, $graduation_university, $graduation_degree, $graduate_stream_specialization, $graduation_passing_date_str, $graduation_percentage, $user_id, now())";
		$this->db->query($query);
				
		$unique_id		= $this->db->escape($this->uniqueID());
		$query = "INSERT INTO student_academic_details(stud_acad_det_id, reg_id, vertical_id, educational_level, institute_name, city_name, board_university, degree, stream_specialization_gen_txt, passing_date, percentage_obtained, created_by, created_dt) VALUES($unique_id, $reg_id, $vertical_id, 'POST_GRADUATION', $post_graduation_institution, $post_graduation_city, $post_graduation_university, $post_graduation_degree, $post_graduate_stream_specialization, $post_graduation_passing_date_str, $post_graduation_percentage, $user_id, now())";
		$this->db->query($query);		
		
		$arr_select_test_exams = explode('~|~', $select_test_exams);
		$arr_test_registration_number = explode('~|~', $test_registration_number);
		$arr_test_rank 			= explode('~|~', $test_rank);
		$arr_test_score_obtained = explode('~|~', $test_score_obtained);
		$arr_test_percentile_obtained = explode('~|~', $test_percentile_obtained);
		$arr_test_date 			= explode('~|~', $test_date);
		$arr_test_result_status = explode('~|~', $test_result_status);
		
		$query = "DELETE FROM student_test_details
			      WHERE reg_id = $reg_id";				  
		$this->db->query($query);	
		
		foreach($arr_select_test_exams as $key=>$value){
			if($value != null && $value != ""){
				$unique_id		= $this->db->escape($this->uniqueID());	
				
				$val_exam_test_id = $this->db->escape($value);
				$val_exam_test_reg_num = $this->db->escape($arr_test_registration_number[$key]);
				$val_rank = $this->db->escape($arr_test_rank[$key]);
				$val_score_obtained = $this->db->escape($arr_test_score_obtained[$key]);
				$val_percentage_obtained = $this->db->escape($arr_test_percentile_obtained[$key]);
				$val_test_date = $arr_test_date[$key];
				$val_result_status = $this->db->escape($arr_test_result_status[$key]);
								
				$val_test_date_arr = explode('/', $val_test_date);				
				$val_test_date_str = date($val_test_date_arr[2] ."-". $val_test_date_arr[1] ."-". $val_test_date_arr[0]." 00:00:00");
				$val_test_date_str = $this->db->escape($val_test_date_str);
				
				$query = "INSERT INTO student_test_details(stud_test_id, reg_id, exam_test_id, exam_test_reg_num, rank, score_obtained, percentage_obtained, test_date, result_status, created_by, created_dt) VALUES($unique_id, $reg_id , $val_exam_test_id, $val_exam_test_reg_num, $val_rank, $val_score_obtained, $val_percentage_obtained, $val_test_date_str, $val_result_status, $user_id, now())";				
				$this->db->query($query);
			}
		}
		echo "SUCCESSFUL";	
		return;
	}
	
	public function get_coaching_class_master()
	{
		$query = "SELECT a.coaching_class_id, a.name, a.status
				  FROM coaching_class_master as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;
	}
	
	public function get_referrer_source_master()
	{
		$query = "SELECT a.referrer_source_id, a.name, a.status
				  FROM referrer_source_master as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;
	}
	
	public function get_referrer_is_master()
	{
		$query = "SELECT a.referrer_is_id, a.name, a.status
				  FROM referrer_is_master as a
				  WHERE a.status = 'Y'";
		$query_result = $this->db->query($query);
		$res = $query_result->result_array();		
		return $res;
	}
	
	public function save_continue_step6()
	{
		$user_id = $this->db->escape($_SESSION['user_id']);
		
		$unique_id		= $this->db->escape($this->uniqueID());	
		$vertical_id		= $this->db->escape($_POST['vertical_id']);
		$reg_id 			= $this->db->escape($_POST['reg_id']);
		
		$arr_work_experience_company = explode('~|~', $_POST['work_experience_company']);
		$arr_work_experience_designation = explode('~|~',$_POST['work_experience_designation']);
		$arr_work_experience_from_year = explode('~|~',$_POST['work_experience_from_year']);
		$arr_work_experience_to_year = explode('~|~',$_POST['work_experience_to_year']);
		$arr_work_experience_monthly_remuneration = explode('~|~',$_POST['work_experience_monthly_remuneration']);
		$arr_work_experience_reason_leaving = explode('~|~',$_POST['work_experience_reason_leaving']);
		
		$total_work_experience = $this->db->escape($_POST['total_work_experience']);
		
		$referrer_name = $this->db->escape($_POST['referrer_name']);
		$referrer_mobile_no = $this->db->escape($_POST['referrer_mobile_no']);
		$referrer_email_id = $this->db->escape($_POST['referrer_email_id']);
		$referrer_is = $this->db->escape($_POST['referrer_is']);
		$referrer_campus = $this->db->escape($_POST['referrer_campus']);
		$referrer_source = $this->db->escape($_POST['referrer_source']);
		$referrer_coaching_class = $this->db->escape($_POST['referrer_coaching_class']);
		$referrer_city_name = $this->db->escape($_POST['referrer_city_name']);
		$referrer_code = $this->db->escape($_POST['referrer_code']);
		
		$query = "DELETE FROM student_work_experience
			      WHERE reg_id = $reg_id";				  
		$this->db->query($query);
		
		foreach($arr_work_experience_company as $key=>$value){
			if($value != null && $value != ""){
				$unique_id		= $this->db->escape($this->uniqueID());	
				
				$val_work_experience_company = $this->db->escape($value);
				$val_work_experience_designation = $this->db->escape($arr_work_experience_designation[$key]);
				$val_work_experience_from_year = $arr_work_experience_from_year[$key];
				$val_work_experience_to_year = $arr_work_experience_to_year[$key];
				$val_work_experience_monthly_remuneration = $this->db->escape($arr_work_experience_monthly_remuneration[$key]);
				$val_work_experience_reason_leaving = $this->db->escape($arr_work_experience_reason_leaving[$key]);

				$val_work_experience_to_year = explode('/', $val_work_experience_to_year);				
				$val_test_date_str = date($val_work_experience_to_year[1] ."-". $val_work_experience_to_year[0] ."-01 00:00:00");
				$str_work_experience_to_year = $this->db->escape($val_test_date_str);
				
				$val_work_experience_from_year = explode('/', $val_work_experience_from_year);				
				$val_test_date_str = date($val_work_experience_from_year[1] ."-". $val_work_experience_from_year[0] ."-01 00:00:00");
				$str_work_experience_from_year = $this->db->escape($val_test_date_str);
		
				
				$query = "INSERT INTO student_work_experience(stud_work_exp_id, reg_id, company_name, designation, start_date, end_date, monthly_salary, reason_leaving, created_by, created_dt) VALUES($unique_id, $reg_id, $val_work_experience_company, $val_work_experience_designation, $str_work_experience_from_year, $str_work_experience_to_year, $val_work_experience_monthly_remuneration, $val_work_experience_reason_leaving, $user_id , now())";
				$this->db->query($query);
			}
		}
		
		$query = "DELETE FROM student_referrer_details
			      WHERE reg_id = $reg_id";				  
		$this->db->query($query);
		
		$unique_id		= $this->db->escape($this->uniqueID());
		$query = "INSERT INTO student_referrer_details(stud_refer_id, reg_id, name, mobile_no, email_id, referrer_is_id, referrer_source_id, coatching_class_id, city, campus_id, other_source, created_by, created_dt)
		VALUES($unique_id, $reg_id, $referrer_name, $referrer_mobile_no, $referrer_email_id, $referrer_is, $referrer_source, $referrer_coaching_class, $referrer_city_name, $referrer_campus, $referrer_code, $user_id , now())";
		$this->db->query($query);		
		
		echo "SUCCESSFUL";
		return;
	}
	
	public function save_continue_step7()
	{	
		$user_id = $this->db->escape($_SESSION['user_id']);
		$user_id_md = md5($_SESSION['user_id']);
		$reg_id = $this->db->escape($_POST['reg_id']);
		
		if(isset($_FILES['student_recent_photograph']['tmp_name'][0]) && !empty($_FILES['student_recent_photograph']['tmp_name'][0])){
			if(is_uploaded_file($_FILES['student_recent_photograph']['tmp_name'][0])){					
				$sourcePath =  $_FILES['student_recent_photograph']['tmp_name'][0];										
				//$targetPath =  FCPATH . 'assets\admisssion\student_documents\student_recent_photograph\\' . basename($_FILES['student_recent_photograph']['name'][0]);
				$targetPath =  'assets\admisssion\student_documents\student_recent_photograph\\' . basename($_FILES['student_recent_photograph']['name'][0]);
				$ext = basename($_FILES['student_recent_photograph']['name'][0]);
				if(move_uploaded_file($sourcePath, $targetPath)){
					$unique_id		= $this->db->escape($this->uniqueID());
					
					$query = "DELETE FROM admission_document_master
							  WHERE reg_id = $reg_id AND doc_type='RECENT_PHOTOGRAPH'";				  
					$this->db->query($query);
					
					$targetPath = $this->db->escape($targetPath);
					$query = "INSERT INTO admission_document_master(adm_doc_id, reg_id, doc_type, title, doc_physical_path, created_by, created_dt)VALUES($unique_id, $reg_id, 'RECENT_PHOTOGRAPH', 'Recent Photograph', $targetPath, $user_id, now())";							
					$this->db->query($query);		
				}
			}
		}
		
		if(isset($_FILES['student_ssc_marksheet']['tmp_name'][0]) && !empty($_FILES['student_ssc_marksheet']['tmp_name'][0])){
			if(is_uploaded_file($_FILES['student_ssc_marksheet']['tmp_name'][0])){					
				$sourcePath =  $_FILES['student_ssc_marksheet']['tmp_name'][0];										
				//$targetPath =  FCPATH . 'assets\admisssion\student_documents\student_ssc_marksheet\\' . basename($_FILES['student_ssc_marksheet']['name'][0]);
				$targetPath =  'assets\admisssion\student_documents\student_ssc_marksheet\\' . basename($_FILES['student_ssc_marksheet']['name'][0]);
				$ext = basename($_FILES['student_ssc_marksheet']['name'][0]);
				if(move_uploaded_file($sourcePath, $targetPath)){
					$unique_id		= $this->db->escape($this->uniqueID());
					
					$query = "DELETE FROM admission_document_master
							  WHERE reg_id = $reg_id AND doc_type='SSC'";				  
					$this->db->query($query);
					
					$targetPath = $this->db->escape($targetPath);
					$query = "INSERT INTO admission_document_master(adm_doc_id, reg_id, doc_type, title, doc_physical_path, created_by, created_dt)VALUES($unique_id, $reg_id, 'SSC', 'SSC Marksheet', $targetPath, $user_id, now())";		
					$this->db->query($query);
				}
			}
		}
		
		if(isset($_FILES['student_hsc_marksheet']['tmp_name'][0]) && !empty($_FILES['student_hsc_marksheet']['tmp_name'][0])){
			if(is_uploaded_file($_FILES['student_hsc_marksheet']['tmp_name'][0])){					
				$sourcePath =  $_FILES['student_hsc_marksheet']['tmp_name'][0];										
				//$targetPath =  FCPATH . 'assets\admisssion\student_documents\student_hsc_marksheet\\' . basename($_FILES['student_hsc_marksheet']['name'][0]);
				$targetPath =  'assets\admisssion\student_documents\student_hsc_marksheet\\' . basename($_FILES['student_hsc_marksheet']['name'][0]);
				$ext = basename($_FILES['student_hsc_marksheet']['name'][0]);
				if(move_uploaded_file($sourcePath, $targetPath)){
					$unique_id		= $this->db->escape($this->uniqueID());
					
					$query = "DELETE FROM admission_document_master
							  WHERE reg_id = $reg_id AND doc_type='HSC'";				  
					$this->db->query($query);
					
					$targetPath = $this->db->escape($targetPath);
					$query = "INSERT INTO admission_document_master(adm_doc_id, reg_id, doc_type, title, doc_physical_path, created_by, created_dt)VALUES($unique_id, $reg_id, 'HSC', 'HSC Marksheet', $targetPath, $user_id, now())";		
					$this->db->query($query);
				}
			}
		}
		
		if(isset($_FILES['student_graduation_marksheet']['tmp_name'][0]) && !empty($_FILES['student_graduation_marksheet']['tmp_name'][0])){
			if(is_uploaded_file($_FILES['student_graduation_marksheet']['tmp_name'][0])){					
				$sourcePath =  $_FILES['student_graduation_marksheet']['tmp_name'][0];										
				//$targetPath =  FCPATH . 'assets\admisssion\student_documents\student_graduation_marksheet\\' . basename($_FILES['student_graduation_marksheet']['name'][0]);
				$targetPath =  'assets\admisssion\student_documents\student_graduation_marksheet\\' . basename($_FILES['student_graduation_marksheet']['name'][0]);
				$ext = basename($_FILES['student_graduation_marksheet']['name'][0]);
				if(move_uploaded_file($sourcePath, $targetPath)){
					$unique_id		= $this->db->escape($this->uniqueID());
					
					$query = "DELETE FROM admission_document_master
							  WHERE reg_id = $reg_id AND doc_type='GRADUATION'";				  
					$this->db->query($query);
					
					$targetPath = $this->db->escape($targetPath);
					$query = "INSERT INTO admission_document_master(adm_doc_id, reg_id, doc_type, title, doc_physical_path, created_by, created_dt)VALUES($unique_id, $reg_id, 'GRADUATION', 'Graduation Marksheet', $targetPath, $user_id, now())";		
					$this->db->query($query);
				}
			}
		}
		echo "SUCCESSFUL";
	}
	
	public function apply_coupon()
	{
		$user_id = $this->db->escape($_SESSION['user_id']);
		
		$unique_id		= $this->db->escape($this->uniqueID());	
		$vertical_id	= $this->db->escape($_POST['vertical_id']);
		$reg_id 		= $this->db->escape($_POST['reg_id']);
		$payment_student_reg_amount = $_POST['payment_student_reg_amount'];
		$payment_student_coupon 	= $this->db->escape($_POST['payment_student_coupon']);
		
		$query = "SELECT a.coupon_id, a.coupon_code, a.discount_percentage, a.status 
				  FROM coupon_master as a 
				  WHERE a.coupon_code = $payment_student_coupon AND a.status = 'Y'";
		//echo $query;			
		$query_result = $this->db->query($query);
		$res = $query_result->row_array();		
		
		$discount_payment = "";	
		if(is_array($res) && !empty($res)){				
			$coupon_id = $res['coupon_id'];
			$coupon_code = $res['coupon_code'];
			$discount_percentage = $res['discount_percentage'];			
			$discount_payment = $payment_student_reg_amount - ($payment_student_reg_amount * ($discount_percentage / 100));
			//echo $discount_payment;
		}else{
			$discount_payment = "INVALID";
		}
		echo $discount_payment;
		return;	
	}
	
	public function check_accept_terms_conditions()
	{
		$user_id = $this->db->escape($_SESSION['user_id']);
		
		$query = "SELECT IFNULL(a.accept_terms_conditions, '') as accept_terms_conditions 
		          FROM student_register as a
				  WHERE a.student_email_id = $user_id";		
		$query_result = $this->db->query($query);
		$res = $query_result->row_array();
		
		if($res['accept_terms_conditions'] == "" || $res['accept_terms_conditions'] == 'N'){
			echo 'N';
			return;
		}
		echo 'Y';
		return;
	}
	
	public function accept_terms_conditions()
	{
		$user_id = $this->db->escape($_SESSION['user_id']);
		$query = "UPDATE student_register 
				  SET accept_terms_conditions = 'Y'
				  WHERE student_email_id = $user_id";		
		$query_result = $this->db->query($query);
		echo "SUCCESSFUL";
	}
	
	public function get_client_ip(){
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	public function logout()
	{
    
		$this->session->sess_destroy();
	
		session_destroy();
		redirect(base_url());

	}
	public function verify()
	{
		$user_id = $_REQUEST['user_id'];
		$a_key = $_REQUEST['a_key'];		
		
		$query = "SELECT * FROM user_master as a 
				  WHERE a.user_id = $user_id  AND a.activation_key = $a_key";
		$query_result = $this->db->query($query);
		$res = $query_result->row_array();

		$res_data = array();
		if(is_array($res) && !empty($res)){
			$query = "UPDATE user_master 
				  SET active = 'Y' , activation_key = 'Y'
				  WHERE student_email_id = $user_id";
				  $res_data['user_data'] = $res;			
		}
		return $res_data;
		
	}
	
	public function password_change()
	{
		$user_id	 = $this->db->escape($_SESSION['user_id']);
		$current_pwd = $this->db->escape($_POST['current_pwd']);
		$new_pwd	 = $this->db->escape($_POST['new_pwd']);
		$login_pwd	 = $this->db->escape($_POST['login_pwd']);
		

 		/*  echo '<pre>';
		print_r($_POST);
		print_r($current_pwd);
		print_r($new_pwd);
		exit; */ 
		
		$query = $this->db->get_where("user_master",array('user_pass'=>$current_pwd,'user_id'=>$user_id));
		//$query = "SELECT * FROM user_master as a 
				 // WHERE a.user_id = $user_id AND a.user_pass = $new_pwd  ";
		echo $query;
		if($query == 1){
			$query = "UPDATE user_master  SET user_pass = $new_pwd 
				  WHERE user_id = $user_id";		
			$query_result = $this->db->query($query);
			echo "SUCCESSFUL";
		}else{
			echo "Un-SUCCESSFUL";
		}
		
	}
	
	public function ask_me(){
		
		$user_id = $this->db->escape($_SESSION['user_id']);
		
		$unique_id		= $this->db->escape($this->uniqueID());	
		$query_type		= $this->db->escape($_POST['query_type']);
		$reg_id		= $this->db->escape($_POST['reg_id']);
		$qry_applicant_student_name 		= $this->db->escape($_POST['qry_applicant_student_name']);
		$query_category 					= $this->db->escape($_POST['query_category']);
		$query_desc 						= $this->db->escape($_POST['query_desc']);	
		$query_type 						= $this->db->escape($_POST['query_type']);	

		$attached_doc	 = "";
		
		$query = "INSERT INTO ticket_master(ticket_master_id, reg_id, ticket_type, applicant_name, ticket_desc, ticket_category,  status, created_dy, created_dt) VALUES($unique_id, $reg_id, $query_type, $qry_applicant_student_name, $query_desc, $query_category, 'OPEN', $user_id, now())";		
		echo $query;
		$this->db->query($query);		
		echo "SUCCESSFUL";
	}
	
	
}