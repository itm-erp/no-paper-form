<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ITM-Group of Institutions</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicons
    ================================================== -->    
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/nivo-lightbox/nivo-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/nivo-lightbox/default.css">    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/bootstrap-datepicker.css">    
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/jquery.fancybox.css?v=2.1.5" media="screen" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/jquery.fancybox-thumbs.css?v=1.0.7" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/admisssion/css/jquery.fancybox-buttons.css?v=1.0.5" />
	<style>
		.fancybox-overlay {
			z-index: 1000000 !important;
		}
	</style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	<input id="baseurl" type="hidden" value="<?php echo base_url(); ?>"/>
	<input id="vertical_id" type="hidden" value="PGDM" />
	<input id="user_type" type="hidden" value="PGDM_STUDENT" />
	

    <!-- Navigation
    ==========================================-->
 
    <br/>    
    <!-- Contact Section -->
    <div id="contact" class="text-center">
        <div class="container">        
				<h3 style ="color:red">Congratulations!!!! You have been successfully Verified By Our Team.</h3>
				
				<!--<p><h4>Transaction ID:</h4> <b></b></p>
				<p><h4>Application No:</h4> <b>ITM-PGDM-<?php echo date("Y");?>-<?php echo $user_data['id']; ?></b></p>-->
				
				<a id="btn_accept_terms_conditions" type="button" href="<?php echo base_url();?>index.php/admission/logout_view" class="btn btn-primary btn-xl show_login_model" style="background-color:#72a411;border-color:#72a411;cursor:pointer;">Login</a>
        </div>    
    </div>
	
	
    <!--<div id="footer" style="padding:0 0 0 0;">	 	
        <div class="container-fluid text-center copyrights">
            &copy;
        </div>
    </div>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.1.11.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/nivo-lightbox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.isotope.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jqBootstrapValidation.js"></script>    
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jqBootstrapValidation.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/bootstrap-datepicker-au.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/angular.min.1.6.4.js"></script>	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.mousewheel.pack.js?v=3.1.3"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.fancybox.pack.js?v=2.1.5"></script>	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.fancybox-buttons.js?v=1.0.5"></script>		
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admisssion/js/jquery.fancybox-media.js?v=1.0.6"></script>
	<script src='https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js'></script>
	<script src='https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js'></script>
	<script src='https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js'></script>
	<script src='https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js'></script>

	<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqLiG8kPgjqiayBYmBK6ciwI3ADdJrwhY&libraries=places&callback=initMap" async defer></script>-->
</body>

</html>