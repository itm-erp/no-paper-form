<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<table class="table table-bordered">					
	<tbody>
	  <tr>
		<td style="width:33.33%"><b>Student Name:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['first_name']) . " " . ucfirst($student_personal_address_info['middle_name']) . " " . ucfirst($student_personal_address_info['last_name']); ?></td>
		<td style="width:33.33%"><b>Student DOB:</b>&nbsp;<?php echo date("d/m/Y", strtotime($student_personal_address_info['dob'])); ?></td>
		<td style="width:33.33%"><b>Student Gender:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['gender']); ?></td>
	  </tr>
	  <tr>
		<td><b>Student Email:</b>&nbsp;<?php echo ucfirst($_SESSION['user_id']); ?></td>
		<td><b>Student Mobile:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['student_mobile_no']); ?></td>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td><b>Father Name:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['father_first_name']) . " " . ucfirst($student_personal_address_info['father_middle_name']) . " " . ucfirst($student_personal_address_info['father_last_name']); ?></td>
		<td><b>Mobile No:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['father_mobile']); ?></td>
		<td><b>&nbsp;</b></td>
	  </tr>
	  <tr>
		<td><b>Mother Name:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['mother_first_name']) . " " . ucfirst($student_personal_address_info['mother_middle_name']) . " " . ucfirst($student_personal_address_info['mother_last_name']); ?></td>
		<td><b>Mother No:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['mother_mobile']); ?></td>
		<td><b>&nbsp;</b></td>
	  </tr>	
     </tbody>	  
	</table>  
   <table class="table table-bordered">					
	<tbody>  
	  <tr>
		<td><b>Correspondence Address:</b&nbsp;<?php echo ucfirst($student_personal_address_info['corres_address']); ?></td>		
		<td><b>City:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['corres_city']); ?></td>
	  </tr>					  					  
	  <tr>
		<td><b>State</b>&nbsp;<?php echo ucfirst($student_personal_address_info['corres_state']); ?></td>
		<td><b>Pin</b>&nbsp;<?php echo ucfirst($student_personal_address_info['corres_pin']); ?></td>
		<td>&nbsp;</td>
	  </tr>
	  <tr>
		<td><b>Permanent Address:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['permanent_address']); ?></td>		
		<td><b>City:</b>&nbsp;<?php echo ucfirst($student_personal_address_info['permanent_city']); ?></td>
	  </tr>					  					  
	  <tr>
		<td><b>State</b>&nbsp;<?php echo ucfirst($student_personal_address_info['permanent_state']); ?></td>
		<td><b>Pin</b>&nbsp;<?php echo ucfirst($student_personal_address_info['permanent_pin']); ?></td>
		<td>&nbsp;</td>
	  </tr>	
	  </tbody>
	  </table>
	<table class="table table-bordered">					
	<tbody>			  
	   
	 
	 <?php $sr=1;
	 $cam_pref_info=array();
	 $cam_pref=array();
	  //if(count())
		foreach($student_campus_specilization_info as $cam_info){
			$level=$cam_info['preference_level'];
			if($sr==$level)
			{
				array_push($cam_pref,$cam_info['specialization_name']);
				$cam_pref_info[$level] =array('specialization_name' => implode(",", $cam_pref),'preference_level'=>$level,'campus_name'=>$cam_info['campus_name'],'available_date'=>$cam_info['available_date'] );
				
			}else
			{
				array_push($cam_pref_info,array('specialization_name' => $cam_info['specialization_name'] ,'preference_level'=>$level,'campus_name'=>$cam_info['campus_name'],'available_date'=>$cam_info['available_date'] )); 
				$sr++;
			}
		}?>
		<tr>
		<td><b>Preference Level </b></td>
		<td><b>Preferred Campus </b></td>
		<td><b>Specialization</b></td>
		<td><b>Available date</b></td>
		</tr>		
	  <?php
		foreach($cam_pref_info as $cam_info_dsply){
		?>
	    <tr><td><?php echo ucfirst($cam_info_dsply['preference_level']); ?></td>
		<td><?php echo ucfirst($cam_info_dsply['campus_name']); ?></td>
		<td><?php echo ucfirst($cam_info_dsply['specialization_name']); ?></td>
		<td><?php echo date("d/m/Y", strtotime($cam_info_dsply['available_date']));  ?></td>
	    </tr>					  					  
	 <?php  } ?>	
	 
	   </tbody>
	  </table>
	  <table class="table table-bordered">					
	  <tbody>	
	  <tr>
		<td><b>Education</b></td>
		<td><b>Institute </b></td>
		<td><b>City</b></td>
		<td><b>Board</b></td>
		<td><b>Year</b></td>
		<td><b>Percentage</b></td>
	  </tr>					  					  
	 
	 <?php
	   	foreach($student_academic_info as $academic_info){ ?>
	   <tr><td><?php echo ucfirst($academic_info['educational_level']); ?>-<?php echo ucfirst($academic_info['degree']); ?>(<?php echo ucfirst($academic_info['stream_specialization_gen_txt']); ?>)</td>
		<td><?php echo ucfirst($academic_info['institute_name']); ?></td>
		<td><?php echo ucfirst($academic_info['city_name']); ?></td>
		<td><?php echo ucfirst($academic_info['board_university']); ?> (<?php echo ucfirst($academic_info['unversity']); ?>)</td>
		<td><?php echo date("d/m/Y", strtotime($academic_info['passing_date'])); ?> </td>
		<td><?php echo $academic_info['percentage_obtained']; ?></td>
	  </tr>					  					  
	<?php }?>
       </tbody>	
	  </table>
	  <table class="table table-bordered">					
	  <tbody>
	  <tr>
		<td><b>Company Name </b></td>
		<td><b>Designation </b></td>
		<td><b>Period</b></td>
		<td><b>Monthly Salary</b></td>
		<td><b>Reason</b></td>
	  </tr>					  					  
	 
	 <?php
	   	foreach($student_workexp_info as $workexp_info){ ?>
	   <tr><td><?php echo ucfirst($workexp_info['company_name']); ?></td>
		<td><?php echo ucfirst($workexp_info['designation']); ?></td>
		<td><?php echo date("d/m/Y", strtotime($workexp_info['start_date'])); ?> - <?php echo date("d/m/Y", strtotime($workexp_info['end_date'])); ?></td>
		<td><?php echo ucfirst($workexp_info['monthly_salary']); ?></td>
		<td><?php echo ucfirst($workexp_info['reason_leaving']); ?></td>
	  </tr>					  					  
	<?php }?>	
       </tbody>	
	 </table>
	 <table class="table table-bordered">					
		<tbody>
	  <?php extract($student_refer_info); ?>
	   <tr>
		<td><b>Name:&nbsp;</b><?php echo ucfirst($name); ?></td>
		<td><b>Mobile No:&nbsp;</b><?php echo ucfirst($mobile_no); ?></td>
		<td><b>Email:&nbsp;</b><?php echo $email_id; ?></td>
	   </tr>	
		<tr>
		<td><b>Refere Type:&nbsp;</b><?php echo ucfirst($ref_type); ?></td>
		<td><b>Source:&nbsp;</b><?php echo ucfirst($ref_source); ?></td>
		<td><b>Campus:&nbsp;</b><?php echo $campus_id; ?></td>
	   </tr>
		</tbody>	   
	 </table>			  
	 		  					  					  
