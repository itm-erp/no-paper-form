<?php 
(defined('BASEPATH')) OR exit('No direct script access allowed'); 
/**
 * Description of site
 *
 * @author Harshal
 * This is Home file of admission_home
 * User comes to admission then this file is displayed
 * Here Register and Login page is displaying
 */
 
 
class Admission extends MY_Controller {
 
    function __construct() 
	{
        parent::__construct();
		$this->load->library('session');
		$this->load->helper('captcha');
		$this->load->helper('url');
		
		$this->load->model('admission_model');
    }
 
    public function index() 
	{	
		
		$data['cities_data'] = $this->admission_model->get_cities();
		$data['states_data'] = $this->admission_model->get_states();
		
		$data['captcha_image'] = $this->custom_create_captcha() . 
		'&nbsp;<button id="btn_refresh_captcha" type="button" class="btn btn-primary btn-xs" style="background-color:#72a411;border-color:#72a411;">Refresh</button>';
		
		$this->load->view('admission_home', $data);
    }
	
	public function check_accept_terms_conditions(){
		 return $this->admission_model->check_accept_terms_conditions();
	}

	private function custom_create_captcha()
	{	
		$options = array('img_path'=>FCPATH.'assets/captcha/','img_url'=>base_url().'assets/captcha/','img_height'=>'30','expiration'=>7200,);		
		$cap = create_captcha($options);		
		$image = $cap['image'];		
		$this->session->set_userdata('captchaword', $cap['word']);		
		return $image;
	}
	
	public function check_captcha($string)
	{
		if($string==$this->session->userdata('captchaword')){
		  return TRUE;
		}
		else{		  
		  return FALSE;
		}
    }
	
	public function register_user()
	{
		$vertical_id 	= $_POST['vertical_id'];		
		$student_full_name 		= $_POST['student_full_name'];
		$your_email 	= $_POST['your_email'];
		$mobile_number 	= $_POST['mobile_number'];
		$your_password 	= $_POST['your_password'];
		$choose_state 	= $_POST['choose_state'];
		$choose_city 	= $_POST['choose_city'];
		$get_captcha 	= $_POST['get_captcha'];		 	
		$user_type 		= $_POST['user_type'];		 	
		
		if(is_array($this->admission_model->validate_existing_email()) && !empty($this->admission_model->validate_existing_email())){
			echo "FAIL,Email address already registered with us.";
			return;
		}
		
		if(!$this->check_captcha($get_captcha)){
			echo "FAIL,Wrong CAPTCHA";		
			return;
		}
		
		if(empty($student_full_name) || empty($your_email) || empty($mobile_number) || empty($your_password) || empty($choose_state) 
			|| empty($get_captcha)){
			echo "FAIL,Please enter mandatory fields";
			return;
		}
				
		$this->admission_model->student_register();
		echo "SUCCESSFUL,SUCCESSFUL";
	}	
	
	public function refresh_captcha()
	{			
		$data['captcha_image'] = $this->custom_create_captcha();
		echo $data['captcha_image'] . 
		'&nbsp;<button id="btn_refresh_captcha" type="button" class="btn btn-primary btn-xs" style="background-color:#72a411;border-color:#72a411;">Refresh</button>';
	}
	
	public function forgot_password()
	{
		$your_email_forgot_password  = $_POST['your_email_forgot_password'];
		$this->admission_model->forgot_password();
	}
	
	public function login_user()
	{
		$your_email_login	= $_POST['your_email_login'];
		$your_password_login= $_POST['your_password_login'];
		$remember_me		= $_POST['remember_me'];
		
		$return_value = $this->admission_model->login_user();		
		echo $return_value;
		return;
	}
		
	public function logout_view(){
		
			$this->admission_model->logout();
			//redirect("admission/login");
	}	
		
	public function process()
	{
		$user_id = $_SESSION['user_id'];
		if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
			return;
		}
		
		$get_steps_data = $this->admission_model->get_steps_data();			
		$this->load->view('admission_process', $get_steps_data);		
	}	
	public function print_doc()
	{
		$this->admission_model->print_doc();
	}
	public function save_step1()
	{
		$this->admission_model->save_step1();
		return;
	}
	
	public function save_step2()
	{
		$this->admission_model->save_step2();
		return;
	}
	
	public function save_continue_step3()
	{
		$this->admission_model->save_continue_step3();
		return;
	}
	
	public function campus_specialization_1()
	{
		$data = $this->admission_model->campus_specialization_1();
		echo $data;
		return;
	}
	
	public function campus_specialization_2()
	{
		$data = $this->admission_model->campus_specialization_2();
		echo $data;
		return;
	}
	
	public function save_continue_step4()
	{
		$data = $this->admission_model->save_continue_step4();
		echo $data;
		return;
	}
	
	public function get_gdpi_location_available_dates()
	{
		$data = $this->admission_model->get_gdpi_location_available_dates();
		echo $data;
		return;
	}
	
	public function save_continue_step5()
	{
		$data = $this->admission_model->save_continue_step5();		
		return;
	}
	
	public function get_coaching_class_master()
	{
		$data = $this->admission_model->get_coaching_class_master();
		echo $data;
		return;
	}
	
	public function save_continue_step6()
	{
		$data = $this->admission_model->save_continue_step6();
		echo $data;
		return;
	}
	
	public function save_continue_step7()
	{
		$data = $this->admission_model->save_continue_step7();		
		return;
	}
	
	public function campus_specialization_11($campus_id)
	{
		$data = $this->admission_model->campus_specialization_11($campus_id);		
		return;
	}
	
	public function campus_specialization_22($campus_id)
	{		
		$data = $this->admission_model->campus_specialization_22($campus_id);		
		return;
	}
	
	public function apply_coupon()
	{		
		return $this->admission_model->apply_coupon();		
		
	}
	
	public function accept_terms_conditions()
	{		
		return $this->admission_model->accept_terms_conditions();		
	}
	
	public function password_change()
	{		
		return $this->admission_model->password_change();		
	}
	
	public function verify()
	{		
		//$record = $this->admission_model->verify();		
		$this->load->view('login_verify');
	}
	
	public function ask_me()
	{		
		return $this->admission_model->ask_me();		
	}
}