<?php 
(defined('BASEPATH')) OR exit('No direct script access allowed'); 
/**
 * Description of site
 *
 * @author Harshal Patil
 * This is controller file for Login to dashboard
 * 
 */
class Login extends MY_Controller {
 
    function __construct() 
	{
        parent::__construct();
		$this->load->library('session');
		$this->load->helper('captcha');
		$this->load->helper('url');
		
		$this->load->model('dashboard_model');
    }
 
    public function index() 
	{		
		$this->load->view('login');
    }

	public function user_login()
	{
		$this->dashboard_model->user_login();
	}
	public function logout() 
	{	
		$this->session->sess_destroy ();
		redirect('index.php/pgdmmr/login', 'refresh');	
	}
		
}