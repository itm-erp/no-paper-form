<?php 
(defined('BASEPATH')) OR exit('No direct script access allowed'); 
/**
 * Description of site
 *
 * @author Harshal Patil
 * This is controller file for Dashbard used by 
 * marketing team to see form details
 */
class Dashboard extends MY_Controller {
 
    function __construct() 
	{
        parent::__construct();
		$this->load->library('session');
		$this->load->helper('captcha');
		$this->load->helper('url');
		
		$this->load->model('dashboard_model');
		
		if(!isset($_SESSION['user_id']) || $_SESSION['user_id'] == "" || !isset($_SESSION['user_type']) || $_SESSION['user_type'] == ""){
		   redirect('index.php/pgdmmr/login', 'refresh');			
		}
    }
 
    public function index() 
	{	
		if(!isset($_SESSION['user_id']) || $_SESSION['user_id'] == "" || !isset($_SESSION['user_type']) || $_SESSION['user_type'] == ""){
		   redirect('index.php/pgdmmr/login', 'refresh');			
		}		
		$this->load->view('template');
    }
	/********  Application Module End**********/
	function loadViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL){

        $this->load->view('header', $headerInfo);
        $this->load->view($viewName, $pageInfo);
        $this->load->view('footer', $footerInfo);
    }
	
	public function get_new_registrations()
	{
		$this->dashboard_model->get_new_registrations();
	}
	public function get_xls()
	{
		$this->dashboard_model->xls();
	}

	public function get_form_submitted()
	{
		$this->dashboard_model->get_form_submitted();
	}
	
	public function get_payment_done()
	{
		$this->dashboard_model->get_payment_done();
	}		
	
	public function get_pre_payment()
	{
		$this->dashboard_model->get_pre_payment();
	}

	public function daywise_form_submitted()
	{
		$this->dashboard_model->daywise_form_submitted();
	}
	
	public function recent_form_submitted()
	{
		$this->dashboard_model->recent_form_submitted();
	}
	
	public function daywise_form_submitted_tabularform()
	{
		$this->dashboard_model->daywise_form_submitted_tabularform();
	}
	
	public function applications()
	{
		$this->loadViews('applications');
    			
	}

	public function marketing_office_submission_graph()
	{
		$this->dashboard_model->marketing_office_submission_graph();
	}
	
	public function marketing_office_submission_tabularform()
	{
		$this->dashboard_model->marketing_office_submission_tabularform();
	}
	
	public function display_filter_data()
	{
		$this->dashboard_model->display_filter_data();
		
	}
	public function search_applications()
	{
		$data['applications'] = $this->dashboard_model->search_applications();
		$data['txtApplicationNumber'] =trim($this->input->post('txtApplicationNumber'));
		$data['form_status'] = trim($this->input->post('form_status'));
		$data['txtLocation'] = trim($this->input->post('txtLocation'));
		$data['exact_search_select'] = trim($this->input->post('exact_search_select'));
		$data['exact_search_txt'] =  trim($this->input->post('exact_search_txt'));
		$data['relative_search_select'] =  trim($this->input->post('relative_search_select'));
		$data['relative_search_txt'] = trim($this->input->post('relative_search_txt'));
		$this->loadViews('applications_list',$data);
				
	}
	
	public function print_doc()
	{
		$this->dashboard_model->print_doc();
	}

	public function edit_record()
	{
		$this->dashboard_model->edit_record();
	}
	
	public function view_document()
	{
		$this->dashboard_model->view_document();
	}
		
	public function save_step2()
	{
		$this->dashboard_model->save_step2();
		return;
	}
	
	public function save_continue_step3()
	{
		$this->dashboard_model->save_continue_step3();
		return;
	}
	
	public function campus_specialization_1()
	{
		$data = $this->dashboard_model->campus_specialization_1();
		echo $data;
		return;
	}
	
	public function campus_specialization_2()
	{
		$data = $this->dashboard_model->campus_specialization_2();
		echo $data;
		return;
	}
	
	public function save_continue_step4()
	{
		$data = $this->dashboard_model->save_continue_step4();
		echo $data;
		return;
	}
	
	public function get_gdpi_location_available_dates()
	{
		$data = $this->dashboard_model->get_gdpi_location_available_dates();
		echo $data;
		return;
	}
	
	public function save_continue_step5()
	{
		$data = $this->dashboard_model->save_continue_step5();		
		return;
	}
	
	public function get_coaching_class_master()
	{
		$data = $this->dashboard_model->get_coaching_class_master();
		echo $data;
		return;
	}
	
	public function save_continue_step6()
	{
		$data = $this->dashboard_model->save_continue_step6();
		echo $data;
		return;
	}
	
	public function save_continue_step7()
	{
		$data = $this->dashboard_model->save_continue_step7();		
		return;
	}
	
	public function campus_specialization_11($campus_id)
	{
		$data = $this->dashboard_model->campus_specialization_11($campus_id);		
		return;
	}
	
	public function campus_specialization_22($campus_id)
	{		
		$data = $this->dashboard_model->campus_specialization_22($campus_id);		
		return;
	}
	/********  Application Module End**********/
	
	/********  Coupons Module **********/
	public function get_coupons()
	{
		
		$data['coupons']=$this->dashboard_model->get_coupons();
		$data['users'] = $this->dashboard_model->get_users("PGDM_MARKETING");
		$this->loadViews('coupons',$data);
	
	}
	public function get_coupons_approved()
	{
		
		$data['coupons']=$this->dashboard_model->get_coupons_approved();
		$data['users'] = $this->dashboard_model->get_users("PGDM_MARKETING");
		$this->loadViews('apprvdcoupons',$data);
				
	}
	public function generate_coupons()
	{
		
		$this->load->helper('form');
		$g_coupons = $this->input->post('g_coupons');
		$g_discount_percentage = $this->input->post('g_discount_percentage');
		$this->dashboard_model->generate_coupons($g_coupons,$g_discount_percentage);
		$this->get_coupons();
	}
	public function search_coupons()
	{
		$data['coupons']=$this->dashboard_model->get_coupons();
		$data['users'] = $this->dashboard_model->get_users("PGDM_MARKETING");
		$data['coupon_code'] = $this->input->post('coupon_code');
		$data['discount_percentage'] = $this->input->post('discount_percentage');
		$data['created_by'] = $this->input->post('user_id');
		$this->loadViews('coupons',$data);
	
			
	}
	public function search_coupons_approved()
	{
		$data['coupons'] = $this->dashboard_model->get_coupons_approved();
		$data['users'] = $this->dashboard_model->get_users("PGDM_MARKETING");
		$data['coupon_code'] = $this->input->post('coupon_code');
		$data['discount_percentage'] = $this->input->post('discount_percentage');
		$data['created_by'] = $this->input->post('user_id');
		$this->loadViews('apprvdcoupons',$data);
				
	}
	
	public function approved_coupons()
	{
		
		$this->load->helper('form');
		$coupons = $this->input->post('approved');
		if(empty($coupons))
		{ ?>
			<script>
			alert("Please Select at least one coupon");
			</script>
		 <?php  $this->get_coupons_approved();	
		} else {
			
		$this->dashboard_model->approved_coupons($coupons);
		?>
			<script>
			alert("Coupons Approved");
			</script>
		 <?php
		$this->get_coupons_approved();	
		}
		
		
	}
	public function coupon_deactive($id)
	{
		if($this->dashboard_model->coupon_deactive($id))
		{
		?>
			<script>
			alert("Coupons Deactivated");
			</script>
		 <?php
		  $this->get_coupons();
		}
		
	}
	/********  Coupons Module End**********/

	
		
	/********  Agent Module **********/
	// get_all($fields = '', $from = '', $join = '',$joincondition = '' ,$where = array(), $limit = '', $order_by = '', $group_by = '')
	
	public function get_agents() 
	{
		$agent_data['data'] = $this->dashboard_model->get_all('*', 'agent_master',NULL,NULL,NULL,NULL,NULL,NULL);
		$this->loadViews('agents',$agent_data);
    }
	
	public function checkEmailExists()
	{
		$email = $this->input->post("email");
		$where=array('email'=>$email);
        $result = $this->dashboard_model->get_all('id','agent_master',NULL,NULL,$where, NULL, NULL, NULL);
        if(empty($result)){ echo("true"); }
        else { echo("false"); }
	}
	public function get_agent_det(){
		$agent_id 		= $_POST['agent_id'];
		$agent_data['data'] = $this->dashboard_model->get($agent_id);
		//print_r($agent_data);
	}
	public function add_agent() 
	{
		$agent_data['cities'] = $this->dashboard_model->get_all('city_id,name','city_master',NULL,NULL,NULL,NULL,NULL,NULL);		
		$agent_data['agent_types'] = $this->dashboard_model->get_all('id,name','agent_type_master',NULL,NULL,NULL,NULL,NULL,NULL);
		$agent_data['campus'] = $this->dashboard_model->get_all('campus_id,campus_name','campus_master',NULL,NULL,NULL,NULL,NULL,NULL);
		$this->loadViews('add_agent',$agent_data);
    }
	
	public function addNew() 
	{
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('f_name','First Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('m_name','Middle Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('l_name','Last Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('agent_type','Please select atleast one option','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('consultancy_name','Consultancy Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('contact_number','Contact Number','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('email','Email','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('contact_number','Mobile','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('campus','Select Campus','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('address','Address','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('city','Please select atleast one option','trim|required|max_length[128]|xss_clean');
					  
		if($this->form_validation->run() == FALSE)
		{
			$this->add_agent(); 
		} 
		else{
		$f_name = ucwords(strtolower($this->input->post('f_name')));
		$m_name = ucwords(strtolower($this->input->post('m_name')));
		$l_name = ucwords(strtolower($this->input->post('l_name')));
		$agent_id = $this->dashboard_model->uniqueID(); 
		$agent_type = $this->input->post('agent_type');
		$consultancy_name = $this->input->post('consultancy_name');
		$email = $this->input->post('email');
		$mobile = $this->input->post('contact_number');
		$campus = $this->input->post('campus');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$status = $this->input->post('status');
		$file_name="";
	  
		$data_array = array('agent_id'=>$agent_id,'agent_type_id'=> $agent_type,'first_name'=>$f_name,'middle_name'=>$m_name,'last_name'=>$l_name,
		'consultancy_name'=>$consultancy_name,'city'=>$city,'email'=>$email,'mobile'=>$mobile,'address'=>$address,'campus_id'=>$campus,
		'status'=>$status,'created_dt'=>date('Y-m-d H:i:s'),'created_by'=>$this->loggedId);

		$agent_id = $this->dashboard_model->insert($data_array,'agent_master');
		if($agent_id>0){			
			$config['upload_path']          = 'assets/agent/uploads/';
			$config['allowed_types']        = 'gif|jpg|png|doc|docx|pdf|xls|xlsx';
			$config['file_name']        	= $agent_id."_"."MOU"."_".$f_name;
			$config['max_size']             = 2000;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$config['overwrite']           = TRUE;

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('upload_mou'))
			{
				$error = array('error' => $this->upload->display_errors());
			}
			else
			{
				$data = array('upload_mou' => $this->upload->data());
				$file_name=$data['upload_mou']['file_name'];
			}
		}
		if($file_name!="")
		{
			$data_array=array('mou_doc'=>$file_name);
			$result = $this->dashboard_model->update($data_array,$agent_id,'agent_master');
		}
	    if($agent_id > 0)
		{
			$this->session->set_flashdata('success', 'New Agent Added successfully');
		}
		else
		{
			$this->session->set_flashdata('error', 'Add Agent failed');
		}
                
			
		$this->add_agent(); 
		}
	}
	public function edit_agent($agent_id=NULL) 
	{
		if(!isset($agent_id))
		{
		  $this->index(); 
		}else{
		$agent_data['data'] = $this->dashboard_model->get($agent_id);
		$agent_data['cities'] = $this->dashboard_model->get_all('id,name',NULL,'city_master', NULL, $order_by = 'name', NULL);
		$agent_data['agent_types'] = $this->dashboard_model->get_all('id,name',NULL,'agent_type_master', NULL, $order_by = 'name', NULL);
		$agent_data['campus'] = $this->dashboard_model->get_all('id,campus_name',NULL,'campus_master', NULL, $order_by = 'campus_name', NULL);
		$this->loadViews('edit_agent',$agent_data);
		}

    }
	public function update_agent() 
	{
		$agent_id = $this->input->post('agent_id');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('f_name','First Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('m_name','Middle Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('l_name','Last Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('agent_type','Please select atleast one option','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('consultancy_name','Consultancy Name','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('contact_number','Mobile','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('campus','Select Campus','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('address','Address','trim|required|max_length[128]|xss_clean');
		$this->form_validation->set_rules('city','Please select atleast one option','trim|required|max_length[128]|xss_clean');
					  
		if($this->form_validation->run() == FALSE)
		{
			$this->edit_agent($agent_id); 
		}else{

		$f_name = ucwords(strtolower($this->input->post('f_name')));
		$m_name = ucwords(strtolower($this->input->post('m_name')));
		$l_name = ucwords(strtolower($this->input->post('l_name')));
		$agent_type = $this->input->post('agent_type');
		$consultancy_name = $this->input->post('consultancy_name');
		$mobile = $this->input->post('contact_number');
		$campus = $this->input->post('campus');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$status = $this->input->post('status');
		$file_name="";
	  
		if($agent_id>0){			
			$config['upload_path']          = 'assets/agent/uploads/';
			$config['allowed_types']        = 'gif|jpg|png|doc|docx|pdf|xls|xlsx';
			$config['file_name']        	= $agent_id."_"."MOU"."_".$f_name;
			$config['max_size']             = 2000;
			$config['max_width']            = 0;
			$config['max_height']           = 0;
			$config['overwrite']           = TRUE;

			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('upload_mou'))
			{
				$error = array('error' => $this->upload->display_errors());
			}
			else
			{
				$data = array('upload_mou' => $this->upload->data());
				$file_name=$data['upload_mou']['file_name'];
			}
		}
			$data_array = array('agent_type_id'=> $agent_type,'first_name'=>$f_name,'middle_name'=>$m_name,'last_name'=>$l_name,
			'consultancy_name'=>$consultancy_name,'city'=>$city,'mobile'=>$mobile,'address'=>$address,'campus_id'=>$campus,
			'status'=>$status,'modified_dt'=>date('Y-m-d H:i:s'),'mou_doc'=>$file_name,'modified_by'=>$this->loggedId);
			$result = $this->dashboard_model->update($data_array,$agent_id,'agent_master');	
			redirect('/agent'); 		
		}
	}
	public function delete_agent($agent_id=NULL) 
	{
		if(!isset($agent_id)){
			$this->index(); 
		}else{
			$data_array = array('status'=>0,'modified_dt'=>date('Y-m-d H:i:s'),'modified_by'=>$this->loggedId);
			$result = $this->dashboard_model->update($data_array,$agent_id,'agent_master');	
			$this->index(); 	
		}	
	}
	/********  Agent Module End **********/
	
	public function ticket_details()
	{		
		$this->loadViews('ticket_details');		
	}
	
}