  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Applications
      </h1>
      <ol class="breadcrumb">
        <!--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>-->
      </ol>
    </section>

    <!-- Main content -->	
    <section class="content">
	  <div class="row">        
        <section class="col-lg-6 connectedSortable">
		<form role="form" id="search_form">
              <div class="box-body">
                <div class="form-group"> 
				<div class="col-md-6">
                    <label>Exact Search</label>
					  <select class="form-control" id="exact_search_select">
					    <option value="">Select</option>
						<option value="EMAIL">Email</option>
						<option value="MOBILE_NO">Mobile No</option>						
					  </select>
				</div>
				<div class="col-md-6"> <label>&nbsp;</label>
					<input type="text" class="form-control" id="exact_search_txt" placeholder="" style="display:none;">
				</div>
                </div>                
              </div>              
         
        </section>
		  <section class="col-lg-6 connectedSortable">		
              <div class="box-body">
                <div class="form-group">
					<div class="col-md-6">
					  <label>Relative Search</label>
					  <select id="relative_search_select" class="form-control">
					    <option value="">Select</option>
						<option value="NAME">Name</option>
						<option value="CITY">City</option>						
					  </select>
					</div>
					<div class="col-md-6"> <label>&nbsp;</label>
					<input type="text" class="form-control" id="relative_search_txt" placeholder="" style="display:none;">
				</div>
                </div>                
              </div>              
        </section>		      
      </div>
	  
      <div class="row">        
        	<section class="col-lg-4 connectedSortable">
			<form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Application Number</label>
                  <input type="text" class="form-control" id="txtApplicationNumber" placeholder="Application Number">
                </div>                
              </div>              
            </form>
        </section>
       
        <section class="col-lg-3 connectedSortable">
              <div class="box-body">
                <div class="form-group">
                  <div class="form-group">
					  <label>Status</label>
					  <select id="form_status" class="form-control">
					    <option value="">Select</option>
						<option value="FORM_SUBMITTED">Form Submitted</option>
						<option value="FORM_NOT_SUBMITTED">Form Not Submitted</option>
						<option value="PRE_PAYMENT">Pre-Payment</option>						
					  </select>
					</div>
                </div>                
              </div>              
        </section>
		<section class="col-lg-4 connectedSortable">
			  <div class="box-body">
                <div class="form-group">
					<label><br/><br/></label>
					<button id="btn_reset" type="button" class="btn btn-primary">Reset</button>
					&nbsp;&nbsp;
					<button id="btn_filter" type="button" class="btn btn-primary">Search</button>	
                </div>                
              </div>              
        </section>	
		</form>
      </div>
	  
	  <div class="row">        
        <section id="section_tbl_filter_data" class="col-lg-12 connectedSortable">
			<table id="tbl_filter_data" class="display" width="100%" cellspacing="0" cellpadding="0">
				
			</table>	
	    </section>		        
      </div>
	  <div class="row">   
	  <section class="col-lg-4 connectedSortable">
			  <div class="box-body">
                <div class="form-group">
					<label><br/><br/></label>
					<button id="btn_xls" type="buton" class="btn btn-primary">Download</button>
                </div>                
              </div>              
        </section>	
	</div>   
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!------------------------- Modal -------------------------------->
<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Print Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body printModal">
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary" id="">Save changes</button>-->
      </div>
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->

<!------------------------- Modal -------------------------------->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:80%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Student Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body editModal">
	
      </div>
   
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->

<!------------------------- Modal -------------------------------->
<div class="modal fade" id="showDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Show document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		edit data		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
