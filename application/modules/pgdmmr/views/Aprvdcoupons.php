  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Applications
      </h1>
      <ol class="breadcrumb">
        <!--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>-->
      </ol>
    </section>

    <!-- Main content -->	
    <section class="content">
	  <div class="row">        
        <section class="col-lg-3 connectedSortable">
		<form role="form" id="search_coupon_form">
             <div class="box-body">
                <div class="form-group">
                  <label for="coupon_code">Coupon Code</label>
                  <input type="text" class="form-control" id="coupon_code" placeholder="Coupon Code">
                </div>                
              </div>          
         
        </section>
		 <section class="col-lg-3 connectedSortable">
              <div class="box-body">
                <div class="form-group">
                  <div class="form-group">
					  <label>Percentage</label>
					  <select id="discount_percentage" class="form-control">
					    <option value="">Select</option>
						<option value="10">10%</option>
						<option value="20">20%</option>
						<option value="50">50%</option>						
						<option value="100">100%</option>						
					  </select>
					</div>
                </div>                
              </div>              
        </section> 
		<section class="col-lg-4 connectedSortable">
			  <div class="box-body">
                <div class="form-group">
					<label><br/><br/></label>
					<button id="btn_reset" type="button" class="btn btn-primary">Reset</button>
					&nbsp;&nbsp;
					<button id="btn_filter" type="button" class="btn btn-primary">Search</button>	
					                </div>                
              </div>              
        </section>			
      </div>
	  </form>
	  <div class="row">
		  <section class="col-lg-4">
			<button id="btn_generate_coupons" type="button" data-toggle="modal" data-target="#couponModal" class="btn btn-success">Generate Coupons</button>	
		
		  </section>
	  </div>	  
	  <div class="row">        
        <section id="section_tbl_filter_data" class="col-lg-12 connectedSortable">
			<table id="tbl_coupons_data" class="display dataTable" width="100%" cellspacing="0" cellpadding="0">
				<thead>
						<tr>
							<th>Coupon id</th>
							<th>Coupon Code</th>							
							<th>Percentage</th>
							<th>Active</th>
							<th>Used</th>
														
						</tr>
					</thead>
					<tbody>
					<?php foreach($coupons as $item){ ?>
					<tr><td><?=$item['id']?></td><td><?=$item['coupon_code']?></td><td><?=$item['discount_percentage']?></td>
					<td><?=$item['status']?></td><td><?=$item['coupon_used']?></td> </tr>
					<?php }	?>
			</table>	
	    </section>		        
      </div>
	  <div class="row">   
	  <section class="col-lg-4 connectedSortable">
			  <div class="box-body">
                <div class="form-group">
					<label><br/><br/></label>
					<button id="btn_xls" type="buton" class="btn btn-primary">Download</button>
                </div>                
              </div>              
        </section>	
	</div>   
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!------------------------- Modal -------------------------------->
<div class="modal fade" id="couponModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Generate Coupons</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body couponModal">
		 <div class="row">        
        <section class="col-lg-5 connectedSortable">
		<form role="form" id="search_coupon_form" name='generate_coupon' action="<?=base_url()?>/index.php/pgdmmr/dashboard/generate_coupons" method='post'>
             <div class="box-body">
                <div class="form-group">
                  <label for="coupon_code">No.Of Coupons</label>
                  <input type="text" class="form-control" id="g_coupons" name="g_coupons" placeholder="No.of Coupons to be generated" required>
                </div>                
              </div>          
         
        </section>
		 <section class="col-lg-3 connectedSortable">
              <div class="box-body">
                 <div class="form-group">
					  <label>Percentage</label>
					  <select id="g_discount_percentage" name="g_discount_percentage" class="form-control" required>
					    <option value="">Select</option>
						<option value="10">10%</option>
						<option value="20">20%</option>
						<option value="50">50%</option>						
						<option value="100">100%</option>						
					  </select>
					</div>         
              </div>              
        </section> 
		<section class="col-lg-4 connectedSortable">
			  <div class="box-body">
                <div class="form-group"> <br/>
					<button id="btn_generate" type="submit" class="btn btn-warning">Generate</button>	
				</div>                
              </div>              
        </section>			
      </div>
	  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary" id="">Save changes</button>-->
      </div>
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->

<!------------------------- Modal -------------------------------->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:80%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Student Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body editModal">
	
      </div>
   
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->

<!------------------------- Modal -------------------------------->
<div class="modal fade" id="showDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Show document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		edit data		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
