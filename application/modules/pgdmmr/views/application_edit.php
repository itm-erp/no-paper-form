<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/fonts/font-awesome/css/font-awesome.css">

    <!-- Stylesheet
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/nivo-lightbox/nivo-lightbox.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/nivo-lightbox/default.css">    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/bootstrap-datepicker.css">    
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/jquery.fancybox.css?v=2.1.5" media="screen" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/jquery.fancybox-thumbs.css?v=1.0.7" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pgdmmr/css/bootstrap-select.min.css">

	<style>
		.fancybox-overlay {
			z-index: 1000000 !important;
		}

   </style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	<input id="baseurl" type="hidden" value="<?php echo base_url(); ?>"/>
	<input id="vertical_id" type="hidden" value="PGDM" />
	<input id="user_type" type="hidden" value="PGDM_STUDENT" />
	<input id="reg_id" type="hidden" value="<?php echo $step_data['reg_id']; ?>" />
	<input id="hdn_accept_terms_conditions" type="hidden" value="<?php echo $step_data['accept_terms_conditions']; ?>" />
	<input id="payment_done" type="hidden" value="<?php echo $step_data['payment_done']; ?>" />
	
 
    <!-- Contact Section -->
    <div class="text-center">
        <div class="container" style="width:100%;">            
             <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#step2" data-toggle="tab">Personal Information</a></li>
                            <li><a href="#step3" data-toggle="tab">Address Details</a></li>
                            <li><a href="#step4" data-toggle="tab">Campus Preference</a></li>
							<li><a href="#step5" data-toggle="tab">Academic Details</a></li>
							<li><a href="#step6" data-toggle="tab">Work Experience</a></li>
							<li><a href="#step7" data-toggle="tab">Upload Documents</a></li>
							
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                     
											
                        <div class="tab-pane fade in active" id="step2">
							  <form name="step2_form" id="step2_form" novalidate>
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group text-left">
									  <b>Applicants's Personal Details</b>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_title" class="control-label">Title</label>
									  <select id="stud_title" class="form-control" name="stud_title">
											<option value="">SELECT</option>
											<option value="MR" <?php if(isset($step1_data['stud_title']))echo (trim(strtoupper($step1_data['stud_title'])) == "MR") ? "selected" : "";?>>MR</option>
											<option value="MRS" <?php if(isset($step1_data['stud_title'])) echo (trim(strtoupper($step1_data['stud_title'])) == "MRS") ? "selected" : "";?>>MRS</option>
											<option value="MISS" <?php if(isset($step1_data['stud_title'])) echo (trim(strtoupper($step1_data['stud_title'])) == "MISS") ? "selected" : "";?>>MISS</option>
									  </select>
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_first_name" class="control-label" >First Name</label>
									  <input type="text" id="stud_first_name" class="form-control" name="stud_first_name" placeholder="First Name" value="<?php if(isset($step1_data['stud_first_name'])) echo $step1_data['stud_first_name']; ?>">							  
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_middle_name" class="control-label">Middle Name</label>
									  <input type="text" id="stud_middle_name" class="form-control" name="stud_middle_name" placeholder="Middle Name" value="<?php if(isset($step1_data['stud_middle_name'])) echo $step1_data['stud_middle_name']; ?>">
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_last_name" class="control-label">Last Name</label>
									  <input type="text" id="stud_last_name" class="form-control" name="stud_last_name" placeholder="Last Name" value="<?php  if(isset($step1_data['stud_last_name'])) echo $step1_data['stud_last_name']; ?>">
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_dob" class="control-label">Date of Birth</label>	
									  <input type="text" id="stud_dob" class="form-control" name="stud_dob" placeholder="Date of Birth" value="<?php echo ((trim($step1_data['stud_dob']) != "") && isset($step1_data['stud_dob']) && $step1_data['stud_dob'] != "0000-00-00 00:00:00")? date("d/m/Y", strtotime($step1_data['stud_dob'])):"";?>" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_gender" class="control-label">Gender</label>									  
									  <select id="stud_gender" class="form-control" name="stud_gender">
										<option value="">SELECT</option>
										<option value="MALE"  <?php if(isset($step1_data['stud_gender'])) echo (trim(strtoupper($step1_data['stud_gender'])) == "MALE") ? "selected" : "";?> >MALE</option>
										<option value="FEMALE" <?php if(isset($step1_data['stud_gender'])) echo (trim(strtoupper($step1_data['stud_gender'])) == "FEMALE") ? "selected" : "";?>>FEMALE</option>
									  </select>
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_email" class="control-label">Email Address</label>
									  <input type="text" id="stud_email" class="form-control" name="stud_email" placeholder="Email Address" value="<?php if(isset($step_data['student_email_id'])) echo $step_data['student_email_id']; ?>" readonly>
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="stud_mobileno" class="control-label">Mobile No</label>
									  <input type="text" id="stud_mobileno" class="form-control" name="stud_mobileno" placeholder="10 Digit Mobile No" value="<?php if(isset($step_data['student_mobile_no'])) echo $step_data['student_mobile_no']; ?>" maxlength="10">
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group text-left">
									  <b>Father's Details</b>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group">
									  <label for="father_first_name" class="control-label">First Name</label>	
									  <input type="text" id="father_first_name" class="form-control" name="father_first_name" placeholder="First Name" value="<?php if(isset($step1_data['father_first_name'])) echo ucwords($step1_data['father_first_name']); ?>">
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="father_middle_name" class="control-label">Midddle Name</label>
									  <input type="text" id="father_middle_name" class="form-control" name="father_middle_name" placeholder="Midddle Name" value="<?php if(isset($step1_data['father_middle_name'])) echo $step1_data['father_middle_name']; ?>">							  
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="father_last_name" class="control-label">Last Name</label>
									  <input type="text" id="father_last_name" class="form-control" name="father_last_name" placeholder="Last Name" value="<?php if(isset($step1_data['father_last_name'])) echo $step1_data['father_last_name']; ?>">
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="father_mobile" class="control-label">Mobile No</label>
									  <input type="text" id="father_mobile" class="form-control" name="father_mobile" placeholder="10 Digit Mobile No" value="<?php if(isset($step1_data['father_mobile'])) echo $step1_data['father_mobile']; ?>" maxlength="10">
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group text-left">
									  <b>Mothers's Details</b>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-3">
									<div class="form-group">
									  <label for="mother_first_name" class="control-label">First Name</label>	
									  <input type="text" id="mother_first_name" class="form-control" name="mother_first_name" placeholder="First Name" value="<?php if(isset($step1_data['mother_first_name'])) echo ucwords($step1_data['mother_first_name']); ?>">
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="mother_middle_name" class="control-label">Midddle Name</label>
									  <input type="text" id="mother_middle_name" class="form-control" name="mother_middle_name" placeholder="Midddle Name" value="<?php if(isset($step1_data['mother_middle_name'])) echo $step1_data['mother_middle_name']; ?>">							  
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="mother_last_name" class="control-label">Last Name</label>
									  <input type="text" id="mother_last_name" class="form-control" name="mother_last_name" placeholder="Last Name" value="<?php  if(isset($step1_data['mother_last_name'])) echo $step1_data['mother_last_name']; ?>">
									</div>
								  </div>

								  <div class="col-md-3">
									<div class="form-group">
									  <label for="mother_mobile" class="control-label">Mobile No</label>
									  <input type="text" id="mother_mobile" class="form-control" name="mother_mobile" placeholder="10 Digit Mobile No" value="<?php if(isset($step1_data['mother_mobile'])) echo $step1_data['mother_mobile']; ?>" maxlength="10">
									</div>
								  </div>	
								</div>
								
								<!-- <span id="step2_save_exit_btn" type="text" class="btn btn-custom btn-lg">Save &amp; Exit</span> -->
								<span id="step2_continue_btn" type="text" class="btn btn-primary">Save</span>
							  </form>
						</div>
						<!------------------------------------------------------------------------------------------->
                        <div class="tab-pane fade" id="step3">

						<form name="step3_form" id="step3_form" novalidate>
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Correspondence Address</b>
									</div>
								  </div>
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Permanent Address</b>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									  &nbsp;&nbsp;&nbsp;
									</div>
								  </div>
								  
								  <div class="col-md-6 text-left">
									<div class="form-group">
									  <input type="checkbox"  id="permanent_corres_same" name="permanent_corres_same" <?php (trim(strtoupper($step2_data['permanent_corres_same'])) == "Y") ? "checked" : "";?> style="cursor:pointer;">
									  <label for="permanent_corres_same" class="control-label">Same as Correspondence Address</label>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									  <label for="corres_address" class="control-label">Address</label>	
									  <input type="text" id="corres_address" class="form-control"  name="corres_address" placeholder="Correspondence Address" value="<?php if(isset($step2_data['corres_address'])) echo ucwords($step2_data['corres_address']); ?>">
									</div>
								  </div>

								  <div class="col-md-6 text-left">
									<div class="form-group">
									  <label for="permanent_address" class="control-label">Address</label>	
									  <input type="text" id="permanent_address" class="form-control" name="permanent_address" placeholder="Permanent Address" value="<?php if(isset($step2_data['permanent_address']))  echo ucwords($step2_data['permanent_address']); ?>">
									</div>
								  </div>								  
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group" >
									    <label for="find_your_location" class="control-label">Find Your Location</label>
									     <input type="text"id="autocomplete" class="form-control" name="autocomplete" placeholder="Find Your Location" onFocus="geolocate()" value="<?php if(isset($step2_data['corres_address'])) echo ucwords($step2_data['corres_address']); ?>">							  
									</div>
								  </div>

								  <div class="col-md-6">
								   <div class="form-group" >
									  <label for="find_your_location" class="control-label">Find Your Location</label>
									    <input type="text" id="autocomplete1" class="form-control" name="autocomplete1" placeholder="Find Your Location" onFocus="geolocate()" value="<?php if(isset($step2_data['corres_address'])) echo ucwords($step2_data['corres_address']); ?>">							  
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-6 text-left">
									<div class="form-group">
									  <input type="checkbox" id="unable_find_corres_location" name="unable_find_corres_location" style="cursor:pointer;">	
									  <label for="unable_find_corres_location" class="control-label">Unable to Find My Location. Want to Enter it Manually</label>									  						  
									</div>
								  </div>

								  <div class="col-md-6 text-left">
									<div class="form-group">
									  <input type="checkbox" id="unable_find_perm_location"  name="unable_find_perm_location" style="cursor:pointer;">
									  <label for="unable_find_perm_location" class="control-label">Unable to Find My Location. Want to Enter it Manually</label>									  
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									  <label for="corres_city" class="control-label">City Name</label>
									  <input type="text" id="corres_city" class="form-control locality"  name="corres_city" value="<?php if(isset($step2_data['corres_city'])) echo ucwords($step2_data['corres_city']); ?>" placeholder="City Name">							  
									</div>
								  </div>

								  <div class="col-md-6">
									<div class="form-group">
									  <label for="permanent_city" class="control-label">City Name</label>
									  <input type="text" id="permanent_city" class="form-control locality1" name="permanent_city" value="<?php if(isset($step2_data['permanent_city'])) echo ucwords($step2_data['permanent_city']); ?>" placeholder="City Name"> 
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									  <label for="corres_state" class="control-label">State Name</label>
									  <input type="text" id="corres_state" class="form-control administrative_area_level_1" name="corres_state" value="<?php if(isset($step2_data['corres_state'])) echo ucwords($step2_data['corres_state']); ?>" placeholder="State Name">							  
									</div>
								  </div>

								  <div class="col-md-6">
									<div class="form-group">
									  <label for="permanent_state" class="control-label">State Name</label>
									  <input type="text" id="permanent_state" class="form-control administrative_area_level_11 " name="permanent_state" value="<?php if(isset($step2_data['permanent_state'])) echo ucwords($step2_data['permanent_state']); ?>" placeholder="State Name">
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									  <label for="corres_pin" class="control-label">Pin</label>
									  <input type="text" id="corres_pin" class="form-control postal_code" name="corres_pin" value="<?php if(isset($step2_data['corres_pin'])) echo $step2_data['corres_pin']; ?>" placeholder="Pin" maxlength="6">							  
									</div>
								  </div>

								  <div class="col-md-6">
									<div class="form-group">
									  <label for="permanent_pin" class="control-label">Pin</label>
									  <input type="text" id="permanent_pin" class="form-control postal_code1" name="permanent_pin" value="<?php if(isset($step2_data['permanent_pin'])) echo $step2_data['permanent_pin']; ?>" placeholder="Pin" maxlength="6">
									</div>
								  </div>	
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									 <span id="step3_back_btn" type="text" class="btn btn-primary">Back</span>
								<!-- <span id="step3_save_exit_btn" type="text" class="btn btn-custom btn-lg">Save &amp; Exit</span>		-->							  
									</div>
								  </div>

								  <div class="col-md-6">
									<div class="form-group">
									  <span id="step3_continue_btn" type="text" class="btn btn-primary">Save</span>
									</div>
								  </div>	
								</div>
								
							</form>							
						</div>
						<!------------------------------------------------------------------------------------------->
                        <div class="tab-pane fade" id="step4">
							<form name="step4_form" id="step4_form" novalidate>
								<div class="row">
								  <div class="col-md-9 text-left">
									<div class="form-group">										
										<b>Select Preferred Campus And Course 1</b>
										<br/><br/>
										<div class="row">
										  <div class="col-md-6">
											<div class="form-group">
											  <label for="campus_pref_1" class="control-label">Select Campus</label>
											  <select id="campus_pref_1" class="form-control" name="campus_pref_1">
													<option value="">SELECT</option>
													<?php 
													  foreach($campuses_data as $row){ 
													  $selected= $campus_id_1==$row['campus_id']?"selected":"";
																									  ?>
														<option value="<?php echo $row['campus_id'] ?>" <?php echo $selected; ?> >
														<?php echo trim($row['campus_name'] . " " .$row['campus_city']); ?>
														</option>													
													<?php } ?>													
											  </select>
											</div>
										  </div>
										  <div class="col-md-6">
											<div class="form-group">
											  <label for="campus_specialization_1" class="control-label">Select Specialization</label>
											  <select id="campus_specialization_1" class="selectpicker" name="campus_specialization_1" multiple data-max-options="3"  >
											  	<?php	
												$specialization_ids=array();
												foreach($student_campus_preferences as $specialization_id){
													$specialization_ids[]=$specialization_id['specialization_id'];
												}
												if(isset($campus_id_1)){
														foreach($campus_specialization_11 as $item){
															$selected="";
															if(in_array($item['campus_specialization_id'],$specialization_ids))
															{
																$selected = "selected";
															}
															echo '<option value="'. $item['campus_specialization_id'] .'" '. $selected .'>'. $item['specialization_name'] .'</option>';																
													   }
													}
												?>
											  </select>
											</div>
										  </div>		
										</div>										
										
										<b>Select Preferred Campus And Course 2</b>
										<br/><br/>
										<div class="row">
										  <div class="col-md-6">
											<div class="form-group">
											  <label for="campus_pref_2" class="control-label">Select Campus</label>
											  <select id="campus_pref_2" class="form-control" name="campus_pref_2">
													<option value="">SELECT</option>
													<?php foreach($campuses_data as $row){ 
													 $selected= $campus_id_2==$row['campus_id']?"selected":"";
													?>
														<option value="<?php echo $row['campus_id'] ?>" <?php echo $selected; ?> >
														<?php echo trim($row['campus_name'] . " " .$row['campus_city']); ?>
														</option>													
													<?php } ?>													
											  </select>
											</div>
										  </div>

										  <div class="col-md-6">
											<div class="form-group">
											  <label for="campus_specialization_2" class="control-label">Select Specialization</label>											  
											  <select id="campus_specialization_2" class="selectpicker" multiple data-max-options="3" name="campus_specialization_2" >
												<?php
													if(isset($campus_id_2)){
														foreach($campus_specialization_22 as $item){
															$selected="";
															if(in_array($item['campus_specialization_id'],$specialization_ids))
															{
																$selected = "selected";
															}
															echo '<option value="'. $item['campus_specialization_id'] .'" '. $selected .'>'. $item['specialization_name'] .'</option>';																
													   }
													}									
												?>
											  </select>
											</div>
										  </div>		
										</div>
										
										<b>Select Process Location(GD/PI Location)</b>
										<br/><br/>
										<div class="row">
										  <div class="col-md-6">
											<div class="form-group">											  
											  <select id="gdpi_locations" class="form-control" name="gdpi_locations">
											  
													<?php foreach($gdpi_locations as $row){ 
													$selected=$row['gdpi_location_id'] == trim($gdpi_location_id) ? "selected" : "";
													?>
														<option value="<?php echo $row['gdpi_location_id']; ?>"  <?php echo $selected; ?> >									
														<?php echo trim($row['name']); ?>
														</option>													
													<?php } ?>													
											  </select>
											</div>
										  </div>										 
										  
										  <div class="col-md-6">
											<div class="form-group">
											  <label for="gdpi_available_dates" class="control-label">Available Dates</label>
											  <select id="gdpi_available_dates" class="selectpicker" data-max-options="1" name="gdpi_available_dates" >
												<?php 
													if(isset($get_gdpi_locations_date)){
														foreach($get_gdpi_locations_date as $row){ 
														$selected = "";
														if($row['gdpi_location_id'] == $gdpi_location_id){
															$selected = "selected";
														}
														echo '<option value="'. $row['gdpi_location_available_dates_id'] .'" '. $selected .'>'. date('d/m/Y',strtotime($row['available_date'])) .'</option>';					
														}
													}
												?>
											  </select>
											</div>
										  </div>		
										</div>
										
									</div>
								  </div>
												  
								  <div class="col-md-3">
									<div class="form-group">
										<br/>
										<div style="width:80%;height:300px;border:1px solid #cdcdcd;">
											Instructions
										</div>	
									</div>	
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									 <span id="step4_back_btn" type="text" class="btn btn-primary">Back</span>
								<!--	 <span id="step4_save_exit_btn" type="text" class="btn btn-custom btn-lg">Save &amp; Exit</span>		-->							  
									</div>
								  </div>

								  <div class="col-md-6">
									<div class="form-group">
									  <span id="step4_continue_btn" type="text" class="btn btn-primary">Save</span>
									</div>
								  </div>	
								</div>
								
							</form>
						</div>
						<!------------------------------------------------------------------------------------------->
                        <div class="tab-pane fade" id="step5">
							<form name="step5_form" id="step5_form" novalidate>
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Educational Qualification</b>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-1">
									<div class="form-group text-center">
									  &nbsp;
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Institution</b>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>City/Town</b>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Board/University</b>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Stream/Specialization</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <b>Degree</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <b>Year of Passing</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <b>Percentage of Marks</b>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-1">
									<div class="form-group text-center">
									  10th
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group">									  
									  <input type="text" id="ssc_institution" class="form-control" name="ssc_institution" value="<?php echo $step5_ssc_data['institute_name']; ?>" placeholder="Institution">							  
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" id="ssc_city" class="form-control" name="ssc_city" value="<?php echo $step5_ssc_data['city_name']; ?>" placeholder="City/Town">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select id="ssc_board_university" class="form-control" name="ssc_board_university">
										<option value="">SELECT</option>
										<?php foreach($board_university_master as $row){ ?>
										<option value="<?php echo $row['board_university_id'] ?>" <?php echo (trim($row['board_university_id']) == $step5_ssc_data['board_university']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  --
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									   <select id="ssc_degree" class="form-control" name="ssc_degree">
										<option value="">SELECT</option>
										<?php foreach($degree_master as $row){ ?>
										<option value="<?php echo $row['degree_master_id'] ?>" <?php echo (trim($row['degree_master_id']) == $step5_ssc_data['degree']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="ssc_passing_date" class="form-control" name="ssc_passing_date" value="<?php echo (trim($step5_ssc_data['passing_date']) != "") && isset($step5_ssc_data['passing_date']) && $step5_ssc_data['passing_date'] != "0000-00-00 00:00:00"? date("m/Y", strtotime($step5_ssc_data['passing_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="ssc_percentage" class="form-control" name="ssc_percentage" value="<?php echo $step5_ssc_data['percentage_obtained']; ?>" placeholder="Percentage of Marks">
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-1">
									<div class="form-group text-center">
									  12th
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group">									  
									  <input type="text" id="hsc_institution" class="form-control" name="hsc_institution"  value="<?php echo $step5_hsc_data['institute_name']; ?>" placeholder="Institution">							  
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" id="hsc_city" class="form-control" name="hsc_city" value="<?php echo $step5_hsc_data['city_name']; ?>" placeholder="City/Town">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select id="hsc_board_university" class="form-control" name="hsc_board_university">
										<option value="">SELECT</option>
										<?php foreach($board_university_master as $row){ ?>
										<option value="<?php echo $row['board_university_id'] ?>" <?php echo (trim($row['board_university_id']) == $step5_hsc_data['board_university']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select id="hsc_stream_specialization" class="form-control" name="hsc_stream_specialization">
										<option value="">SELECT</option>
										<?php foreach($stream_specialization_master as $row){ ?>
										<option value="<?php echo $row['stream_specialization_id'] ?>" <?php echo (trim($row['stream_specialization_id']) == $step5_hsc_data['board_university']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									   <select id="hsc_degree" class="form-control" name="hsc_degree">
										<option value="">SELECT</option>
										<?php foreach($degree_master as $row){ ?>
										<option value="<?php echo $row['degree_master_id'] ?>" <?php echo (trim($row['degree_master_id']) == $step5_hsc_data['degree']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="hsc_passing_date" class="form-control" name="hsc_passing_date" value="<?php echo (trim($step5_hsc_data['passing_date']) != "") && isset($step5_hsc_data['passing_date']) && $step5_hsc_data['passing_date'] != "0000-00-00 00:00:00"? date("m/Y", strtotime($step5_hsc_data['passing_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="hsc_percentage" class="form-control" name="hsc_percentage" value="<?php echo $step5_hsc_data['percentage_obtained']; ?>" placeholder="Percentage of Marks">
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-1">
									<div class="form-group text-center">
									  Graduation
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group">									  
									  <input type="text" id="graduation_institution" class="form-control" name="graduation_institution" value="<?php echo $step5_graduation_data['institute_name']; ?>" placeholder="Institution">							  
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" id="graduation_city" class="form-control" name="graduation_city" value="<?php echo $step5_graduation_data['city_name']; ?>" placeholder="City/Town">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select id="graduation_university" class="form-control" name="graduation_university">
										<option value="">SELECT</option>
										<?php foreach($board_university_master as $row){ ?>
										<option value="<?php echo $row['board_university_id'] ?>" <?php echo (trim($row['board_university_id']) == $step5_graduation_data['board_university']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">									  
									  <input type="text" id="graduate_stream_specialization"  class="form-control" name="graduate_stream_specialization" value="<?php echo $step5_graduation_data['stream_specialization_gen_txt']; ?>" placeholder="Stream/Specialization">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									   <select id="graduation_degree" class="form-control" name="graduation_degree">
										<option value="">SELECT</option>
										<?php foreach($degree_master as $row){ ?>
										<option value="<?php echo $row['degree_master_id'] ?>" <?php echo (trim($row['degree_master_id']) == $step5_graduation_data['degree']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="graduation_passing_date" class="form-control" name="graduation_passing_date" value="<?php echo (trim($step5_graduation_data['passing_date']) != "") && isset($step5_graduation_data['passing_date']) && $step5_graduation_data['passing_date'] != "0000-00-00 00:00:00"? date("m/Y", strtotime($step5_graduation_data['passing_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="graduation_percentage" class="form-control" name="graduation_percentage" value="<?php echo $step5_graduation_data['percentage_obtained']; ?>" placeholder="Percentage of Marks">
									</div>
								  </div>
								</div>
								
								
								<div class="row">
								  <div class="col-md-1">
									<div class="form-group text-center">
									  Post Graduation
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group">									  
									  <input type="text" id="post_graduation_institution" class="form-control" name="post_graduation_institution" value="<?php echo $step5_post_graduation_data['institute_name']; ?>" placeholder="Institution">							  
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" id="post_graduation_city" class="form-control" name="post_graduation_city" value="<?php echo $step5_post_graduation_data['city_name']; ?>" placeholder="City/Town">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select id="post_graduation_university" class="form-control" name="post_graduation_university">
										<option value="">SELECT</option>
										<?php foreach($board_university_master as $row){ ?>
										<option value="<?php echo $row['board_university_id'] ?>" <?php echo (trim($row['board_university_id']) == $step5_post_graduation_data['board_university']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" id="post_graduate_stream_specialization" class="form-control" name="post_graduate_stream_specialization" value="<?php echo $step5_post_graduation_data['stream_specialization_gen_txt']; ?>" placeholder="Stream/Specialization">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									   <select id="post_graduation_degree" class="form-control" name="post_graduation_degree">
										<option value="">SELECT</option>
										<?php foreach($degree_master as $row){ ?>
										<option value="<?php echo $row['degree_master_id'] ?>" <?php echo (trim($row['degree_master_id']) == $step5_post_graduation_data['degree']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="post_graduation_passing_date" class="form-control" name="post_graduation_passing_date" value="<?php echo (trim($step5_post_graduation_data['passing_date']) != "") && isset($step5_post_graduation_data['passing_date']) && $step5_post_graduation_data['passing_date'] != "0000-00-00 00:00:00"? date("m/Y", strtotime($step5_post_graduation_data['passing_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" id="post_graduation_percentage" class="form-control" name="post_graduation_percentage" value="<?php echo $step5_post_graduation_data['percentage_obtained']; ?>" placeholder="Percentage of Marks">
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-12">
									<div class="form-group text-center">
									  <hr style="border-top: 1px solid #8c8b8b;width:100%;"/>
									</div>
								  </div>
								</div>  
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Test Score</b>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-center">
									  &nbsp;
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Test</b>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Registration Number</b>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Rank</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <b>Score Obtained</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <b>Percentile Obtained</b>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <b>Date of Test</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <b>Result Status</b>
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  &nbsp;
									</div>
								  </div>
								</div>
								
							    <span id="span_exam_rows">
								<?php
								if(is_array($student_test_details) && !empty($student_test_details)){ 
									$numItems = count($student_test_details);
									$i = 0;
									$exam_row = "";
									foreach($student_test_details as $val){
										if(++$i === $numItems) {
											$exam_row = " exam_row ";
										}	
								?>								
								<div class="row <?php echo $exam_row; ?>" >
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select class="form-control select_test_exams" name="select_test_exams">
										<option value="">SELECT</option>
										<?php foreach($exam_test_master as $row){ ?>
										<option value="<?php echo $row['exam_test_id'] ?>" <?php echo (trim($row['exam_test_id']) == $val['exam_test_id']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control test_registration_number" name="test_registration_number" value="<?php echo (trim($val['score_obtained']) != "") && isset($val['score_obtained']) ? $val['score_obtained']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_rank" name="test_rank" value="<?php echo (trim($val['rank']) != "") && isset($val['rank']) ? $val['rank']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_score_obtained"  name="test_score_obtained" value="<?php echo (trim($val['score_obtained']) != "") && isset($val['score_obtained']) ? $val['score_obtained']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_percentile_obtained" name="test_percentile_obtained" value="<?php echo (trim($val['percentage_obtained']) != "") && isset($val['percentage_obtained']) ? $val['percentage_obtained']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_date" name="test_date" value="<?php echo (trim($val['test_date']) != "") && isset($val['test_date']) && (trim($val['test_date']) != "0000-00-00 00:00:00") ? date("d/m/Y", strtotime($val['test_date'])) :"";?>" placeholder="DD/MM/YYYY" >
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_result_status" name="test_result_status" value="<?php echo (trim($val['result_status']) != "") && isset($val['result_status']) ? $val['result_status']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <span class="delete_exam_row" style="cursor:pointer;">X</span>
									</div>
								  </div>
								</div>
								
								<?php } } else { ?>
								<div class="row exam_row" >
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <select class="form-control select_test_exams" name="select_test_exams">
										<option value="">SELECT</option>
										<?php foreach($exam_test_master as $row){ ?>
										<option value="<?php echo $row['exam_test_id'] ?>">
											<?php echo trim($row['name']); ?>
										</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control test_registration_number" name="test_registration_number" value="" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_rank" name="test_rank" value="" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_score_obtained" name="test_score_obtained" value="" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_percentile_obtained" name="test_percentile_obtained" value="" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_date" name="test_date"  value="" placeholder="DD/MM/YYYY" >
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <input type="text" class="form-control test_result_status" name="test_result_status" value="" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-1">
									<div class="form-group text-center">
									  <span class="delete_exam_row" style="cursor:pointer;">X</span>
									</div>
								  </div>
								</div>
								<?php } ?>
							    </span>
								<div class="row">
								  <div class="col-md-12">
									<div class="form-group text-left">
									   <div id="btn_add_more_exams" class="btn btn-primary btn-sm" style="background-color:#61b329;border-color:#61b329;cursor:pointer;">Add More</div>
									</div>
								  </div>
								</div>
								
							
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group">
									 <span id="step5_back_btn" type="text" class="btn btn-primary">Back</span>
									<!--  <span id="step5_save_exit_btn" type="text" class="btn btn-custom btn-lg">Save &amp; Exit</span>	 -->								  
									</div>
								  </div>

								  <div class="col-md-6">
									<div class="form-group">
									  <span id="step5_continue_btn" type="text" class="btn btn-primary">Continue</span>
									</div>
								  </div>	
								</div>
								
							</form>	
						</div>
                        <!------------------------------------------------------------------------------------------->
						<div class="tab-pane fade" id="step6">
							<form name="step6_form" id="step6_form" novalidate>
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Work Experience</b>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-left">
									  Company
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  Designation
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  From Year
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  To Year
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  Monthly Remuneration
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  Reason for Leaving
									</div>
								  </div>								  
								</div>
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_company"  name="work_experience_company"
									  value="<?php if (isset($student_work_experience[0]['company_name'])) echo (trim($student_work_experience[0]['company_name']) != "") && isset($student_work_experience[0]['company_name']) ? $student_work_experience[0]['company_name']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_designation" name="work_experience_designation" value="<?php if (isset($student_work_experience[0]['designation'])) echo (trim($student_work_experience[0]['designation']) != "") && isset($student_work_experience[0]['designation']) ? $student_work_experience[0]['designation']:"";?>" placeholder="">
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_from_year" name="work_experience_from_year" value="<?php if (isset($student_work_experience[0]['start_date'])) echo (trim($student_work_experience[0]['start_date']) != "") && isset($student_work_experience[0]['start_date']) && $student_work_experience[0]['start_date'] != "0000-00-00 00:00:00"? date("m/Y", strtotime($student_work_experience[0]['start_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_to_year" name="work_experience_to_year" value="<?php if (isset($student_work_experience[0]['end_date'])) echo (trim($student_work_experience[0]['end_date']) != "") && isset($student_work_experience[0]['end_date']) && $student_work_experience[0]['end_date'] != "0000-00-00 00:00:00" ? date("m/Y", strtotime($student_work_experience[0]['end_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_monthly_remuneration" name="work_experience_monthly_remuneration" value="<?php if (isset($student_work_experience[0]['monthly_salary']))
									  echo (trim($student_work_experience[0]['monthly_salary']) != "" 
										&& isset($student_work_experience[0]['monthly_salary']) 
										&& floatval($student_work_experience[0]['monthly_salary']) != 0.00) ? $student_work_experience[0]['monthly_salary']:"";?>" placeholder="">
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_reason_leaving" name="work_experience_reason_leaving" value="<?php if (isset($student_work_experience[0]['reason_leaving'])) echo (trim($student_work_experience[0]['reason_leaving']) != "") && isset($student_work_experience[0]['reason_leaving']) ? $student_work_experience[0]['reason_leaving']:"";?>" placeholder="">
									</div>
								  </div>								  
								</div>
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_company"  name="work_experience_company"
									  value="<?php if (isset($student_work_experience[1]['company_name'])) echo (trim($student_work_experience[1]['company_name']) != "") && isset($student_work_experience[1]['company_name']) ? $student_work_experience[1]['company_name']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_designation" name="work_experience_designation" value="<?php if (isset($student_work_experience[1]['designation'])) echo (trim($student_work_experience[1]['designation']) != "") && isset($student_work_experience[1]['designation']) ? $student_work_experience[1]['designation']:"";?>" placeholder="">
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_from_year" name="work_experience_from_year" value="<?php if (isset($student_work_experience[1]['start_date'])) echo (trim($student_work_experience[1]['start_date']) != "") && isset($student_work_experience[1]['start_date']) && $student_work_experience[1]['start_date'] != "0000-00-00 00:00:00" ? date("m/Y", strtotime($student_work_experience[1]['start_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_to_year" name="work_experience_to_year" value="<?php if (isset($student_work_experience[1]['end_date'])) echo (trim($student_work_experience[1]['end_date']) != "") && isset($student_work_experience[1]['end_date']) && $student_work_experience[1]['end_date'] != "0000-00-00 00:00:00" ? date("m/Y", strtotime($student_work_experience[1]['end_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_monthly_remuneration" name="work_experience_monthly_remuneration" value="<?php  if (isset($student_work_experience[1]['monthly_salary']))
									  echo (trim($student_work_experience[1]['monthly_salary']) != "") 
										&& isset($student_work_experience[1]['monthly_salary'])
										&& floatval($student_work_experience[1]['monthly_salary']) != 0.00 ? $student_work_experience[1]['monthly_salary']:"";?>" placeholder="">
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_reason_leaving" name="work_experience_reason_leaving" value="<?php if (isset($student_work_experience[1]['reason_leaving'])) echo (trim($student_work_experience[1]['reason_leaving']) != "") && isset($student_work_experience[1]['reason_leaving']) ? $student_work_experience[1]['reason_leaving']:"";?>" placeholder="">
									</div>
								  </div>								  
								</div>
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_company" name="work_experience_company"
									  value="<?php if (isset($student_work_experience[2]['company_name'])) echo (trim($student_work_experience[2]['company_name']) != "") && isset($student_work_experience[2]['company_name']) ? $student_work_experience[2]['company_name']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_designation" name="work_experience_designation" value="<?php if (isset($student_work_experience[2]['designation'])) echo (trim($student_work_experience[2]['designation']) != "") && isset($student_work_experience[2]['designation']) ? $student_work_experience[2]['designation']:"";?>" placeholder="">
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_from_year" name="work_experience_from_year" value="<?php if (isset($student_work_experience[2]['start_date'])) echo (trim($student_work_experience[2]['start_date']) != "") && isset($student_work_experience[2]['start_date']) && $student_work_experience[2]['start_date'] != "0000-00-00 00:00:00" ? date("m/Y", strtotime($student_work_experience[2]['start_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_to_year" name="work_experience_to_year" value="<?php if (isset($student_work_experience[2]['end_date'])) echo (trim($student_work_experience[2]['end_date']) != "") && isset($student_work_experience[2]['end_date']) && $student_work_experience[2]['end_date'] != "0000-00-00 00:00:00" ? date("m/Y", strtotime($student_work_experience[2]['end_date'])):"";?>" placeholder="MM/YYYY" readonly='true'>
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_monthly_remuneration" name="work_experience_monthly_remuneration" value="<?php if (isset($student_work_experience[2]['monthly_salary']))
									  echo (trim($student_work_experience[2]['monthly_salary']) != "") 
									  && isset($student_work_experience[2]['monthly_salary']) 
									  && floatval($student_work_experience[2]['monthly_salary']) != 0.00 ? $student_work_experience[2]['monthly_salary']:"";?>" placeholder="">
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  <input type="text" class="form-control work_experience_reason_leaving" name="work_experience_reason_leaving" value="<?php if (isset($student_work_experience[2]['reason_leaving'])) echo (trim($student_work_experience[2]['reason_leaving']) != "") && isset($student_work_experience[2]['reason_leaving']) ? $student_work_experience[2]['reason_leaving']:"";?>" placeholder="">
									</div>
								  </div>								  
								</div>
								
								<div class="row">
								  <div class="col-md-12 text-left">
									<small>Mention full time paid employment after graduation. Do not include Training/Project Work.</small>
								  </div>
								</div> 
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>								  
								</div>
								
								<div class="row">
								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								  
								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-2">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>

								  <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="total_work_experience" class="control-label">Total Period of Work Experience(In Months)</label>	
									  <input type="text" id="total_work_experience" name="total_work_experience" class="form-control total_work_experience" value="" placeholder="EG. 36 MONTHS">
									</div>
								  </div>						  
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<hr style="width:100%;"/>
									</div>
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Referrer is</b>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="referrer_name" class="control-label">Name</label>	
									  <input type="text" id="referrer_name" class="form-control" name="referrer_name" value="<?php if (isset($student_referrer_details['name']))  echo (trim($student_referrer_details['name']) != "") && isset($student_referrer_details['name']) ? $student_referrer_details['name']:"";?>" placeholder="NAME">
									</div>
								  </div>
								  
								  <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="referrer_mobile_no" class="control-label">Mobile No.</label>	
									  <input type="text" id="referrer_mobile_no"  class="form-control"  name="referrer_mobile_no" value="<?php if (isset($student_referrer_details['mobile_no']))  echo (trim($student_referrer_details['mobile_no']) != "") && isset($student_referrer_details['mobile_no']) ? $student_referrer_details['mobile_no']:"";?>" placeholder="Mobile No">
									</div>
								  </div>
								  
								  <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="referrer_email_id" class="control-label">Email Id.</label>	
									  <input type="text" id="referrer_email_id" class="form-control" name="referrer_email_id" value="<?php if (isset($student_referrer_details['email_id']))  echo (trim($student_referrer_details['email_id']) != "") && isset($student_referrer_details['email_id']) ? $student_referrer_details['email_id']:"";?>" placeholder="Email Id">
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="referrer_is" class="control-label">Referrer Is</label>	
									  <select id="referrer_is" class="form-control" name="referrer_is">
										<option value="">Select</option>
										<?php foreach($referrer_is_master as $row){ ?>
											<option value="<?php echo $row['referrer_is_id'] ?>"  <?php  echo (trim($row['referrer_is_id']) == $student_referrer_details['referrer_is_id']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
											</option>													
										<?php } ?>											
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="referrer_campus" class="control-label">Campus</label>	
									  <select id="referrer_campus" class="form-control" name="referrer_campus">
										<option value="">Select</option>
										<?php foreach($campuses_data as $row){ ?>
											<option value="<?php echo $row['campus_id'] ?>"  <?php echo (trim($row['campus_id']) == $student_referrer_details['campus_id']) ? "selected" : "";?>>
											<?php echo trim($row['campus_name'] . " " .$row['campus_city']); ?>
											</option>													
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-4">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								</div>
								
								<div class="row">
								   <div class="col-md-4">
									<div class="form-group text-left">
									  <label for="referrer_source" class="control-label">Source</label>										  
									  <select id="referrer_source" class="form-control" name="referrer_source">
										<option value="">Select</option>										
										<?php foreach($referrer_source_master as $row){ ?>
											<option value="<?php echo $row['referrer_source_id'] ?>"  <?php echo (trim($row['referrer_source_id']) == $student_referrer_details['referrer_source_id']) ? "selected" : "";?>>
											<?php echo trim($row['name']); ?>
											</option>													
										<?php } ?>
										
									  </select>
									</div>
								  </div>
								  <?php
									 $referrer_source_coaching_class = "none";
									 if(isset($student_referrer_details['coatching_class_id']) && !empty($student_referrer_details['coatching_class_id']) && $student_referrer_details['coatching_class_id'] != ""){
										$referrer_source_coaching_class = "";
									 }
								  ?>								  
								   <div class="col-md-4 referrer_source_coaching_class" style="display:<?php echo $referrer_source_coaching_class; ?>">
									<div class="form-group text-left">
									  <label for="referrer_coaching_class" class="control-label">Coaching Class</label>	
									  <select id="referrer_coaching_class" class="form-control" name="referrer_coaching_class">
										<option value="">Select</option>
										<?php foreach($coaching_class_master as $row){ ?>
											<option value="<?php echo $row['coaching_class_id'] ?>" <?php echo (trim($row['coaching_class_id']) == $student_referrer_details['coatching_class_id']) ? "selected" : "";?>> <?php echo trim($row['name']); ?>
											</option>												
										<?php } ?>													
									  </select>
									</div>
								  </div>
								  
								  <div class="col-md-4 referrer_source_coaching_class" style="display:<?php echo $referrer_source_coaching_class; ?>">
									<div class="form-group text-left">
									  <label for="referrer_city_name" class="control-label">City</label>	
									  <input type="text" id="referrer_city_name" class="form-control" name="referrer_city_name" value="<?php echo (trim($student_referrer_details['city']) != "") && isset($student_referrer_details['city']) ? $student_referrer_details['city']:"";?>" placeholder="">
									</div>
								  </div>
								  
								  <?php
									 $referrer_code_cls = "none";
									 if(isset($student_referrer_details['other_source']) && !empty($student_referrer_details['other_source']) && $student_referrer_details['other_source'] != ""){
										$referrer_code_cls = "";
									 }
								  ?>
								  <div class="col-md-4 referrer_code_cls" style="display:<?php echo $referrer_code_cls;?>">
									<div class="form-group text-left">
									  <label for="referrer_code" class="control-label">Referrer Code</label>	
									  <input type="text" id="referrer_code" class="form-control" name="referrer_code" value="<?php echo (trim($student_referrer_details['other_source']) != "") && isset($student_referrer_details['other_source']) ? $student_referrer_details['other_source']:"";?>" placeholder="">
									</div>
								  </div>
								</div>
								  
								<div class="row">
									<div class="col-md-6">
									    <div class="form-group">
										 <span id="step6_back_btn" type="text" class="btn btn-primary">Back</span>
									<!-- <span id="step6_save_exit_btn" type="text" class="btn btn-primary">Save &amp; Exit</span>	-->							  
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										  <span id="step6_continue_btn" type="text"  class="btn btn-primary">Save</span>
										</div>
									</div>	
								</div>
							</form>
						</div>
						<!------------------------------------------------------------------------------------------->
                        <div class="tab-pane fade" id="step7">							
							<form name="step7_form" id="step7_form" novalidate enctype="multipart/formdata" action="#">
								<input id="reg_id_step7_form" type="hidden" value="<?php echo $step_data['reg_id']; ?>" />
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <b>Upload Documents</b>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									  &nbsp;
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <label for="student_recent_photograph" class="control-label">Your Recent Photograph</label>	
									  <input type="file" accept="application/pdf,image/*" name="student_recent_photograph[]" class="form-control" value="">
									  <?php	
										if(is_array($admission_document) && !empty($admission_document)){
											foreach($admission_document as $admission_document_val){
												if(is_array($admission_document_val) && !empty($admission_document_val)){
													if($admission_document_val['doc_type'] == 'RECENT_PHOTOGRAPH'){
														echo "<small><a href='". base_url() . $admission_document_val['doc_physical_path'] ."' style='cursor:pointer;' target='_blank'>Click Here to View File</a></small>";
													}
												}													
											}
										}	
									  ?>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <label for="student_ssc_marksheet" class="control-label">Upload Your 10th Marksheet</label>	
									  <input type="file" accept="application/pdf,image/*" name="student_ssc_marksheet[]" class="form-control" value="">
									  <?php										
										if(is_array($admission_document) && !empty($admission_document)){
											foreach($admission_document as $admission_document_val){
												if(is_array($admission_document_val) && !empty($admission_document_val)){
													if($admission_document_val['doc_type'] == 'SSC'){
														echo "<small><a href='". base_url() . $admission_document_val['doc_physical_path'] ."' style='cursor:pointer;' target='_blank'>Click Here to View File</a></small>";
													}
												}													
											}
										}	
									  ?>
									</div>
								  </div>
								</div>
								
								<div class="row">
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <br/>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									  <br/>
									</div>
								  </div>
								</div>
								
								<div class="row">
								   <div class="col-md-6">
									<div class="form-group text-left">
									  <label for="student_hsc_marksheet" class="control-label">Upload Your 12th Marksheet</label>	
									  <input type="file" accept="application/pdf,image/*" name="student_hsc_marksheet[]" class="form-control" value="">
									  <?php										
										if(is_array($admission_document) && !empty($admission_document)){
											foreach($admission_document as $admission_document_val){
												if(is_array($admission_document_val) && !empty($admission_document_val)){
													if($admission_document_val['doc_type'] == 'HSC'){
														echo "<small><a href='". base_url() . $admission_document_val['doc_physical_path'] ."' style='cursor:pointer;' target='_blank'>Click Here to View File</a></small>";
													}
												}													
											}
										}	
									  ?>
									</div>
								  </div>
								  
								  <div class="col-md-6">
									<div class="form-group text-left">
									 <label for="student_graduation_marksheet" class="control-label">Upload Your Graduation Marksheet</label>	
									  <input type="file" accept="application/pdf,image/*" name="student_graduation_marksheet[]" class="form-control" value="">
									  <?php										
										if(is_array($admission_document) && !empty($admission_document)){
											foreach($admission_document as $admission_document_val){
												if(is_array($admission_document_val) && !empty($admission_document_val)){
													if($admission_document_val['doc_type'] == 'GRADUATION'){
														echo "<small><a href='". base_url() . $admission_document_val['doc_physical_path'] ."' style='cursor:pointer;' target='_blank'>Click Here to View File</a></small>";
													}
												}													
											}
										}	
									  ?>
									</div>
								  </div>
								</div>
								
								<div class="row">
									<div class="col-md-6">
									    <div class="form-group">
										 <span id="step7_back_btn" type="text" class="btn btn-primary">Back</span>
										<!-- <span id="step7_save_exit_btn" type="text" class="btn btn-primary">Save &amp; Exit</span>	-->								  
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
										  <span id="step7_continue_btn" type="text" class="btn btn-primary">Save</span>
										</div>
									</div>	
								</div>
								
							</form>	
						</div>						
						<!------------------------------------------------------------------------------------------->
                     
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	
	
    <!--<div id="footer" style="padding:0 0 0 0;">	 	
        <div class="container-fluid text-center copyrights">
            &copy;
        </div>
    </div>-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/nivo-lightbox.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jquery.isotope.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jqBootstrapValidation.js"></script>
	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/pgdmmr.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/bootstrap-datepicker-au.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/angular.min.1.6.4.js"></script>	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jquery.mousewheel.pack.js?v=3.1.3"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jquery.fancybox.pack.js?v=2.1.5"></script>	
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jquery.fancybox-buttons.js?v=1.0.5"></script>		
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/jquery.fancybox-media.js?v=1.0.6"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/pgdmmr/js/googleaddress.js"></script>
	

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqLiG8kPgjqiayBYmBK6ciwI3ADdJrwhY&libraries=places&callback=initAutocomplete" async defer></script>  
	

</body>

</html>