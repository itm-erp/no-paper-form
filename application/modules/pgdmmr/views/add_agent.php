<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Agent Management
        <small>Add / Edit Agent</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Agent Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addagent" action="<?php echo base_url() ?>index.php/pgdmmr/dashboard/addNew" method="post" role="form"  enctype="multipart/form-data">
                        <div class="box-body">
							<div class="row">                                                              
                            <div class="col-md-4">
                                    <div class="form-group">
                                       <label for="f_name">First Name </label><br/>
										<input type="text" id="f_name" placeholder="First Name" name="f_name" class="form-control required" value="" >
                                    </div>
                            </div>  
							<div class="col-md-4">
                                    <div class="form-group">
										<label  for="m_name">Middle Name </label><br/>
										<input type="text" id="m_name" placeholder="Middle Name" name="m_name" class="form-control required" value="" >
										                                        
                                    </div>
                            </div>
							<div class="col-md-4">
                                    <div class="form-group">
										<label  for="l_name">Last Name </label><br/>
										<input type="text" id="l_name" placeholder="Last Name" name="l_name" class="form-control required" value="" >
										                                        
                                    </div>
                            </div>
                             
                               
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="agent_type">Agent Type</label>
                                         <select class="form-control required" id="agent_type" name="agent_type">
                                          
                                           <?php
											if(!empty($agent_types))
											{
												foreach($agent_types as $key=>$value)
												{ ?>
													<option value="<?php echo $value['id']; ?>" ><?php echo $value['name']; ?></option>
												   <?php
												}
											}
											?>
										</select>
                                     
                                    </div>
                                    
                                </div>
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="consultancy_name">Consultancy Name</label>
                                        <input type="text" class="form-control required" id="consultancy_name"  name="consultancy_name" placeholder="Consultancy Name" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact_number">Contact Number</label>
                                        <input type="text" class="form-control required"   name="contact_number"  id="contact_number"  placeholder="Contact Number" maxlength="10">
                                    </div>
                                </div>
                     
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control required"   name="email"  id="email" placeholder="Email" maxlength="128">
									</div> 
                                </div>
                               
                            </div>	
							 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="campus">Campus</label>
                                        <select class="form-control required" id="campus" name="campus">
                                          
                                           <?php
											if(!empty($campus))
											{
												foreach($campus as $key=>$value)
												{ ?>
													<option value="<?php echo $value['campus_id']; ?>" ><?php echo $value['campus_name']; ?></option>
												   <?php
												}
											}
											?>
										</select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="passing_marks">Address</label>
                                      	<textarea class="form-control required"  name="address" id="address" placeholder="Address" ></textarea>
                                    </div>
                                </div>
                               
                            </div>
						
							
							<div class="row">                     
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="city">City</label>
                                        <select class="form-control required" id="city" name="city">
                                          
                                           <?php
											if(!empty($cities))
											{
												foreach($cities as $key=>$value)
												{ ?>
													<option value="<?php echo $value['city_id']; ?>" ><?php echo $value['name']; ?></option>
												   <?php
												}
											}
											?>
										</select>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                       <label for="upload_mou">Upload MOU Doc</label>
                                        <input type="file" class="form-control"   name="upload_mou"  id="upload_mou"> 
										
                                    </div>
                                </div>
						    </div>
							<div class="row">     
                               <div class="col-md-6">
                                    <div class="form-group">
									   <label for="status">Status</label>
                                       <input type="radio"   name="status" value="1"  checked>Active &nbsp;&nbsp;&nbsp;&nbsp;
                                       <input  type="radio"    name="status" value="0" >   Deactive
									</div>
                                </div>
                            </div>
                          
                       
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>



<script src="<?php echo base_url(); ?>assets/agent/js/add.js" type="text/javascript"></script>