<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ITM-Group of Institutions</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/ionicons.min.css">
  <!-- Theme style --> 
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Jquery Alert Css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/jAlert.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/pgdmmr.css">  
</head>


<body class="hold-transition skin-blue sidebar-mini">
<input id="baseurl" type="hidden" value="<?php echo base_url(); ?>"/>
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>PGDM</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>PGDM</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">          
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url(); ?>assets/pgdmmr/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['user_id']; ?> </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url(); ?>assets/pgdmmr/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['user_id']; ?> 
			      <small><?php echo $_SESSION['user_type']; ?> </small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() . 'index.php/pgdmmr/login/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <!--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/pgdmmr/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Adminstrator</p>
          <!--<a href="#"><i class="fa fa-circle text-success"></i>Online</a>-->
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/index'; ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
		<li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/applications'; ?>">
            <i class="fa fa-dashboard" id="applications"></i><span>Applications</span>            
          </a>
        </li>
		<?php if($_SESSION['user_type']=="PGDM_MARKETING_HEAD") { ?> 
		<li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/get_coupons_approved'; ?>">
            <i class="fa fa-dashboard"></i><span>Coupons Approval</span>            
          </a>
        </li>
		<li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/get_coupons'; ?>">
            <i class="fa fa-dashboard"></i><span>Coupons</span>            
          </a>
        </li>
		<?php } else { ?>
		<li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/get_coupons'; ?>">
            <i class="fa fa-dashboard"></i><span>Coupons</span>            
          </a>
        </li>
		<?php } ?>
			<li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/get_agents'; ?>">
            <i class="fa fa-dashboard"></i><span>Agents</span>            
          </a>
        </li>
		<li class="active treeview">
          <a href="<?php echo base_url() . 'index.php/pgdmmr/dashboard/ticket_details'; ?>">
            <i class="fa fa-dashboard"></i><span>Ticket Details</span>            
          </a>
        </li>
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>  