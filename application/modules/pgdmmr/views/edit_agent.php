<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Agent Management
        <small>Add / Edit Agent</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Agent Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="edit_agent" action="<?php echo base_url() ?>agent/update_agent" method="post" role="form"  enctype="multipart/form-data">
                        <div class="box-body">
							<div class="row">                                                              
                            <div class="col-md-4">
                                    <div class="form-group">
                                       <label for="f_name">First Name </label><br/>
										<input type="text" id="f_name" placeholder="First Name" name="f_name" class="form-control required" value="<?php if(isset($data->first_name)) echo $data->first_name; ?>" >
										<input type="hidden" name="agent_id" value="<?php if(isset($data->id)) echo $data->id; ?>" />
									</div>
                            </div>  
							<div class="col-md-4">
                                    <div class="form-group">
										<label  for="m_name">Middle Name </label><br/>
										<input type="text" id="m_name" placeholder="Middle Name" name="m_name" class="form-control required" value="<?php if(isset($data->middle_name)) echo $data->middle_name; ?>" >
										                                        
                                    </div>
                            </div>
							<div class="col-md-4">
                                    <div class="form-group">
										<label  for="l_name">Last Name </label><br/>
										<input type="text" id="l_name" placeholder="Last Name" name="l_name" class="form-control required" value="<?php if(isset($data->last_name)) echo $data->last_name; ?>" >
										                                        
                                    </div>
                            </div>
                             
                               
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="agent_type">Agent Type</label>
                                         <select class="form-control required" id="agent_type" name="agent_type">
                                          
                                           <?php
											if(!empty($agent_types))
											{
												foreach($agent_types as $key=>$value)
												{
												    $selected=$value['id']==$data->agent_type_id?"selected":"";		?>
													<option value="<?php echo $value['id']; ?>" <?=$selected?> ><?php echo $value['name']; ?></option>
												   <?php
												}
											}
											?>
										</select>
                                     
                                    </div>
                                    
                                </div>
								 <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="consultancy_name">Consultancy Name</label>
                                        <input type="text" class="form-control required" id="consultancy_name"  name="consultancy_name" placeholder="Consultancy Name" maxlength="128" value="<?php if(isset($data->consultancy_name)) echo $data->consultancy_name; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact_number">Contact Number</label>
                                        <input type="text" class="form-control required"   name="contact_number"  id="contact_number"  placeholder="Contact Number" maxlength="10" value="<?php if(isset($data->mobile)) echo $data->mobile; ?>">
                                    </div>
                                </div>
                     
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control required"   name="email"  id="email" placeholder="Email" maxlength="128" value="<?php if(isset($data->email)) echo $data->email; ?>" readonly>
									</div> 
                                </div>
                               
                            </div>	
							 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="campus">Campus</label>
                                        <select class="form-control required" id="campus" name="campus" value="">
                                          
                                           <?php
											if(!empty($campus))
											{
												foreach($campus as $key=>$value)
												{ 
												 $selected=$value['id']==$data->campus_id?"selected":"";
												?>
													<option value="<?php echo $value['id']; ?>"  <?=$selected?> ><?php echo $value['campus_name']; ?></option>
												   <?php
												}
											}
											?>
										</select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="passing_marks">Address</label>
                                      	<textarea class="form-control required"  name="address" id="address" placeholder="Address" ><?php if(isset($data->address)) echo $data->address; ?></textarea>
                                    </div>
                                </div>
                               
                            </div>
						
							
							<div class="row">                     
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="city">City</label>
                                        <select class="form-control required" id="city" name="city">
                                          
                                           <?php
											if(!empty($cities))
											{
												foreach($cities as $key=>$value)
												{ 
												    $selected=$value['id']==$data->city?"selected":"";?>
													<option value="<?php echo $value['id']; ?>" <?=$selected?> ><?php echo $value['name']; ?></option>
												   <?php
												}
											}
											?>
										</select>
                                    </div>
                                </div>
								<?php if(!isset($data->mou_doc)){?>
								<div class="col-md-6">
                                    <div class="form-group">
                                       <label for="upload_mou">Upload MOU Doc</label>
                                        <input type="file" class="form-control"   name="upload_mou"  id="upload_mou"> 
										
                                    </div>
                                </div>
							  <?php } ?>
						    </div>
							<?php
							$check1=$data->status==1?"checked":"";
							$check2=$data->status==0?"checked":"";
							
							?>
							<div class="row">     
                               <div class="col-md-6">
                                    <div class="form-group">
									   <label for="status">Status</label>
                                       <input type="radio"   name="status" value="1"  <?=$check1?>>Active &nbsp;&nbsp;&nbsp;&nbsp;
                                       <input  type="radio"    name="status" value="0" <?=$check2?>>Deactive
									</div>
                                </div>
                            </div>
                          
                       
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>



<script src="<?php echo base_url(); ?>assets/agent/js/edit.js" type="text/javascript"></script>