  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Applications
      </h1>
      <ol class="breadcrumb">
        <!--<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>-->
      </ol>
    </section>

    <!-- Main content -->	
    <section class="content">
	  <div class="row">        
        <section class="col-lg-3 connectedSortable">
		<form role="form" id="search_apprv_coupon_form" action="<?=base_url()?>/index.php/pgdmmr/dashboard/search_coupons_approved" method="post">
             <div class="box-body">
                <div class="form-group">
                  <label for="coupon_code">Coupon Code</label>
                  <input type="text" class="form-control" id="coupon_code"  name="coupon_code"  placeholder="Coupon Code" <?php if(isset($coupon_code)) echo $coupon_code; ?>>
                </div>                
              </div>          
         
        </section>
		 <section class="col-lg-3 connectedSortable">
              <div class="box-body">
                <div class="form-group">
                  <div class="form-group">
					  <label>Percentage</label>
					  <select id="discount_percentage" class="form-control" name="discount_percentage" >
					    <option value="">Select</option>
						<?php foreach(COUPON_PERCENTAGE as $value) { ?>
						<option value="<?=$value?>" <?php if(isset($discount_percentage)&&($discount_percentage==$value)) echo "selected";?>><?=$value?>%</option>
						<?php }?>						
					  </select>
					</div>
                </div>                
              </div>              
        </section> 
		 <section class="col-lg-3 connectedSortable">
              <div class="box-body">
                <div class="form-group">
                  <div class="form-group">
					  <label>Search By User</label>
					  <select id="coupon_app_user_id" class="form-control" name="user_id">
					   <option value="">Select</option>
					   <option value="<?=$_SESSION['user_id']?>" <?php if(isset($created_by)&&($created_by==$_SESSION['user_id'])) echo "selected";?> ><?=$_SESSION['user_id']?></option>  
					   <?php 
					   if(!empty($users)){
						foreach($users as $user){ ?>
						<option value="<?=$user['user_id']?>" <?php if(isset($created_by)&&($created_by==$user['user_id'])) echo "selected";?> ><?=$user['user_id']?></option>
					</td>
				
					<?php } } ?>						
					  </select>
					</div>
                </div>                
              </div>              
        </section> 
		<section class="col-lg-3 connectedSortable">
			  <div class="box-body">
                <div class="form-group">
					<label><br/><br/></label>
					<button id="btn_reset" type="reset" class="btn btn-primary">Reset</button>
					&nbsp;&nbsp;
					<button id="btn_coupon_search" type="submit" class="btn btn-primary">Search</button>	
					                </div>                
              </div>              
        </section>			
      </div>
	  </form>
	  <div class="row">
		  <section class="col-lg-4">
			<button id="btn_generate_coupons" type="button" data-toggle="modal" data-target="#couponModal" class="btn btn-success">Generate Coupons</button>	
		
		  </section>
	  </div>
	<form role="form" id="approved_coupon_form" name="approved_coupon_form" action="<?=base_url()?>/index.php/pgdmmr/dashboard/approved_coupons" method="Post">	 
	 <div class="row"> 
	    <section id="section_tbl_filter_data" class="col-lg-12 connectedSortable">
			<table id="tbl_approved_coupons_data" class="display dataTable" width="100%" cellspacing="0" cellpadding="0">
				<thead>
						<tr>
							<th>Select All <input type='checkbox' id="approve_all" name="approve_all" value="" /></th>
							<th>Id</th>
							<th>Coupon Code</th>							
							<th>Percentage</th>
							<th>Active</th>
							<th>Created Date</th>
							<th>Created By</th>
							
														
						</tr>
					</thead>
					<tbody>
					<?php 
					if(!empty($coupons)){
					foreach($coupons as $item){ ?>
					<tr><td><input type='checkbox' name="approved[]" class="chk_approved" value="<?=$item['coupon_id']?>" /> </td><td><?=$item['id']?></td><td><?=$item['coupon_code']?></td><td><?=$item['discount_percentage']?></td>
					<td><?=$item['status']?></td><td><?=$item['created_dt']?></td><td><?=$item['created_by']?></td>				
					</tr>
					<?php } } else {	?>
						<tr><td> </td><td></td><td>No Record Found</td><td></td>
					<td></td><td></td> </tr>
					<?php }	?>
			</table>	
	    </section>		        
      </div>
	   <div class="row">   
	  <section class="col-lg-4 connectedSortable">
			  <div class="box-body">
			  	<div class="form-group">
					<button id="btn_approved" type="submit" class="btn btn-success">Approved</button>  
              	&nbsp;
					<button id="btn_approved_coupons_xls" type="buton" class="btn btn-primary">Download</button>
                </div>	
              </div>              
        </section>	
		</div>   
	</form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
 <!------------------------- Modal -------------------------------->
<div class="modal fade" id="couponModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Generate Coupons</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body couponModal">
		 <div class="row">        
        <section class="col-lg-5 connectedSortable">
		<form role="form" id="search_coupon_form" name='generate_coupon' action="<?=base_url()?>/index.php/pgdmmr/dashboard/generate_coupons" method='post'>
             <div class="box-body">
                <div class="form-group">
                  <label for="coupon_code">No.Of Coupons</label>
                  <input type="text" class="form-control" id="g_coupons" name="g_coupons" placeholder="No.of Coupons to be generated" required>
                </div>                
              </div>          
         
        </section>
		 <section class="col-lg-3 connectedSortable">
              <div class="box-body">
                 <div class="form-group">
					  <label>Percentage</label>
					  <select id="g_discount_percentage" name="g_discount_percentage" class="form-control" required>
					    <option value="">Select</option>
					<?php foreach(COUPON_PERCENTAGE as $value) { ?>
						<option value="<?=$value?>" ><?=$value?>%</option>
						<?php }?>						
					  </select>
					</div>         
              </div>              
        </section> 
		<section class="col-lg-4 connectedSortable">
			  <div class="box-body">
                <div class="form-group"> <br/>
					<button id="btn_generate" type="submit" class="btn btn-warning">Generate</button>	
				</div>                
              </div>              
        </section>			
      </div>
	  </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary" id="">Save changes</button>-->
      </div>
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->

<!------------------------- Modal -------------------------------->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:80%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Student Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body editModal">
	
      </div>
   
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->

<!------------------------- Modal -------------------------------->
<div class="modal fade" id="showDocumentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Show document</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		edit data		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!------------------------- End Modal -------------------------------->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>


