  <div class="content-wrapper">    
    <section class="content-header">
      <h1>
        Ticket Details
      </h1>      
    </section>    
    <section class="content">
		<div class="row">        
			<section class="col-lg-9">		             
				<div class="chat"><!-- start chat -->   
				  <div class="chat-history"><!-- start chat-history -->
					<ul class="chat-ul">
					  <li>&nbsp;</li>
					  <li>
						<div class="message-data">
						  <span class="message-data-name"><i class="fa fa-circle you"></i> You</span>
						</div>
						<div class="message you-message">
						A new client?!?! I would love to help them, but where are we going to find the time?

						</div>
					  </li>
					  <li class="clearfix">
						<div class="message-data align-right">
						  <span class="message-data-name">Ada, your OperationsAlly</span> <i class="fa fa-circle me"></i>
						</div>
						<div class="message me-message float-right"> We should take a look at your onboarding and service delivery workflows, for most businesess there are many ways to save time and not compromise quality.  </div>
					  </li>
					  <li>&nbsp;</li>
						<li class="clearfix">
						<div class="message-data">
						  <span class="message-data-name"><i class="fa fa-circle you"></i> You</span>
						</div>
						<div class="message you-message">
						What?! No way, how did I miss that. I never forgot that part before.

						</div>
					  </li>
					  <li class="clearfix">
						<div class="message-data align-right">
						  <span class="message-data-name">Ada, your OperationsAlly</span> <i class="fa fa-circle me"></i>
						</div>
						<div class="message me-message float-right">Remembering everything can quickly become impossible as your business grows, we need to take a look at your reminder management system and also see if there are steps in your business we can automate.</div>
					  </li>
					  <li>&nbsp;</li>
						<li>
						<div class="message-data">
						  <span class="message-data-name"><i class="fa fa-circle you"></i> You</span>
						</div>
						<div class="message you-message">
						6? Bob told me 8! How did this mix up happen?!
						</div>
					  </li>
					  <li class="clearfix">
						<div class="message-data align-right">
						  <span class="message-data-name">Ada, your OperationsAlly</span> <i class="fa fa-circle me"></i>
						</div>
						<div class="message me-message float-right">
			The more people in your business, the more opportunity for mistakes, having a solid system in place for tracking important client data will help avoid these miscommunications.            </div>
					  </li>
					  <li>&nbsp;</li>
						<li>
						<div class="message-data">
						  <span class="message-data-name"><i class="fa fa-circle you"></i> You</span>
						</div>
						<div class="message you-message">
						I know that I spoke with Mary about this, but where did I put that note...hopefully she also sent me an email...

						</div>
					  </li>
					  <li class="clearfix">
						<div class="message-data align-right">
						  <span class="message-data-name">Ada, your OperationsAlly</span> <i class="fa fa-circle me"></i>
						</div>
						<div class="message me-message float-right">Finding the right information when you need it will save you time and energy. Your data management systems need to grow with your business. All businesses need a dynamic data strategy and a system to ensure that the strategy is implemented correctly.</div>
					  </li>
					</ul>						
				  </div><!-- end chat-history -->					  
				</div> <!-- end chat -->	
			</section>
			<section class="col-lg-9">
				<textarea rows="4" cols="70" style=" resize: none;">
				</textarea> 
				<button id="btn-reply" type="submit" class="btn btn-primary">Reply</button>
			</section>
		</div>  
    </section>    
  </div>  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>