<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ITM-Group of Institutions</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pgdmmr/plugins/iCheck/flat/blue.css">

</head>

<body class="hold-transition login-page">
    <input id="baseurl" type="hidden" value="<?php echo base_url(); ?>"/>
    <div class="login-box">
        <div class="login-logo">
            <a href="#">
                <b>Login</b>
            </a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="#" method="post">
                <div class="form-group has-feedback">
                    <input id="user_email" type="text" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input id="user_pass" type="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                &nbsp;
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <span id="login_btn" class="btn btn-primary btn-block btn-flat">Sign In</span>						
                    </div>
					<div class="col-xs-8">                        
						<p class="help-block class_your_email" style="color:red;display:none;">Please enter your Email</p>
						<p class="help-block class_your_valid_email" style="color:red;display:none;">Please enter valid Email</p>	
						<p class="help-block class_your_password" style="color:red;display:none;">Please enter Password</p>
						<p class="help-block class_wrong_uid_pass" style="color:red;display:none;">Wrong UserId or Password</p>				
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(); ?>assets/pgdmmr/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url(); ?>assets/pgdmmr/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url(); ?>assets/pgdmmr/plugins/iCheck/icheck.min.js"></script>
	
    <script src="<?php echo base_url(); ?>assets/pgdmmr/js/pgdmmr.js"></script>
    <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

</html>